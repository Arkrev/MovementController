This project goal is to let a player be controlled by multiple kind of device such as:
	-Gamepad
	-Keyboard
	-Tablet
	-Leap Motion
	-Kinect

Once you put the Player prefab into a project, you should be able to do with any of this device the following action:
	-Movement
		-Move forward

	-Rotation
		-Rotate Up/Down		
		-Rotate Right/Left

	-Flying

	-Selection of an object

	-Observation of the object
		-Rotate Up/Down
		-Rotate Right/Left

	-Store the object into an inventory.


Some device may let you do more action like:
	-Move backward with a keyboard/gamepad/tablet.
	-Manipulate an object directly with the hands with a leap motion.


--------------------------------------------------------------------------------

Installation : 

	Import the player package.

	Make sure to select which device you want to use in the Game Module.xml which is located into the ressources.
		- "0" if not use.
		- "1" if use.

	Drag an instance of the Player prefab inside the scene.


	If you use a leap motion or a kinnect.
		- You might need to install the device driver onto the computer.

	If you use a tablet.
		- The tablet act the same as a keyboard.
		- You should install a software that let you connect a tablet to a computer.