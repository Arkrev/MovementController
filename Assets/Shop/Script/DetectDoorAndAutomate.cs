﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DetectDoorAndAutomate : MonoBehaviour {

    //public MonoScript animation;

    // Use this for initialization
    void Start()
    {

        bool temp = true;       

        if (temp)
        {
            GameObject[] allObjects = GameObject.FindGameObjectsWithTag("Unknown");
            //Debug.Log(allObjects);
            //Debug.Log(allObjects.Length);
            for (int i = 0; i < (allObjects.Length); i++)
            {

                findAllChild(allObjects[i].transform);
                /*foreach (Transform child in allObjects[i].transform)
                {
                    
                    
                    //Debug.Log(child);
                    if (child.name == "Group1")
                    {
                        GameObject frontDoor = Instantiate((GameObject)Resources.Load("FrontDoor"));
                        frontDoor.transform.SetPositionAndRotation(child.transform.position, child.transform.rotation);
                        
                        
                        DestroyObject(GameObject.Find(child.name));
                    }
                }*/

                /*if (allObjects[i].name.Contains("Group1") == true)
                {
                    GameObject frontDoor = Instantiate((GameObject)Resources.Load("FrontDoor"));
                    frontDoor.transform.SetPositionAndRotation(allObjects[i].transform.position, allObjects[i].transform.rotation);
                    DestroyObject(allObjects[i]);
                }
                Debug.Log("list n°="+i+" is "+allObjects[i].name);*/
            }
        }
        else
        {
            GameObject door = GameObject.Find("Group1");
            GameObject frontDoor = Instantiate((GameObject)Resources.Load("FrontDoor"));
            frontDoor.transform.SetPositionAndRotation(door.transform.position, door.transform.rotation);
            DestroyObject(door);
        }
        /*
        
        */
        //AssetDatabase.SaveAssets();
    }


    void findAllChild(Transform targetObject)
    {
        foreach (Transform child in targetObject.transform)
        {

            if (child.name == "Group1")
            {
                GameObject frontDoor = Instantiate((GameObject)Resources.Load("FrontDoor"));
                frontDoor.transform.SetPositionAndRotation(child.transform.position, child.transform.rotation);


                DestroyObject(GameObject.Find(child.name));
            }
            else
            if (child.childCount > 0)
            {
                findAllChild(child);
            }
            else
            {

                if (child.GetComponent<MeshRenderer>() != null)
                {
                    MeshRenderer sr = child.GetComponent<MeshRenderer>();

                    if (child.GetComponent<MeshRenderer>() != null)
                    {
                        sr = child.GetComponent<MeshRenderer>();
                        GameObject newParent = new GameObject();
                        newParent.transform.position = new Vector3(sr.bounds.center.x, sr.bounds.center.y, sr.bounds.center.z);
                        newParent.name = child.name + "_parent";

                        child.transform.parent = newParent.transform;


                        int childId = extractIdFromString(child.name);
                        //Debug.Log("Id" + childId);
                        if (childId > 331 && childId < 366)
                        {
                            child.gameObject.AddComponent<Rigidbody>();
                            child.gameObject.AddComponent<MeshCollider>();
                            MeshCollider bc = child.gameObject.GetComponent<MeshCollider>();
                            Rigidbody rb = child.gameObject.GetComponent<Rigidbody>();
                            rb.useGravity = true;
                            bc.enabled = true;
                            bc.convex = true;
                            bc.skinWidth = 0.001f;
                            bc.inflateMesh = true;
                            child.parent.gameObject.AddComponent<SmoothMove>();
                            child.parent.gameObject.AddComponent<ObjectSettings>();
                            Texture2D bottleIcon = (Texture2D)Resources.Load("GUIIcon_Bottle");
                            child.parent.gameObject.GetComponent<ObjectSettings>().GuiIcon = bottleIcon;
                            //child.parent.tag = "ObsBotParent";                            
                            child.parent.tag = "Pickable";
                            child.tag = "ObservableBottle";
                            //bc.center = new Vector3(.3970409f, .7355947f, -1.485001f);
                            //bc.size = new Vector3(.08345514f, .3311896f, 0834552f);
                            //Debug.Log("child compo" + child.gameObject.GetComponent<Rigidbody>());
                        }
                        else if (childId == 76)
                        {
                            child.gameObject.AddComponent<MeshCollider>();
                            MeshCollider bc = child.gameObject.GetComponent<MeshCollider>();
                            bc.convex = true;
                        }
                        else if (childId > 9)
                        {
                            child.gameObject.AddComponent<MeshCollider>();
                        }
                        //now replacing the object by prefab one.
                        //Destroy(child.gameObject);


                    }
                }
            }
        }
    }

    int extractIdFromString(string name)
    {
        string a = name;
        string b = string.Empty;

        for (int i = 0; i < a.Length; i++)
        {
            if (Char.IsDigit(a[i]))
                b += a[i];
        }

        if (b.Length > 0)
            return int.Parse(b);
        return -1;
    }
}


