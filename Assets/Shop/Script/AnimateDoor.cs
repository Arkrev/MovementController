﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateDoor : MonoBehaviour
{

    Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "NPC")
        {
            Debug.Log("Enter");
            animator.SetTrigger("PlayerEnter");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" || other.tag == "NPC")
        {
            Debug.Log("Exit");
            animator.SetTrigger("PlayerExit");
        }
    }
}
