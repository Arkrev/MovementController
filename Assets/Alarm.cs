﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alarm : MonoBehaviour
{

    public AudioClip alarm;
    private Light lightAlarm;

    private void Start()
    {
        lightAlarm = GetComponent<Light>();
        lightAlarm.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Enter");
            foreach (KeyValuePair<int, PickObject> pickObj in ObjectManager.instance.PickedObjectDict)
            {
                if (pickObj.Value.objSettings.name.Contains("Mesh"))
                {
                    StartCoroutine("alarmLoop");
                    StartCoroutine("lightLoop"); 
                    StartCoroutine("stopLoop"); 
                    break;
                }
            }
        }
    }

    IEnumerator alarmLoop()
    {
        while (true)
        {
            AudioSource.PlayClipAtPoint(alarm, transform.position);
            yield return new WaitForSecondsRealtime(2f);
        }
    }

    IEnumerator lightLoop()
    {
        lightAlarm.enabled = true;
        lightAlarm.intensity = 0f;
        bool isUp = true;
        while (true)
        {
            if (isUp)
            {
                lightAlarm.intensity += 5f;
                isUp = (lightAlarm.intensity >= 100f);
            }
            else
            {
                lightAlarm.intensity -= 5f;
                isUp = (lightAlarm.intensity <= 0f);
            }
            yield return new WaitForSecondsRealtime(0.5f);
        }
    }

    IEnumerator stopLoop()
    {
        yield return new WaitForSecondsRealtime(10f);
        lightAlarm.enabled = false;
        StopCoroutine("alarmLoop");
        StopCoroutine("lightLoop");
    }

}
