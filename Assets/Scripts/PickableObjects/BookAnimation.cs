﻿#region using...
using System.Collections;
using UnityEngine;
#endregion

public class BookAnimation : MonoBehaviour
{
    #region Settings...
    private Animator anim;
    private bool isTurningLeft = false;
    private bool isTriggerLeft = false;
    private bool isTurningRight = false;
    private bool isTriggerRight = false;
    private bool isOpen = false;

    private GameObject visibleVerso;
    private GameObject visibleRecto;
    private GameObject turnedPage;
    private int currentPageID = 7; // 7 is the number of the page which the verso is visible so the visible recto is of the page 8
    private int minPageID = 4;
    private int maxPageID = 14;
    public Texture2D[] versoTextures;
    public Texture2D[] rectoTextures;
    public Texture2D[] turnedPageTextures;
    private GameObject[] textAreas;

    private GameObject player;
    #endregion

    #region Body...
    // Use this for initialization
    void Awake()
    {
        player = GameObject.Find("Player");
        anim = GetComponent<Animator>();
        visibleVerso = GameObject.Find("PageLeftTex");
        visibleRecto = GameObject.Find("PageRightTex");
        turnedPage = GameObject.Find("PageTex");
        turnPage();
        textAreas = GameObject.FindGameObjectsWithTag("TextArea");
        if (!isOpen)
        {
            foreach (GameObject textArea in textAreas)
            {
                textArea.SetActive(false);
            }
        }
    }


    IEnumerator AnimeBook()
    {
        while (true)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                isOpen = true;
                currentPageID = 7;
                setActivePagesTextAreas();
            }
            else if (Input.GetKeyDown(KeyCode.P))
            {
                isOpen = false;
                currentPageID = -1;
                setActivePagesTextAreas();
            }
            else if (Input.GetKeyDown(KeyCode.L) && isOpen && (currentPageID < maxPageID))
            {
                Debug.Log("isTurningLeft");
                isTurningLeft = true;
                setTurnedPageTexture(currentPageID - minPageID);
            }
            else if (Input.GetKeyDown(KeyCode.M) && isOpen && (currentPageID > minPageID))
            {
                isTurningRight = true;
                setTurnedPageTexture(currentPageID - minPageID - 1);
            }

            OnTriggerStay();

            //if (isOpen) Debug.Log("P" + currentPageID + "_v is visible");

            anim.SetBool("Opening", isOpen);
            anim.SetBool("Closing", !isOpen);
            anim.SetBool("TurningLeft", isTurningLeft);
            anim.SetBool("TurningRight", isTurningRight);
            yield return null;
        }
    }

    IEnumerator CloseBook()
    {
        isOpen = false;
        isTurningLeft = false;
        isTurningRight = false;
        anim.SetBool("Opening", isOpen);
        anim.SetBool("Closing", !isOpen);
        anim.SetBool("TurningLeft", isTurningLeft);
        anim.SetBool("TurningRight", isTurningRight);
        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("ClosedBook"))
        {
            yield return null;
        }
    }

    private void setActivePagesTextAreas()
    {
        if (textAreas != null)
        {
            foreach (GameObject textArea in textAreas)
            {
                if (textArea.name.Contains(currentPageID + "_v") || textArea.name.Contains((currentPageID + 1) + "_r"))
                {
                    textArea.SetActive(true);
                }
                else
                {
                    textArea.SetActive(false);
                }
            }
        }
    }

    private void turnPage()
    {
        int pageID = currentPageID - minPageID;
        setRectoTexture(pageID);
        setVersoTexture(pageID);
    }

    private void setPageTexture(GameObject page, int pageID)
    {
        Material[] mats = page.GetComponent<Renderer>().materials;
        foreach (Material mat in mats)
        {
            //Debug.Log(mat.name);
            if (mat.name == "lr_left (Instance)")
            {
                mat.SetTexture("_MainTex", versoTextures[pageID + 1]);
                //Debug.Log("Texture is apply :" + versoTextures[pageID].name + " //" + mat.name);
            }
            if (mat.name == "lr_right (Instance)")
            {
                mat.SetTexture("_MainTex", rectoTextures[pageID]);
            }
        }
    }

    private void setTurnedPageTexture(int pageID)
    {
        Material[] mats = turnedPage.GetComponent<Renderer>().materials;
        foreach (Material mat in mats)
        {
            //Debug.Log(mat.name);
            if (mat.name == "lr_left (Instance)")
            {
                mat.SetTexture("_MainTex", versoTextures[pageID + 1]);
                //Debug.Log("Texture is apply :" + versoTextures[pageID].name + " //" + mat.name);
            }
            if (mat.name == "lr_right (Instance)")
            {
                mat.SetTexture("_MainTex", rectoTextures[pageID]);
            }
        }
        //.material.SetTexture("_MainTex", turnedPageTextures[pageID]);
    }

    private void setVersoTexture(int pageID)
    {
        //visibleVerso.GetComponent<Renderer>().material.SetTexture("_MainTex", versoTextures[pageID]);
        Material[] mats = visibleVerso.GetComponent<Renderer>().materials;
        foreach (Material mat in mats)
        {
            if (mat.name == "lr_left (Instance)")
            {
                mat.SetTexture("_MainTex", versoTextures[pageID]);
            }
        }
    }

    private void setRectoTexture(int pageID)
    {
        //visibleRecto.GetComponent<Renderer>().material.SetTexture("_MainTex", rectoTextures[pageID]);
        Material[] mats = visibleRecto.GetComponent<Renderer>().materials;
        foreach (Material mat in mats)
        {
            if (mat.name == "lr_right (Instance)")
            {
                mat.SetTexture("_MainTex", rectoTextures[pageID]);
            }
        }
    }

    private void OnTriggerStay()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("TurningPageLeft"))
        {
            // Avoid any reload.
            isTriggerLeft = true;
            setRectoTexture(currentPageID + 1 - minPageID);
        }
        else if (isTriggerLeft)
        {
            isTurningLeft = false;
            isTriggerLeft = false;
            currentPageID++;
            setActivePagesTextAreas();
            setVersoTexture(currentPageID - minPageID);
            // You have just leaved your state!
        }
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("TurningPageRight"))
        {
            // Avoid any reload.
            isTriggerRight = true;
            setVersoTexture(currentPageID - 1 - minPageID);
        }
        else if (isTriggerRight)
        {
            isTurningRight = false;
            isTriggerRight = false;
            currentPageID--;
            setActivePagesTextAreas();
            setRectoTexture(currentPageID - minPageID);
            // You have just leaved your state!
        }
    }
    #endregion
}
