﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class HandleClicks : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log("You clicked:" + eventData.pointerPress.name);
    }
}
