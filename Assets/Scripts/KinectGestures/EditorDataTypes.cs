﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_KINECT
using Kinect = Windows.Kinect;
#endif
using System;

//[System.Flags]
//public enum PoseAxis
//{
//    X = 0x1,
//    Y = 0x2,
//    Z = 0x4
//}

//public enum TrackingState
//{
//    Inferred = 0,
//    NotTracked = 1,
//    Tracked = 2
//}

//public enum TransitionType
//{
//    Rotation
//}

//public enum SequenceType
//{
//    Continues,
//    Static
//}

//public enum InvokeType
//{
//    ContinuesInvoke,
//    SingleInvoke
//}

//public enum RecordDevice
//{
//    None,
//    Kinect
//}
#if UNITY_EDITOR
public class MatchPose: ListItem
{
    
    public Pose Pose { get; set; }

    public bool IgnoreBoneRotation { get; set; }
    public MatchPose(string name): base (name)
    {

    }
    public List<ListItem> BonesList = new List<ListItem>();
    
}
public class Sequence : ListItem
{
    //PROPERTIES
    public PoseAxis PoseAxis { get; set; }
    public float MaxInterludePoses { get; set; }
    public bool TrackHandState { get; set; }

    public TransitionType TransitionType { get; set; }
    public SequenceType SequenceType { get; set; }
    public InvokeType InvokeType { get; set; }
    public TimeLine TimeLine { get; set; }
    public List<ListItem> BonesList = new List<ListItem>();

    //CONSTRUCTOR
    public Sequence(string name) : base(name)
    {
        TimeLine = new TimeLine(new Rect(0.0f, 0.0f, 0.0f, 0.0f), 5.0f, this);
        MaxInterludePoses = 5.0f;
        TrackHandState = true;
    }

    public void DestroyImmediate()
    {
        TimeLine.DestroyImmediate();
    }
}
#endif
public class Pose : ListItem
{
    //PROPERTIES
    public SnapShot SnapShot { get; set; }
    public float Duration { get; set; }

    //CONSTRUCTOR
    public Pose(string name) : base(name)
    {
        Duration = 0.1f;
    }
}


