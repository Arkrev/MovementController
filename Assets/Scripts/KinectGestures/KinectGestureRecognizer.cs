﻿//Usings
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

//RawLinkedPose: used to store information of the linked pose. This raw class is used as between the reading of the xml file
//and it storing of the data in memory. This is crucial due to the small differences between the saved file and the structure
//at runtime.
public class RawLinkedPose
{
    //PROPERTIES
    public string Name { get; set; }
    public float StartTime { get; set; }
    public float Duration { get; set; }

    public HandTrackingState LeftHandState { get; set; }
    public HandTrackingState RightHandState { get; set; }
}

//RawComparableSequence: same principle as RawLinkedPose
public class RawComparableSequence
{
    //FIELDS
    private List<RawLinkedPose> _rawLinkedPoses = new List<RawLinkedPose>();
    private List<ActiveBoneInformation> _activeBones = new List<ActiveBoneInformation>();

    //PROPERTIES
    public List<RawLinkedPose> RawLinkedPoses { get { return _rawLinkedPoses; } set { _rawLinkedPoses = value; } }
    public List<ActiveBoneInformation> ActiveBones { get { return _activeBones; } set { _activeBones = value; } }
    public string Name { get; set; }
    public float Duration { get; set; }
    public float Interlude { get; set; }
    public bool TrackHandstate { get; set; }

    public TransitionType TransitionType { get; set; }
    public SequenceType SequenceType { get; set; }
    public InvokeType InvokeType { get; set; }
    public PoseAxis PoseAxis { get; set; }

    //CONSTRUCTOR
    public RawComparableSequence()
    { }
}

public class RawComparableMatchPose
{
    //FIELDS
    private RawLinkedPose _rawLinkedPose;
    private List<ActiveBoneInformation> _activeBones = new List<ActiveBoneInformation>();
    public bool IgnoreBoneRotation { get; set; }
    //PROPERTIES
    public RawLinkedPose RawLinkedPose { get { return _rawLinkedPose; } set { _rawLinkedPose = value; } }
    public List<ActiveBoneInformation> ActiveBones { get { return _activeBones; } set { _activeBones = value; } }
    public string Name { get; set; }
}

//ComparablePoseInformation: the information of the pose (this is made from the elements on the timeline in the editor.
//This is used to determine the poses in a sequence.
public class ComparablePoseInformation
{
    //PROPERTIES
    public int ID { get; set; } //ID pose has in the master list in the recognizer
    public float StartTime { get; set; } //The starttime of the pose on the "timeline"
    public float Duration { get; set; } //The duration of the pose on the "timeline"
    public float CurrentTimeRunning { get; set; } //The time this pose is matched - for Continues types
    public string Name { get; set; } //Name of this pose
}

//ComparablePose: the final pose itself as it is in memory. It holds it's name (== ID in hash), it's bones and some additional info.
public class ComparablePose
{
    //FIELDS
    private List<Bone> _bones = new List<Bone>(); //The bones that create this pose

    //PROPERTIES
    public string Name { get; private set; } //The name of the pose itself
    public List<Bone> Bones { get { return _bones; } }

    public HandTrackingState LeftHandState { get; private set; }
    public HandTrackingState RightHandState { get; private set; }


    //CONSTRUCTOR
    public ComparablePose(List<Bone> bones, HandTrackingState leftHandState, HandTrackingState rightHandState, string name, float startTime)
    {
        //Construct the pose
        if (bones != null)
            _bones = bones;
        Name = name;
        LeftHandState = leftHandState;
        RightHandState = rightHandState;
    }

    //METHODS
    private bool CanMapAllBones(ref List<Bone> inputBones)
    {
        foreach (Bone bone in inputBones)
        {
            if (!_bones.Any(x => x.Equals(bone))) //If we don't find the bone return false. Comparrison is NAME BASED!! See overridden 'Equals' method in Bon (same as below)
                return false;
        }
        return true;
    }

    public override bool Equals(object obj)
    {
        string other = (string)obj;

        return this.Name.GetHashCode().Equals(other.GetHashCode());
    }

    public override int GetHashCode() //Override hashcode method to no collide with logic of equals
    {
        return this.Name.GetHashCode();
    }
}
//ActiveBoneInformation: holds the information about the active bones in a sequence
public class ActiveBoneInformation
{
    //PROPERTIES
    public string Name { get; set; }
    public float AllowedOffset { get; set; }

    //CONSTRUCTOR
    public ActiveBoneInformation(string name, float allowedOffset)
    {
        Name = name;
        AllowedOffset = allowedOffset;
    }
}
//ComparableMatchPose
public class ComparableMatchPose
{
    //FIELDS
    private List<ActiveBoneInformation> _activeBones = new List<ActiveBoneInformation>(); //List with all the active bones we want to check


    //PROPERTIES
    public string Name { get; set; }
    public ComparablePoseInformation PoseInformation { get; set; }
    public List<ActiveBoneInformation> ActiveBones { get { return _activeBones; } }

    public bool IgnoreBoneRotation { get; set; }

    public ComparableMatchPose(string name, ComparablePoseInformation linkedposeinfo, List<ActiveBoneInformation> activeBones)
    {
        Name = name;
        PoseInformation = linkedposeinfo;
        _activeBones = activeBones;
    }
}

//ComparableSequence: the final sequence. Mostly a composition of sequence info and poses info.
public class ComparableSequence
{
    //FIELDS
    private List<ComparablePoseInformation> _linkedPosesInformation = new List<ComparablePoseInformation>(); //List with all the info for the poses (with the ID of linkedposes)
    private List<ActiveBoneInformation> _activeBones = new List<ActiveBoneInformation>(); //List with all the active bones we want to check
    private int _currentIndexInformation = -1; //The index used to get the correct pose information

    //PROPERTIES
    public string Name { get; set; }
    public float Duration { get; set; }
    public float Interlude { get; set; }
    public bool TrackHandstate { get; set; }
    public float TimeBetweenPoses { get; set; }
    public TransitionType TransitionType { get; set; }
    public SequenceType SequenceType { get; set; }
    public InvokeType InvokeType { get; set; }
    public PoseAxis PoseAxis { get; set; }
    public ComparablePoseInformation CurrentPoseInformation { get; set; }
    public ComparablePoseInformation PreviousPoseInformation { get; set; }
    public bool SequenceCompleted { get; set; }

    public List<ComparablePoseInformation> RequiredPosesInformation { get { return _linkedPosesInformation; } }
    public List<ActiveBoneInformation> ActiveBonesNames { get { return _activeBones; } }

    //CONSTRUCTOR
    public ComparableSequence(List<ComparablePoseInformation> linkedPosesInformation, List<ActiveBoneInformation> activeBones)
    {
        _linkedPosesInformation = linkedPosesInformation;
        _activeBones = activeBones;
    }

    //METHODS
    public void NextPoseInformation()
    {
        if (_currentIndexInformation < (_linkedPosesInformation.Count - 1))
        {
            if (CurrentPoseInformation != null)
            {
                //Store the current information as the previous one
                PreviousPoseInformation = CurrentPoseInformation;
                CurrentPoseInformation.CurrentTimeRunning = 0.0f; //Reset timer running if not used anymore
            }

            ++_currentIndexInformation;
            CurrentPoseInformation = _linkedPosesInformation[_currentIndexInformation];
        }
        else
        {
            if (CurrentPoseInformation != null)
                PreviousPoseInformation = CurrentPoseInformation;
            CurrentPoseInformation = null;
        }
    }

    public void ResetSequence()
    {
        _currentIndexInformation = -1;
        TimeBetweenPoses = 0.0f;
        if (CurrentPoseInformation != null)
            CurrentPoseInformation.CurrentTimeRunning = 0.0f;
        CurrentPoseInformation = null;
        if (PreviousPoseInformation != null)
            PreviousPoseInformation.CurrentTimeRunning = 0.0f;
        PreviousPoseInformation = null;
        SequenceCompleted = false;
    }
}

//KinectGestureRecognizer: the recognizer itself. It uses data defined above and is responsible for the updating.
public class KinectGestureRecognizer : Singleton<KinectGestureRecognizer>
{
#if UNITY_KINECT
    //FIELDS
    private GameObject _objectRecorder = null;

    private List<ComparableSequence> _comparableSequences = new List<ComparableSequence>(); //All the sequences that we need to keep track of
    private List<ComparablePose> _comparablePoses = new List<ComparablePose>(); //All the possible poses. A sequence holds the ID's of the elements in the list to create a sequence
    private List<ComparableMatchPose> _comparableMatchPoses = new List<ComparableMatchPose>();
    private string _pathToFile = "";
    private KinectBody _realtimeBody = null;

    //METHODS
    void Start()
    {
        //Fill in path
        _pathToFile = "GestureEditorSave";

        //Load all the poses from the savefile and store them
        List<Pose> posesFromFile = GestureEditorSerializer.Instance.LoadPosesInGame(_pathToFile);
        foreach (Pose pose in posesFromFile)
        {
            ComparablePose cp = new ComparablePose(pose.SnapShot.BonesSnapShot, pose.SnapShot.LeftHandState, pose.SnapShot.RightHandState, pose.Name, pose.Duration);
            _comparablePoses.Add(cp);
        }

        //Load all the sequences from the savefile and store them
        List<RawComparableSequence> rawSequences = GestureEditorSerializer.Instance.LoadRawSequencesInGame(_pathToFile);
        foreach (RawComparableSequence rawSequence in rawSequences)
        {
            //List with "ID's"
            List<ComparablePoseInformation> poseInformation = new List<ComparablePoseInformation>();
            foreach (RawLinkedPose rawPose in rawSequence.RawLinkedPoses)
            {
                //Find the pose in the list of poses and store the index (==ID), if it is located in the list (NAME BASED!!)
                if (_comparablePoses.Any(x => x.Name.Equals(rawPose.Name)))
                {
                    ComparablePoseInformation cpi = new ComparablePoseInformation();
                    cpi.ID = _comparablePoses.FindIndex(x => x.Name.Equals(rawPose.Name));
                    cpi.Duration = rawPose.Duration;
                    cpi.StartTime = rawPose.StartTime;
                    cpi.Name = rawPose.Name;

                    //Add it
                    poseInformation.Add(cpi);
                }
            }

            //ONLY if we found at least one pose, create a new clean sequence
            if (poseInformation.Count > 0)
            {
                //Create clean comparable sequence
                ComparableSequence cs = new ComparableSequence(poseInformation, rawSequence.ActiveBones);

                //Transfer all the variables
                cs.Name = rawSequence.Name;
                cs.Duration = rawSequence.Duration;
                cs.Interlude = rawSequence.Interlude;
                cs.TrackHandstate = rawSequence.TrackHandstate;
                cs.PoseAxis = rawSequence.PoseAxis;
                cs.SequenceType = rawSequence.SequenceType;
                cs.InvokeType = rawSequence.InvokeType;
                cs.TransitionType = rawSequence.TransitionType;

                //Add the new comparable sequence
                _comparableSequences.Add(cs);
            }
        }

        List<RawComparableMatchPose> rawMatchPoses = GestureEditorSerializer.Instance.LoadRawMatchPosesInGame(_pathToFile);
        foreach (RawComparableMatchPose rawmp in rawMatchPoses)
        {
            //Pose information by id
            ComparablePoseInformation poseInformation = new ComparablePoseInformation();
            if (_comparablePoses.Any(x => x.Name.Equals(rawmp.RawLinkedPose.Name)))
            {
                poseInformation.ID = _comparablePoses.FindIndex(x => x.Name.Equals(rawmp.RawLinkedPose.Name));
                poseInformation.Name = rawmp.RawLinkedPose.Name;
            }

            _comparableMatchPoses.Add(new ComparableMatchPose(rawmp.Name, poseInformation, rawmp.ActiveBones) { IgnoreBoneRotation = rawmp.IgnoreBoneRotation });
        }



        //Create Kinect Recorder
        _objectRecorder = new GameObject("KinectGestureRecorder");
        _objectRecorder.AddComponent<KinectGestureRecorder>();
        _objectRecorder.transform.parent = this.transform;
        KinectGestureRecorder.Instance.Record = true;
    }

    new void OnDestroy()
    {
        //Destroy objectRecorder
        if (_objectRecorder != null)
            GameObject.DestroyImmediate(_objectRecorder);
    }

    public Dictionary<string, float> RecognizeMatchPoses()
    { 
        Dictionary<string,float> result = new Dictionary<string,float>();

        foreach (ComparableMatchPose pose in _comparableMatchPoses)
        {
            result.Add(pose.Name, MatchPosePercentage(pose));
        }

        return result;
    }

    public void RecognizeSequences(ref List<ComparableSequence> container)
    {
        //Update real-time pose
        if (KinectGestureRecorder.Instance.KnownIDs.Count > 0 && KinectGestureRecorder.Instance.Record == true)
             //ORIGINAL FUNCTION
            _realtimeBody = KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.First());

			//EDITED FUNCTION WITH NEW 'CLOSEST PLAYER'
			//_realtimeBody = KinectGestureRecorder.Instance.GetKinectBodyNearestToPos(GameManager.instance.PlayerFieldPos, GameManager.instance.PlayerFieldRadius);
		   
			else _realtimeBody = null;

        //Compare with sequences
        foreach (ComparableSequence sequence in _comparableSequences)
        {
            if (sequence == null)
                continue;

            //For each Sequence that is "Static", check if it meets the requirements (in pose while 'running' or completed a sequence)
            if (sequence.SequenceType == SequenceType.Static)
            {
                //Check if we have a current pose, if not get it
                if (sequence.CurrentPoseInformation == null && sequence.PreviousPoseInformation == null && sequence.SequenceCompleted == false) //NOT RUNNING
                {
                    sequence.ResetSequence();
                    sequence.NextPoseInformation();
                }

                //Check if we have match the current pose within the allowed interlude if we have a next pose
                //or if we check our previous pose at the end of a sequence.
                if (sequence.CurrentPoseInformation != null || sequence.PreviousPoseInformation != null)
                {
                    //Increase timer before checking
                    sequence.TimeBetweenPoses += Time.deltaTime;

                    //Check if we have a match (checks the current one, and if we don't have current pose, it checks for the previous pose)
                    if (MatchPose(sequence) == true)
                    {
                        //If so, get the next in line if any
                        if (sequence.SequenceCompleted == false)
                            sequence.NextPoseInformation();

                        /*Reset the time between poses so we can check if we don't have to force a reset of the sequence*/
                        sequence.TimeBetweenPoses = 0.0f;

                        //If we don't have a current pose, we completed the sequence (can still be checking previous pose though)
                        if (sequence.CurrentPoseInformation == null)
                        {
                            //Add to input-output container, so we can capture this sequence has completed
                            container.Add(sequence);
                            sequence.SequenceCompleted = true;

                            //Reset based on the sequence invoke type.
                            //If it is a single invoke, reset it immediately else trigger it for x seconds
                            if (sequence.InvokeType == InvokeType.SingleInvoke)
                            {
                                
                                sequence.ResetSequence(); //Reset it
                                //TODO: remove this, we use it for testing the comparison
                                //Camera.main.backgroundColor = Color.green;
                            }
                            else if (sequence.InvokeType == InvokeType.ContinuousInvoke)
                            {
                                //TODO: remove this, we use it for testing the comparison
                                //Camera.main.backgroundColor = Color.green;
                                if (sequence.PreviousPoseInformation != null)
                                    sequence.PreviousPoseInformation.CurrentTimeRunning += Time.deltaTime;

                                sequence.TimeBetweenPoses = 0.0f;
                                //We do nothing, until we exceeded the invoke time. We'll keep checking our previous pose to be sure we are still matching
                                //Then we reset so we can start checking the new current pose again
                                //TODO add timer for max invoke time + option to invoke endless (atm it will be endless)
                            }
                        }
                    }
                    /*Else if we didn't match check if our interlude hasn't expired. If a pose isn't met in a certain timespan we restart the sequence*/
                    /*Only if we have a current pose. If we base ourselves on the previous pose it means we completed the sequence and we keep invoking it*/
                    else if (sequence.CurrentPoseInformation != null && sequence.TimeBetweenPoses >= sequence.Interlude && sequence.SequenceCompleted == false)
                    {
                        sequence.ResetSequence();
                        continue;
                    }
                    /*Else if we don't have a current pose and our previous pose isn't met anymore, we reset the sequence (ending the invoking)*/
                    else if (sequence.CurrentPoseInformation == null && sequence.PreviousPoseInformation != null && sequence.SequenceCompleted == true)
                    {
                        sequence.ResetSequence();
                        continue;
                    }
                }
                else
                {
                    /*If we haven't found a new pose to match or an old pose, reset the sequence to ensure it has been reset correctly. If this shouldn't help this means the sequence
                     doesn't have any poses defined*/
                    sequence.ResetSequence();
                }
            }
            else if (sequence.SequenceType == SequenceType.Continuous)
            {
                //TODO: implement the continues type. Same as the static EXCEPT that the time in and between poses is important!

            }
        }
    }
    public bool HasActiveBody()
    {
        return _realtimeBody != null;
    }
    private bool MatchPose(ComparableSequence sequence)
    {
        //Check all the bones that are active with the current recording and check if the are within a certain offset
        //Start by checking if all the active bones are mapped correct in the real-time one
        if (_realtimeBody == null || sequence == null)
            return false;

        if (sequence.CurrentPoseInformation == null && sequence.PreviousPoseInformation == null)
            return false;

        List<Bone> targetBones = new List<Bone>();
        List<Bone> realtimeBones = new List<Bone>();
        List<float> allowedOffsets = new List<float>();
        //Get all the active bones that are needed to match, based on the to match pose
        //The current one has a higher priority then the previous one!
        ComparablePose currentPose = null;
        if (sequence.CurrentPoseInformation != null)
            currentPose = _comparablePoses[sequence.CurrentPoseInformation.ID];
        else if (sequence.PreviousPoseInformation != null)
            currentPose = _comparablePoses[sequence.PreviousPoseInformation.ID];

        foreach (ActiveBoneInformation activeBone in sequence.ActiveBonesNames)
        {
            Bone targetBone = currentPose.Bones.Find(x => x.Name.Equals(activeBone.Name));
            Bone realtimeBone = _realtimeBody.Bones.Find(x => x.Name.Equals(activeBone.Name));
           
            if (targetBone != null && realtimeBone != null)
            {
                targetBones.Add(targetBone);
                realtimeBones.Add(realtimeBone);
                allowedOffsets.Add(activeBone.AllowedOffset);
            }
            else
            {
                return false;
            }
        }

        //If we have all the bones, first check if we need to track the handstate. If so, first check if these match
        if (sequence.TrackHandstate == true)
        {
            //Required tracking state
            HandTrackingState leftHandState = currentPose.LeftHandState;
            HandTrackingState rightHandState = currentPose.RightHandState;

            if (targetBones.Find(x => x.Name.Equals("HandLeft")) != null && _realtimeBody.LeftHandState != leftHandState)
                return false;

            if (targetBones.Find(x => x.Name.Equals("HandRight")) != null && _realtimeBody.RightHandState != rightHandState)
                return false;
        }

        //Then check if all the bones are within a certain offset angle
        for (int i = 0; i < realtimeBones.Count; ++i)
        {
            //Create 2 vectors representing the bones (based from start position to end position)
            if (realtimeBones[i].EndPosition.HasValue == false || realtimeBones[i].EndPosition == Vector3.zero ||
                targetBones[i].EndPosition.HasValue == false || targetBones[i].EndPosition == Vector3.zero)
            {
                //TODO: keep track of the bones BUT use a standard difference, now the last bone will be skipped
                continue;
            }
            else
            {
                //If we have both an end position and a start position, create the 2 vectors based on those
                //two values
                Vector3 bone1 = realtimeBones[i].EndPosition.Value - realtimeBones[i].StartPosition;
                Vector3 bone2 = targetBones[i].EndPosition.Value - targetBones[i].StartPosition;
                bone1 = bone1.normalized;
                bone2 = bone2.normalized;

                //Check if the angle is not big enough
                float offsetAngle = allowedOffsets[i];
                float currentAngle = Vector3.Angle(bone1, bone2);
                if (currentAngle > offsetAngle && currentAngle != 180.0f)
                {
                    return false;
                }
            }

        }

        //If you reach end of this method, the pose matches
        return true;
    }

    private float MatchPosePercentage(ComparableMatchPose pose)
    {
        if (_realtimeBody == null || pose == null)
            return 0f;

        if (pose.PoseInformation == null)
            return 0f;

        List<Bone> targetBones = new List<Bone>();
        List<Bone> realtimeBones = new List<Bone>();
        //Get all the active bones that are needed to match, based on the to match pose
        //The current one has a higher priority then the previous one!
        ComparablePose currentPose = _comparablePoses[pose.PoseInformation.ID];

        foreach (ActiveBoneInformation activeBone in pose.ActiveBones)
        {
            Bone targetBone = currentPose.Bones.Find(x => x.Name.Equals(activeBone.Name));
            Bone realtimeBone = _realtimeBody.Bones.Find(x => x.Name.Equals(activeBone.Name));
            if (targetBone != null && realtimeBone != null)
            {
                targetBones.Add(targetBone);
                realtimeBones.Add(realtimeBone);
            }
            else
            {
                return 0.0f;
            }
        }
        if(realtimeBones.Count == 0)return 0.0f;

        //If we have all the bones, check if all the bones are within a certain offset angle
        float offsetPct = 1f;
        int trackedbones = 0;
        for (int i = 0; i < realtimeBones.Count; ++i)
        {
            //TODO: capture issue with 180.0f flipping (even if not in real life). Can cause mismatch...

            Quaternion rtRotation = new Quaternion(realtimeBones[i].Orientation.x, realtimeBones[i].Orientation.y, realtimeBones[i].Orientation.z, realtimeBones[i].Orientation.w);
            Quaternion targetRotation = new Quaternion(targetBones[i].Orientation.x, targetBones[i].Orientation.y, targetBones[i].Orientation.z, targetBones[i].Orientation.w);

            if (targetBones[i].TrackingState != TrackingState.Tracked)
                continue;

            float currentAngle = 0f;
            if (pose.IgnoreBoneRotation)
            {
                currentAngle = Quaternion.Angle(Quaternion.LookRotation(rtRotation * Vector3.up), Quaternion.LookRotation(targetRotation * Vector3.up));
            }
            else
            { 
                currentAngle = Quaternion.Angle(rtRotation, targetRotation);
            }

            float minAngle = 5f;
            float maxAngle = 60f;

            float anglepct = Mathf.Clamp01( (maxAngle - currentAngle) / (maxAngle - minAngle));
            offsetPct *= anglepct;

            ++trackedbones;
        }

        //if (trackedbones > 0)
        //{
        //    offsetPct /= trackedbones;
        //}
        //else
        //{
        //    offsetPct = 0f;
        //}

        return offsetPct;
    }

    private Quaternion DiscardAxis(Vector3 axis, Quaternion rotation)
    {
        Quaternion adjustedRotation = rotation;
        if (axis.x != 0)
        {
            Vector3 angles = adjustedRotation.eulerAngles;
            angles.x = 0;
            adjustedRotation = Quaternion.Euler(angles);
        }

        if (axis.y != 0)
        {
            Vector3 angles = adjustedRotation.eulerAngles;
            angles.y = 0;
            adjustedRotation = Quaternion.Euler(angles);
        }

        if (axis.z != 0)
        {
            Vector3 angles = adjustedRotation.eulerAngles;
            angles.z = 0;
            adjustedRotation = Quaternion.Euler(angles);
        }

        return adjustedRotation;
    }
#endif
}