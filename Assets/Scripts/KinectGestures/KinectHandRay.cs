﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinectHandRay : MonoBehaviour {

    public Transform leftHand;
    public Transform rightHand;

    // Update is called once per frame
    void Update () {
        Debug.DrawLine(leftHand.position, rightHand.position, Color.green);
	}
}
