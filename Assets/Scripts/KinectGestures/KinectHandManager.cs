﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KinectHandManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        KinectBody body = PlayerInputManager.MyKinectBody;
        if (body != null)
        {
            //Rotation of object based on hand positions
            Bone handBone1 = body.Bones.Find(x => x.Name == "HandLeft");
            Bone handBone2 = body.Bones.Find(x => x.Name == "HandRight");
            //Debug.Log("handBone1.StartPosition = " + handBone1.StartPosition);
            Debug.Log("handDir = " + (handBone1.StartPosition - handBone2.StartPosition).normalized);
            if (tag == "LeftHandKinect")
            {
                //transform.localPosition = new Vector3(handBone1.StartPosition.x-0.5f, handBone1.StartPosition.y-1, -handBone1.StartPosition.z + 2.5f);
                transform.localPosition = new Vector3(handBone1.StartPosition.x, handBone1.StartPosition.y, handBone1.StartPosition.z);
            }
            else if (tag == "RightHandKinect")
            {
                //transform.localPosition = new Vector3(handBone2.StartPosition.x-0.5f, handBone2.StartPosition.y-1, -handBone2.StartPosition.z + 2.5f);
                transform.localPosition = new Vector3(handBone2.StartPosition.x, handBone2.StartPosition.y, handBone2.StartPosition.z);
            }

        }
    }
}
