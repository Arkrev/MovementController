﻿#if UNITY_KINECT
using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;

public class GestureEditorSerializer : Singleton<GestureEditorSerializer>
{
    //FIELDS
    private bool _loadedConfigInformation = false;
    private string _configPath = "GestureEditorConfig.xml";

    //PROPERTIES

    //METHODS
#if UNITY_EDITOR
    public void LoadConfig()
    {
        //Try to open the config file and get the information required for the editor
        XDocument xml = XDocument.Load(Application.dataPath + "/Resources/" + _configPath);

        //If file is found
        if (xml != null)
        {
            //Get the root
            XElement root = xml.Root;

            //Read the information
            XElement deviceElement = root.Element(XName.Get("Device"));
            if (deviceElement != null)
            {
                RecordDevice type = (RecordDevice)Enum.Parse(typeof(RecordDevice), deviceElement.Attribute(XName.Get("Type")).Value);
                GestureEditor.Instance.CurrentDevice = type;
            }

            _loadedConfigInformation = true;
        }
    }

    public void SaveEditor(string filePath)
    {
        //If never loaded config, return
        if (_loadedConfigInformation == false)
            return;

        //If no path given
        if (filePath == "")
            return;

        //Create or open file
        XDocument doc = new XDocument();
        XElement root = new XElement("EditorSave");

        //Store all the poses
        XElement poses = new XElement("Poses");
        foreach (Pose pose in GestureEditor.Instance.Poses)
        {
            float duration = pose.Duration;
            string name = pose.Name;
            SnapShot snapshot = pose.SnapShot;

            XElement p = new XElement("Pose",
                new XAttribute("Name", name),
                new XAttribute("Duration", duration),
               new XAttribute("LeftHandState", snapshot.LeftHandState),
                new XAttribute("RightHandState", snapshot.RightHandState));


            XElement bones = new XElement("Bones");
            foreach (Bone bone in snapshot.BonesSnapShot)
            {
                XElement b = new XElement("Bone",
                    new XAttribute("Name", bone.Name),
                    new XAttribute("TrackingState", bone.TrackingState),
                    new XAttribute("StartPositionX", bone.StartPosition.x),
                    new XAttribute("StartPositionY", bone.StartPosition.y),
                    new XAttribute("StartPositionZ", bone.StartPosition.z),
                    new XAttribute("EndPositionX", bone.EndPosition.GetValueOrDefault().x),
                    new XAttribute("EndPositionY", bone.EndPosition.GetValueOrDefault().y),
                    new XAttribute("EndPositionZ", bone.EndPosition.GetValueOrDefault().z),
                    new XAttribute("OrientationX", bone.Orientation.x),
                    new XAttribute("OrientationY", bone.Orientation.y),
                    new XAttribute("OrientationZ", bone.Orientation.z),
                    new XAttribute("OrientationW", bone.Orientation.w));

                bones.Add(b); //Add bone to bones
            }
            p.Add(bones); //Add bones to pose
            poses.Add(p); //Add pose to poses
        }

        //Store all the sequences
        XElement sequences = new XElement("Sequences");
        foreach (Sequence sequence in GestureEditor.Instance.Sequences)
        {
            bool trackHandstate = sequence.TrackHandState;
            float duration = sequence.TimeLine.TotalDuration;
            float interlude = sequence.MaxInterludePoses;
            TransitionType transitionType = sequence.TransitionType;
            SequenceType sequenceType = sequence.SequenceType;
            InvokeType invokeType = sequence.InvokeType;
            PoseAxis axis = sequence.PoseAxis;
            string name = sequence.Name;

            //Create an element
            XElement s = new XElement("Sequence",
                new XAttribute("Name", name),
                new XAttribute("Duration", duration),
                new XAttribute("Interlude", interlude),
                new XAttribute("TransitionType", transitionType),
                new XAttribute("SequenceType", sequenceType),
                new XAttribute("InvokeType", invokeType),
                new XAttribute("PoseAxis", axis),
                new XAttribute("TrackHandstate", trackHandstate));

            //Add all the poses linked in the sequence
            XElement linkedPoses = new XElement("LinkedPoses");
            foreach (TimeMarker marker in sequence.TimeLine.Markers)
            {
                Pose linkedPose = null;
                if (marker.LinkedItem is Pose)
                    linkedPose = (Pose)marker.LinkedItem;
                else
                    continue;
                //If pose not found in poselist, ignore it (error)
                if (!GestureEditor.Instance.Poses.Any(x => x.Name.Equals(linkedPose.Name)))
                    continue;
                //Else store it
                float startTimePose = marker.StartTime;
                float durationPose = linkedPose.Duration;

                XElement p = new XElement("LinkedPose",
                    new XAttribute("Name", linkedPose.Name),
                    new XAttribute("StartTime", startTimePose),
                    new XAttribute("Duration", durationPose));
                linkedPoses.Add(p); //Add linked pose to the linked poses
            }
            s.Add(linkedPoses); //Add linked poses == markers to the sequence

            //Add the list of active bones
            XElement ab = new XElement("ActiveBones");
            foreach (ListItem bone in sequence.BonesList)
            {
                XElement b = new XElement("Bone",
                    new XAttribute("Name", bone.Name),
                    new XAttribute("Active", bone.Active),
                    new XAttribute("AllowedOffset", bone.ExtraFloatValue));

                ab.Add(b); //Add bone to active bone list
            }
            s.Add(ab); //Add active bonelist to sequence

            sequences.Add(s);//Add sequence to sequences
        }

        //Matchposes
        XElement matchPoses = new XElement("MatchPoses");
        foreach (MatchPose matchpose in GestureEditor.Instance.MatchPoses)
        {
            XElement p = new XElement("MatchPose",
               new XAttribute("Name", matchpose.Name));

            //add linkedpose
            if (matchpose.Pose != null && GestureEditor.Instance.Poses.Any(x => x.Name.Equals(matchpose.Pose.Name)))
            {
                p.Add(new XAttribute("LinkedPose", matchpose.Pose.Name));
            }

            p.Add(new XAttribute("IgnoreBoneRotation", matchpose.IgnoreBoneRotation));

            XElement ab = new XElement("ActiveBones");
            foreach (ListItem bone in matchpose.BonesList)
            {



                XElement b = new XElement("Bone",
                    new XAttribute("Name", bone.Name),
                    new XAttribute("Active", bone.Active),
                    new XAttribute("AllowedOffset", bone.ExtraFloatValue));

                ab.Add(b); //Add bone to active bone list
            }

            p.Add(ab);

            matchPoses.Add(p);
        }

        //Save file if path is valid
        root.Add(poses); //Add poses to root
        root.Add(sequences); //Add sequences to root
        root.Add(matchPoses);//Add matchPoses to root
        doc.Add(root); //Add root to doc
        doc.Save(filePath);
    }

    public void LoadEditor(string filePath)
    {
        //If never loaded config, return
        if (_loadedConfigInformation == false)
            return;

        //If no path given
        if (filePath == "")
            return;

        //Load file if any
        XDocument xml = XDocument.Load(filePath);
        if (xml != null)
        {
            //Get the root
            XElement root = xml.Root;

            //Start by getting the poses
            XElement posesElement = root.Element(XName.Get("Poses"));
            if (posesElement != null)
            {
                //Foreach pose
                IEnumerable<XElement> poseTabs = posesElement.Descendants(XName.Get("Pose"));
                if (poseTabs != null)
                {
                    //Clear list in editor
                    GestureEditor.Instance.ClearPoses();
                    //Froeach pose
                    foreach (XElement pose in poseTabs)
                    {
                        //Create a clean pose
                        Pose p = new Pose("");
                        //Get the attributes and fill them in
                        p.Name = pose.Attribute(XName.Get("Name")).Value;
                        p.Duration = float.Parse(pose.Attribute(XName.Get("Duration")).Value);
                        HandTrackingState leftHandState = HandTrackingState.Unknown;
                        HandTrackingState rightHandState = HandTrackingState.Unknown;

                        if (pose.Attribute(XName.Get("LeftHandState")) != null && pose.Attribute(XName.Get("RightHandState")) != null)
                        {
                            leftHandState = (HandTrackingState)Enum.Parse(typeof(HandTrackingState), pose.Attribute(XName.Get("LeftHandState")).Value);
                            rightHandState = (HandTrackingState)Enum.Parse(typeof(HandTrackingState), pose.Attribute(XName.Get("RightHandState")).Value);
                        }

                        //Create a new snapshot and fill the bones
                        IEnumerable<XElement> bones = pose.Descendants(XName.Get("Bone"));
                        if (bones != null)
                        {
                            List<Bone> bonesList = new List<Bone>();
                            foreach (XElement bone in bones)
                            {
                                string boneName = bone.Attribute(XName.Get("Name")).Value;
                                TrackingState state = (TrackingState)Enum.Parse(typeof(TrackingState), bone.Attribute(XName.Get("TrackingState")).Value);
                                Vector3 startPosition = new Vector3(
                                    float.Parse(bone.Attribute(XName.Get("StartPositionX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("StartPositionY")).Value), 
                                    float.Parse(bone.Attribute(XName.Get("StartPositionZ")).Value));
                                Vector3? endPosition = new Vector3(
                                    float.Parse(bone.Attribute(XName.Get("EndPositionX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("EndPositionY")).Value),
                                    float.Parse(bone.Attribute(XName.Get("EndPositionZ")).Value));
                                if (endPosition.Value.x == 0.0f && endPosition.Value.y == 0.0f && endPosition.Value.z == 0.0f)
                                    endPosition = null;
                                Vector4 orientation = new Vector4(
                                    float.Parse(bone.Attribute(XName.Get("OrientationX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationY")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationZ")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationW")).Value));
                                Bone b = new Bone(startPosition, endPosition, orientation, state, boneName);
                                bonesList.Add(b);
                            }
                            //If we have bones, create a snapshot and fill it with the received bones
                            if (bonesList.Count > 0)
                            {
                                SnapShot snapShot = new SnapShot(bonesList, leftHandState, rightHandState);
                                p.SnapShot = snapShot;
                            }
                        }
                        //Add the pose to the list in the editor
                        GestureEditor.Instance.AddPose(p);
                    }
                }
            }

            //After reading the poses, read the sequences
            XElement sequenceElement = root.Element(XName.Get("Sequences"));
            if (sequenceElement != null)
            {
                //Clear list
                GestureEditor.Instance.ClearSequences();
                //Foreach sequence
                IEnumerable<XElement> sequenceTabs = sequenceElement.Descendants(XName.Get("Sequence"));
                if (sequenceTabs != null)
                {
                    foreach (XElement sequence in sequenceTabs)
                    {
                        //Get the attributes
                        string sequenceName = sequence.Attribute(XName.Get("Name")).Value;
                        float duration = float.Parse(sequence.Attribute(XName.Get("Duration")).Value);
                        float interlude = float.Parse(sequence.Attribute(XName.Get("Interlude")).Value);
                        TransitionType transition = (TransitionType)Enum.Parse(typeof(TransitionType), sequence.Attribute(XName.Get("TransitionType")).Value);
                        SequenceType sequenceType = (SequenceType)Enum.Parse(typeof(SequenceType), sequence.Attribute(XName.Get("SequenceType")).Value);
                        InvokeType invokeType = (InvokeType)Enum.Parse(typeof(InvokeType), sequence.Attribute(XName.Get("InvokeType")).Value);
                        PoseAxis axis = (PoseAxis)Enum.Parse(typeof(PoseAxis), sequence.Attribute(XName.Get("PoseAxis")).Value);

                       
                        bool trackHandstate = sequence.Attribute(XName.Get("TrackHandstate"))!= null && bool.Parse(sequence.Attribute(XName.Get("TrackHandstate")).Value);


                        //Create a clean sequence and fill in the attributes
                        Sequence s = new Sequence(sequenceName);
                        s.MaxInterludePoses = interlude;
                        s.TrackHandState = trackHandstate;
                        s.TransitionType = transition;
                        s.SequenceType = sequenceType;
                        s.InvokeType = invokeType;
                        s.PoseAxis = axis;
                        s.TimeLine.TotalDuration = duration;
                        s.TimeLine.LinkedSequence = s;

                        //For each sequence check the linked poses and add them to the timeline
                        XElement linkedPosesTab = sequence.Element(XName.Get("LinkedPoses"));
                        if (linkedPosesTab != null)
                        {
                            IEnumerable<XElement> linkedPoses = linkedPosesTab.Descendants(XName.Get("LinkedPose"));
                            if (linkedPoses != null)
                            {
                                foreach (XElement linkedPose in linkedPoses)
                                {
                                    string markerName = linkedPose.Attribute(XName.Get("Name")).Value;
                                    float markerStartTime = float.Parse(linkedPose.Attribute(XName.Get("StartTime")).Value);
                                    float markerDuration = float.Parse(linkedPose.Attribute(XName.Get("Duration")).Value);
                                    Pose pose = GestureEditor.Instance.Poses.Find(x => x.Name.Equals(markerName));

                                    if (pose != null)
                                    {
                                        //Create new marker
                                        TimeMarker marker = new TimeMarker(markerStartTime, markerDuration, Vector2.zero);
                                        Pose linkedItem = (Pose)pose.Clone();
                                        linkedItem.Duration = markerDuration;
                                        marker.LinkedItem = linkedItem;
                                        marker.Duration = markerDuration;
                                        //Link marker to timeline
                                        s.TimeLine.Markers.Add(marker);
                                    }
                                }
                            }
                        }

                        //For each sequence check the active bones
                        XElement activeBonesTab = sequence.Element(XName.Get("ActiveBones"));
                        if (activeBonesTab != null)
                        {
                            //Get all the bones
                            IEnumerable<XElement> activeBones = activeBonesTab.Descendants(XName.Get("Bone"));
                            if (activeBones != null)
                            {
                                s.BonesList.Clear(); //Clear list
                                foreach (XElement bone in activeBones)
                                {
                                    string activeBoneName = bone.Attribute(XName.Get("Name")).Value;
                                    bool isActiveBone = bool.Parse(bone.Attribute(XName.Get("Active")).Value);

                                    XAttribute offsetAtt = bone.Attribute(XName.Get("AllowedOffset"));

                                    float allowedOffset = offsetAtt == null ? 0f : float.Parse(offsetAtt.Value);

                                    //Add new list item
                                    ListItem li = new ListItem(activeBoneName, allowedOffset);
                                    li.Active = isActiveBone;
                                    s.BonesList.Add(li);
                                }
                            }
                        }

                        //Add the sequence to the list in the editor
                        GestureEditor.Instance.AddSequence(s);
                    }
                }
            }

            GestureEditor.Instance.ClearMatchPoses();
            XElement MatchPoseElement = root.Element(XName.Get("MatchPoses"));
            if (MatchPoseElement != null)
            {
                foreach (XElement matchPose in MatchPoseElement.Elements(XName.Get("MatchPose")))
                {
                    string name = matchPose.Attribute(XName.Get("Name")).Value;
                    string linkedPoseName = matchPose.Attribute(XName.Get("LinkedPose")).Value;
                    MatchPose m = new MatchPose(name);
                    m.Pose = GestureEditor.Instance.Poses.Find(x => x.Name.Equals(linkedPoseName));

                    XAttribute ignoreRotAtt = matchPose.Attribute(XName.Get("IgnoreBoneRotation"));
                    m.IgnoreBoneRotation = ignoreRotAtt == null ? false : bool.Parse(ignoreRotAtt.Value);

                    //check the active bones
                    XElement activeBonesTab = matchPose.Element(XName.Get("ActiveBones"));
                    if (activeBonesTab != null)
                    {
                        //Get all the bones
                        IEnumerable<XElement> activeBones = activeBonesTab.Descendants(XName.Get("Bone"));
                        if (activeBones != null)
                        {
                            m.BonesList.Clear(); //Clear list
                            foreach (XElement bone in activeBones)
                            {
                                string activeBoneName = bone.Attribute(XName.Get("Name")).Value;
                                bool isActiveBone = bool.Parse(bone.Attribute(XName.Get("Active")).Value);

                                XAttribute offsetAtt = bone.Attribute(XName.Get("AllowedOffset"));
                                float allowedOffset = offsetAtt == null ? 0f : float.Parse(offsetAtt.Value);

                                //Add new list item
                                ListItem li = new ListItem(activeBoneName, allowedOffset);

                                li.Active = isActiveBone;
                                m.BonesList.Add(li);
                            }
                        }
                    }

                    GestureEditor.Instance.AddMatchPose(m);
                }
            }
        }
    }
#endif
    public List<Pose> LoadPosesInGame(string filePath)
    {
        //If no path given
        if (filePath == "")
            return null;

        TextAsset xmlAsset = Resources.Load<TextAsset>(filePath);
        if (xmlAsset == null) return null;

        //Load file if any
        XDocument xml = XDocument.Load(new System.IO.MemoryStream(xmlAsset.bytes));
        if (xml != null)
        {
            //Get the root
            XElement root = xml.Root;

            //Start by getting the poses
            XElement posesElement = root.Element(XName.Get("Poses"));
            if (posesElement != null)
            {
                //Foreach pose
                IEnumerable<XElement> poseTabs = posesElement.Descendants(XName.Get("Pose"));
                if (poseTabs != null)
                {
                    List<Pose> poses = new List<Pose>();
                    //Foreach pose
                    foreach (XElement pose in poseTabs)
                    {
                        //Create a clean pose
                        Pose p = new Pose("");
                        //Get the attributes and fill them in
                        p.Name = pose.Attribute(XName.Get("Name")).Value;
                        p.Duration = float.Parse(pose.Attribute(XName.Get("Duration")).Value);

                        HandTrackingState leftHandState = HandTrackingState.Unknown;
                        HandTrackingState rightHandState = HandTrackingState.Unknown;

                        if (pose.Attribute(XName.Get("LeftHandState")) != null && pose.Attribute(XName.Get("RightHandState")) != null)
                        {
                            leftHandState = (HandTrackingState)Enum.Parse(typeof(HandTrackingState), pose.Attribute(XName.Get("LeftHandState")).Value);
                            rightHandState = (HandTrackingState)Enum.Parse(typeof(HandTrackingState), pose.Attribute(XName.Get("RightHandState")).Value);
                        }

                        //Create a new snapshot and fill the bones
                        IEnumerable<XElement> bones = pose.Descendants(XName.Get("Bone"));
                        if (bones != null)
                        {
                            List<Bone> bonesList = new List<Bone>();
                            foreach (XElement bone in bones)
                            {
                                string boneName = bone.Attribute(XName.Get("Name")).Value;
                                TrackingState state = (TrackingState)Enum.Parse(typeof(TrackingState), bone.Attribute(XName.Get("TrackingState")).Value);
                                Vector3 startPosition = new Vector3(
                                    float.Parse(bone.Attribute(XName.Get("StartPositionX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("StartPositionY")).Value),
                                    float.Parse(bone.Attribute(XName.Get("StartPositionZ")).Value));
                                Vector3? endPosition = new Vector3(
                                    float.Parse(bone.Attribute(XName.Get("EndPositionX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("EndPositionY")).Value),
                                    float.Parse(bone.Attribute(XName.Get("EndPositionZ")).Value));
                                if (endPosition.Value.x == 0.0f && endPosition.Value.y == 0.0f && endPosition.Value.z == 0.0f)
                                    endPosition = null;
                                Vector4 orientation = new Vector4(
                                    float.Parse(bone.Attribute(XName.Get("OrientationX")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationY")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationZ")).Value),
                                    float.Parse(bone.Attribute(XName.Get("OrientationW")).Value));
                                Bone b = new Bone(startPosition, endPosition, orientation, state, boneName);
                                bonesList.Add(b);
                            }
                            //If we have bones, create a snapshot and fill it with the received bones
                            if (bonesList.Count > 0)
                            {
                                SnapShot snapShot = new SnapShot(bonesList, leftHandState, rightHandState);
                                p.SnapShot = snapShot;

                                //And add it as a valid pose
                                poses.Add(p);
                            }
                        }
                    }

                    //When all poses are filled in, return them
                    return poses;
                }
                else
                    return null;
            }
            else
                return null;
        }
        else
            return null;
    }

    public List<RawComparableSequence> LoadRawSequencesInGame(string filePath)
    {
        //If no path given
        if (filePath == "")
            return null;

        TextAsset xmlAsset = Resources.Load<TextAsset>(filePath);
        if (xmlAsset == null) return null;

        //Load file if any
        XDocument xml = XDocument.Load(new System.IO.MemoryStream(xmlAsset.bytes));
        if (xml != null)
        {
            //Get the root
            XElement root = xml.Root;

            //After reading the poses, read the sequences
            XElement sequenceElement = root.Element(XName.Get("Sequences"));
            if (sequenceElement != null)
            {
                //For each sequence
                IEnumerable<XElement> sequenceTabs = sequenceElement.Descendants(XName.Get("Sequence"));
                if (sequenceTabs != null)
                {
                    List<RawComparableSequence> comparableSequences = new List<RawComparableSequence>();
                    foreach (XElement sequence in sequenceTabs)
                    {
                        //Get the attributes
                        string sequenceName = sequence.Attribute(XName.Get("Name")).Value;
                        float duration = float.Parse(sequence.Attribute(XName.Get("Duration")).Value);
                        float interlude = float.Parse(sequence.Attribute(XName.Get("Interlude")).Value);
                        bool trackHandstate = sequence.Attribute(XName.Get("TrackHandstate")) != null && bool.Parse(sequence.Attribute(XName.Get("TrackHandstate")).Value);

                        TransitionType transition = (TransitionType)Enum.Parse(typeof(TransitionType), sequence.Attribute(XName.Get("TransitionType")).Value);
                        SequenceType sequenceType = (SequenceType)Enum.Parse(typeof(SequenceType), sequence.Attribute(XName.Get("SequenceType")).Value);
                        InvokeType invokeType = (InvokeType)Enum.Parse(typeof(InvokeType), sequence.Attribute(XName.Get("InvokeType")).Value);
                        PoseAxis axis = (PoseAxis)Enum.Parse(typeof(PoseAxis), sequence.Attribute(XName.Get("PoseAxis")).Value);

                        //Create a clean sequence and fill in the attributes
                        RawComparableSequence s = new RawComparableSequence();
                        s.Name = sequenceName;
                        s.Interlude = interlude;
                        s.TrackHandstate = trackHandstate;

                        s.TransitionType = transition;
                        s.SequenceType = sequenceType;
                        s.InvokeType = invokeType;
                        s.PoseAxis = axis;
                        s.Duration = duration;

                        //For each sequence check the linked poses and add them to the timeline
                        XElement linkedPosesTab = sequence.Element(XName.Get("LinkedPoses"));
                        if (linkedPosesTab != null)
                        {
                            IEnumerable<XElement> linkedPoses = linkedPosesTab.Descendants(XName.Get("LinkedPose"));
                            if (linkedPoses != null)
                            {
                                List<RawLinkedPose> rawLinkedPoses = new List<RawLinkedPose>();
                                foreach (XElement linkedPose in linkedPoses)
                                {
                                    string poseName = linkedPose.Attribute(XName.Get("Name")).Value;
                                    float poseStartTime = float.Parse(linkedPose.Attribute(XName.Get("StartTime")).Value);
                                    float poseDuration = float.Parse(linkedPose.Attribute(XName.Get("Duration")).Value);

                                    RawLinkedPose rawLinkedPose = new RawLinkedPose();
                                    rawLinkedPose.Name = poseName;
                                    rawLinkedPose.StartTime = poseStartTime;
                                    rawLinkedPose.Duration = poseDuration;

                                    //Add raw pose to list
                                    rawLinkedPoses.Add(rawLinkedPose);
                                }

                                //Add raw poses to raw sequence
                                s.RawLinkedPoses = rawLinkedPoses;
                            }
                        }

                        //For each sequence check the active bones
                        XElement activeBonesTab = sequence.Element(XName.Get("ActiveBones"));
                        if (activeBonesTab != null)
                        {
                            //Get all the bones
                            IEnumerable<XElement> activeBones = activeBonesTab.Descendants(XName.Get("Bone"));
                            if (activeBones != null)
                            {
                                List<ActiveBoneInformation> activeBoneInfos = new List<ActiveBoneInformation>();
                                foreach (XElement bone in activeBones)
                                {
                                    string activeBoneName = bone.Attribute(XName.Get("Name")).Value;
                                    bool isActiveBone = bool.Parse(bone.Attribute(XName.Get("Active")).Value);
                                    XAttribute att = bone.Attribute(XName.Get("AllowedOffset"));
                                    float offset = 0f;

                                    if (att != null)
                                    {
                                        offset = float.Parse(att.Value, System.Globalization.CultureInfo.InvariantCulture);
                                    }


                                    //Add new list item
                                    if (isActiveBone == true)
                                        activeBoneInfos.Add(new ActiveBoneInformation(activeBoneName, offset));
                                }
                                //Add the list to the raw sequence
                                s.ActiveBones = activeBoneInfos;
                            }
                        }

                        //Add the raw sequence
                        comparableSequences.Add(s);
                    }
                    //Return all the raw sequences that have been loaded
                    return comparableSequences;
                }
                else
                    return null;
            }
            else
                return null;
        }
        else
            return null;
    }

    public List<RawComparableMatchPose> LoadRawMatchPosesInGame(string filePath)
    {
        //If no path given
        if (filePath == "")
            return null;

        TextAsset xmlAsset = Resources.Load<TextAsset>(filePath);
        if (xmlAsset == null) return null;

        //Load file if any
        XDocument xml = XDocument.Load(new System.IO.MemoryStream(xmlAsset.bytes));
        if (xml != null)
        {
            //Get the root
            XElement root = xml.Root;

            //After reading the poses, read the matchposes
            XElement sequenceElement = root.Element(XName.Get("MatchPoses"));
            if (sequenceElement != null)
            {
                //For each matchpose
                IEnumerable<XElement> matchPoseTabs = sequenceElement.Descendants(XName.Get("MatchPose"));
                if (matchPoseTabs != null)
                {
                    List<RawComparableMatchPose> comparableMatchPoses = new List<RawComparableMatchPose>();
                    foreach (XElement matchPose in matchPoseTabs)
                    {
                        //Get the attributes
                        string matchPoseName = matchPose.Attribute(XName.Get("Name")).Value;

                        //Create a clean matchpose and fill in the attributes
                        RawComparableMatchPose m = new RawComparableMatchPose();
                        m.Name = matchPoseName;


                        //get the linked pose
                        string poseName = matchPose.Attribute(XName.Get("LinkedPose")).Value;
                        m.RawLinkedPose = new RawLinkedPose() { Name = poseName };

                        XAttribute ignoreRotAtt = matchPose.Attribute(XName.Get("IgnoreBoneRotation"));
                        m.IgnoreBoneRotation = ignoreRotAtt == null ? false : bool.Parse(ignoreRotAtt.Value);

                        //For each matchpose check the active bones
                        XElement activeBonesTab = matchPose.Element(XName.Get("ActiveBones"));
                        if (activeBonesTab != null)
                        {
                            //Get all the bones
                            IEnumerable<XElement> activeBones = activeBonesTab.Descendants(XName.Get("Bone"));
                            if (activeBones != null)
                            {
                                List<ActiveBoneInformation> activeBoneInfos = new List<ActiveBoneInformation>();
                                foreach (XElement bone in activeBones)
                                {
                                    string activeBoneName = bone.Attribute(XName.Get("Name")).Value;
                                    bool isActiveBone = bool.Parse(bone.Attribute(XName.Get("Active")).Value);
                                    XAttribute att = bone.Attribute(XName.Get("AllowedOffset"));
                                    float offset = 0f;

                                    if (att != null)
                                    {
                                        offset = float.Parse(att.Value, System.Globalization.CultureInfo.InvariantCulture);
                                    }


                                    //Add new list item
                                    if (isActiveBone == true)
                                        activeBoneInfos.Add(new ActiveBoneInformation(activeBoneName, offset));
                                }
                                //Add the list to the raw sequence
                                m.ActiveBones = activeBoneInfos;
                            }
                        }

                        //Add the raw sequence
                        comparableMatchPoses.Add(m);
                    }
                    //Return all the raw sequences that have been loaded
                    return comparableMatchPoses;
                }
                else
                    return null;
            }
            else
                return null;
        }
        else
            return null;
    }
}
#endif