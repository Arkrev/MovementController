﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;

public class GestureEditor : EditorWindow
{
    private enum SelectedItemType
    {
        MatchPose,
        Sequence
    }
    //FIELDS
    private GUISkin _editorSkin = null; //Skin for our editor

    private ScrolList _posesList = new ScrolList(500.0f, 210f); //List of available poses
    private ScrolList _sequenceList = new ScrolList(500.0f, 210f); //List of created sequences
    private ScrolList _bonesList = new ScrolList(652.5f, 200.0f); //List of bones
    private ScrolList _matchPoseList = new ScrolList(500.0f, 200f); //List of MatchPoses

    private SelectedItemType _currentSelectionType = SelectedItemType.Sequence;

    private RecordDevice _currentDevice = RecordDevice.Kinect; //Used to determine which bones are available and how we will record
    private bool _synchedBones = false;

    private string _logText = ""; //The log text we preview
    private int _logCount = 0; //Amount log, used to clear log area
    private int _logMaxCount = 18; //Amount of logs in one window allowed. If exceeded, log will be cleared
    private bool _loadedContent = false; //Check if loaded content from files

    private float _standardAllowedOffset = 35.0f; //The standard offset of all the bones when created

    //STATIC FIELDS
    private static GameObject _objectContainer = null; //Container holding gameobjects that are being created in the scene
    private static GameObject _objectRecorder = null; //Object that holds our current recorder
    private static GameObject _objectSerializer = null; //Object that holds our current serializer

    //PROPERTIES
    public static GestureEditor Instance { get; private set; } //Instance of this editor
    public Pose SelectedPose { get; set; } //The current selected pose (list and timeline)
    public Pose SelectedPoseInTimeLine { get; set; } //The pose selected in the timeline
    public Sequence SelectedSequence { get; set; } //The current selected sequence in the list

    public MatchPose SelectedMatchPose { get; set; }
    public RecordDevice CurrentDevice { get { return _currentDevice; } set { _currentDevice = value; } } //The current recording device

    private bool _WaitForSnapshot;
    private float _waitForSnapshotTime = 0f;

    private int _numTimesHandsClosed = 0;
    private float _snapshotGestureTimer = 0f;

    public List<Pose> Poses //All the poses
    {
        get
        {
            List<Pose> poses = new List<Pose>();
            foreach (ListItem item in _posesList.Items)
            {
                if (item is Pose)
                    poses.Add((Pose)item.Clone());
            }
            return poses;
        }
    }
    public List<Sequence> Sequences //All the sequences
    {
        get
        {
            List<Sequence> sequences = new List<Sequence>();
            foreach (ListItem item in _sequenceList.Items)
            {
                if (item is Sequence)
                    sequences.Add((Sequence)item.Clone());
            }
            return sequences;
        }
    }
    public List<MatchPose> MatchPoses //All the MatchPoses
    {
        get
        {
            List<MatchPose> matchposes = new List<MatchPose>();
            foreach (ListItem item in _matchPoseList.Items)
            {
                if (item is MatchPose)
                    matchposes.Add((MatchPose)item.Clone());
            }
            return matchposes;
        }
    }
    void UpdateRemoteSnapshot()
    {
        if (_waitForSnapshotTime > 0f)
        {
            _waitForSnapshotTime -= Time.deltaTime * 0.5f;

            if (_waitForSnapshotTime <= 0f)
            {
                //TAKE SNAPSHOT
                Snapshot();
                _waitForSnapshotTime = 0f;
                _WaitForSnapshot = false;
            }

        }
        else
        {
            bool leftOpen = false;
            bool leftClosed = false;
            if (KinectGestureRecorder.Instance.KnownIDs.Count > 0)
            {
                KinectBody kBody = KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.First(), true);
                if (kBody.LeftHandState == HandTrackingState.Closed)
                {
                    leftClosed = true;
                }
                else if (kBody.LeftHandState == HandTrackingState.Open)
                {
                    leftOpen = true;
                }
            }

            //open and close twice within 1 second to start snapshot timer
            switch (_numTimesHandsClosed)
            {
                //first time, don't count down time
                case 0:
                    if (leftClosed)
                    {
                        _numTimesHandsClosed = 1;
                        _snapshotGestureTimer = 2f;
                    }

                    break;
                default:
                    _snapshotGestureTimer -= Time.deltaTime;
                    if (_snapshotGestureTimer < 0f)
                    {
                        _snapshotGestureTimer = 0f;
                        _numTimesHandsClosed = 0;
                        break;
                    }

                    //open 1-> 2
                    //closed 2->3
                    if (_numTimesHandsClosed % 2 == 0 && leftClosed)
                    {
                        ++_numTimesHandsClosed;
                    }
                    else if (_numTimesHandsClosed % 2 == 1 && leftOpen)
                    {
                        ++_numTimesHandsClosed;
                    }

                    if (_numTimesHandsClosed >= 4)
                    {
                        //SNAPSHOT TIMER
                        _waitForSnapshotTime = 3f;
                        _numTimesHandsClosed = 0;
                    }

                    break;
            }
        }

    }
    void Update()
    {
        //------- ITEM FROM LISTS --------
        //Get selected items if any
        SelectedPose = (Pose)_posesList.GetSelectedItem();
        if (SelectedPoseInTimeLine != null)
            SelectedPose = SelectedPoseInTimeLine;
        //Store previous selected sequence, get the maybe new one and check if it is the same
        //If not the same resynch the bones

        SelectedItemType prevType = _currentSelectionType;

        Sequence prevSequence = SelectedSequence;
        SelectedSequence = (Sequence)_sequenceList.GetSelectedItem();

        MatchPose prevMatchPose = SelectedMatchPose;
        SelectedMatchPose = (MatchPose)_matchPoseList.GetSelectedItem();

        if (SelectedMatchPose != prevMatchPose)
        {
            _currentSelectionType = SelectedItemType.MatchPose;
        }
        else if (SelectedSequence != prevSequence)
        {
            _currentSelectionType = SelectedItemType.Sequence;
        }


        if (prevType != _currentSelectionType)
        {
            _synchedBones = false;
        }
        else if (_currentSelectionType == SelectedItemType.Sequence)
        {
            if (SelectedSequence != null && prevSequence == null)
                _synchedBones = false;
            else if (prevSequence != null && !prevSequence.Equals(SelectedSequence))
                _synchedBones = false;
        }
        else if (_currentSelectionType == SelectedItemType.MatchPose)
        {
            if (SelectedMatchPose != null && !SelectedMatchPose.Equals(prevMatchPose))
            {
                _synchedBones = false;

            }
        }

        //Read the possible bones if not done already
        if (_synchedBones == false)
        {
            if (_currentSelectionType == SelectedItemType.Sequence)
            {
                if (prevSequence != null)
                {
                    //prevSequence = SelectedSequence;
                    prevSequence.BonesList.Clear();
                    foreach (ListItem item in _bonesList.Items)
                    {
                        prevSequence.BonesList.Add((ListItem)item.Clone());
                    }
                }

                _bonesList.Clear();
                switch (_currentDevice)
                {
                    case RecordDevice.Kinect:
                        bool emptyAtStart = true;
                        if (SelectedSequence != null)
                            emptyAtStart = SelectedSequence.BonesList.Count == 0;
                        SynchKinectBones();
                        _bonesList.MultipleSelectionAllowed = true;
                        if (emptyAtStart)
                            _bonesList.SelectAll();
                        break;
                }
            }
            else
            {
                if (prevMatchPose != null)
                {
                    //prevSequence = SelectedSequence;
                    prevMatchPose.BonesList.Clear();
                    foreach (ListItem item in _bonesList.Items)
                    {
                        prevMatchPose.BonesList.Add((ListItem)item.Clone());
                    }
                }

                _bonesList.Clear();
                switch (_currentDevice)
                {
                    case RecordDevice.Kinect:
                        bool emptyAtStart = true;
                        if (SelectedMatchPose != null)
                            emptyAtStart = SelectedMatchPose.BonesList.Count == 0;
                        SynchKinectBones();
                        _bonesList.MultipleSelectionAllowed = true;
                        if (emptyAtStart)
                            _bonesList.SelectAll();
                        break;
                }
            }
            _synchedBones = true;
        }



    }

    void Snapshot()
    {
        if (SelectedPose == null) return;
        List<Bone> bones = new List<Bone>();
        foreach (Bone b in KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.First()).Bones)
        {
            Bone bone = new Bone(b.StartPosition, b.EndPosition, b.Orientation, b.TrackingState, b.Name);
            bones.Add(bone);
        }
        SnapShot snapshot = new SnapShot(bones);

        //Find all reference using this pose (in timelines and adjust them)
        foreach (Sequence seq in _sequenceList.Items)
        {
            foreach (TimeMarker m in seq.TimeLine.Markers)
            {
                if (m.LinkedItem.Name.Equals(SelectedPose.Name))
                {
                    if (m.LinkedItem is Pose)
                    {
                        ((Pose)m.LinkedItem).SnapShot = snapshot;
                    }
                }
            }
        }
        //Adjust matchposes
        foreach (MatchPose p in _matchPoseList.Items)
        {
            if (p.Name.Equals(SelectedPose.Name))
            {
                p.Pose.SnapShot = snapshot;
            }
        }

        SelectedPose.SnapShot = snapshot;
        Log("Created snapshot for pose '" + SelectedPose.Name + "'");
    }
    //DESTRUCTOR -- Used for destroying gameobjects and clear textures from memory
    void OnDestroy()
    {
        //Destroy sequences
        foreach (Sequence s in _sequenceList.Items)
        {
            s.DestroyImmediate();
        }
        _sequenceList.Clear();
        _posesList.Clear();
        _matchPoseList.Clear();

        //Destroy objects
#if UNITY_KINECT
        KinectGestureRecorder recorder = null;
        if (_objectRecorder != null)
            recorder = _objectRecorder.GetComponent<KinectGestureRecorder>();
        if (recorder)
            recorder.DestroyImmediate();
#endif

        GameObject.DestroyImmediate(_objectRecorder);
        GameObject.DestroyImmediate(_objectSerializer);
        GameObject.DestroyImmediate(_objectContainer);
    }

    //METHODS
#if UNITY_KINECT
    [MenuItem("GestureEditor/Editor")]
    public static void ShowWindow()
    {
        GestureEditor window = EditorWindow.GetWindow(typeof(GestureEditor)) as GestureEditor;
        window.title = "Gesture Editor";
        window.position = new Rect(0, 0, Screen.currentResolution.width, Screen.currentResolution.height);
        Instance = window;

        //Create the required managers and object untill we close our window (+ parent them in a clean structure)
        _objectContainer = new GameObject("GestureEditor");
        _objectContainer.transform.position = Vector3.zero;

        _objectRecorder = new GameObject("KinectGestureRecorder");
        _objectRecorder.AddComponent<KinectGestureRecorder>();
        _objectRecorder.transform.parent = _objectContainer.transform;

        _objectSerializer = new GameObject("GestureEditorSerializer");
        _objectSerializer.AddComponent<GestureEditorSerializer>();
        _objectSerializer.transform.parent = _objectContainer.transform;

        //Read config information and setup editor using it's values
        GestureEditorSerializer.Instance.LoadConfig();

        //Enable recording by default
        KinectGestureRecorder.Instance.Record = true;
    }
#endif

#if UNITY_KINECT
    void OnGUI()
    {
        //Constant update by setting the container dirty
        if (_objectContainer != null)
            EditorUtility.SetDirty(_objectContainer);
        else
        {
            _objectContainer = GameObject.Find("GestureEditor");
            _objectRecorder = GameObject.Find("KinectGestureRecorder");
            _objectSerializer = GameObject.Find("GestureEditorSerializer");

            if (_objectContainer == null)
            {
                _objectContainer = new GameObject("GestureEditor");
                _objectContainer.transform.position = Vector3.zero;
            }
            if (_objectRecorder == null)
            {
                _objectRecorder = new GameObject("KinectGestureRecorder");
                _objectRecorder.AddComponent<KinectGestureRecorder>();
            }
            _objectRecorder.transform.parent = _objectContainer.transform;

            if (_objectSerializer == null)
            {
                _objectSerializer = new GameObject("GestureEditorSerializer");
                _objectSerializer.AddComponent<GestureEditorSerializer>();
            }
            _objectSerializer.transform.parent = _objectContainer.transform;
        }


        //Load content, if any
        if (_loadedContent == false)
        {
            GestureEditorSerializer.Instance.LoadEditor(Application.dataPath + "/Resources/GestureEditorSave.xml");
            _loadedContent = true;
        }

        //--- Load Skins ---
        if (_editorSkin == null)
            _editorSkin = Resources.Load<GUISkin>("GestureEditor");
        GUI.skin = _editorSkin;

        //------- TIMELINE --------

        Rect timelineRect = EditorGUILayout.GetControlRect(GUILayout.Height(120)); //Push newest ControlRect
        if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            SelectedSequence.TimeLine.Rect = timelineRect;
        GUILayout.Space(25.0f);



        //------- PARAMETER GUI -----
        //TimeLine Duration adjustment
        if (_currentSelectionType == SelectedItemType.Sequence)
        {
            if (SelectedSequence == null)
                GUI.enabled = false;
            float xOffset = 10.0f;
            Rect controlTimeLineRect = EditorGUILayout.GetControlRect(GUILayout.Height(30));
            GUI.Label(new Rect(controlTimeLineRect.x + 5.0f, controlTimeLineRect.y, controlTimeLineRect.width - 10.0f, controlTimeLineRect.height), "", "GroupBox01");
            GUI.Label(new Rect(controlTimeLineRect.x + xOffset, controlTimeLineRect.y + 2.5f, controlTimeLineRect.width / 2.0f, 25.0f), "Total Duration: ");
            if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            {
                SelectedSequence.TimeLine.TotalDuration = EditorGUI.FloatField(new Rect(controlTimeLineRect.x + xOffset + 90.0f, controlTimeLineRect.y + 5.0f, 30.0f, 20.0f), SelectedSequence.TimeLine.TotalDuration);
                SelectedSequence.TimeLine.TotalDuration = GUI.HorizontalSlider(new Rect(controlTimeLineRect.x + xOffset + 130.0f, controlTimeLineRect.y + 7.5f, controlTimeLineRect.width - 155.0f, 25.0f), SelectedSequence.TimeLine.TotalDuration, 1.0f, 15.0f);
            }
            else
            {
                EditorGUI.FloatField(new Rect(controlTimeLineRect.x + xOffset + 90.0f, controlTimeLineRect.y + 5.0f, 30.0f, 20.0f), 1.0f);
                GUI.HorizontalSlider(new Rect(controlTimeLineRect.x + xOffset + 130.0f, controlTimeLineRect.y + 7.5f, controlTimeLineRect.width - 155.0f, 25.0f), 1.0f, 1.0f, 15.0f);
            }
            GUI.enabled = true;
        }

        //Second horinzontal layout
        GUILayout.Space(10.0f);
        GUILayout.BeginHorizontal();
        GUILayout.Space(10.0f);

        //----------------- LEFT COL -----------------
        //GUILayout.FlexibleSpace();
        GUILayout.BeginVertical(GUILayout.Width((Screen.width - 20.0f) / 3 + 45.0f));

        GUILayout.BeginHorizontal("GroupBox01");
        _editorSkin.label.alignment = TextAnchor.UpperCenter;
        GUILayout.Label("<size=15>Snapshot</size>", GUILayout.Height(23.0f));
        _editorSkin.label.alignment = TextAnchor.UpperLeft;
        GUILayout.EndHorizontal();

        GUILayout.Space(10.0f);

        GUILayout.BeginHorizontal("GroupBox01");

        GUILayout.Label("", GUILayout.Height(500.0f));

        if (_WaitForSnapshot && _waitForSnapshotTime > 0f)
        {
            GUI.Label(new Rect(GUILayoutUtility.GetLastRect().x + 10.0f, GUILayoutUtility.GetLastRect().y + 40.0f, GUILayoutUtility.GetLastRect().width - 20.0f, GUILayoutUtility.GetLastRect().height - 50.0f), String.Format("<size=45>{0}</size>", (int)_waitForSnapshotTime + 1), "CountdownRecordScreen");

        }
        else if (_WaitForSnapshot)
        {
            GUI.Label(new Rect(GUILayoutUtility.GetLastRect().x + 10.0f, GUILayoutUtility.GetLastRect().y + 40.0f, GUILayoutUtility.GetLastRect().width - 20.0f, GUILayoutUtility.GetLastRect().height - 50.0f), "", "ActiveRecordScreen");

        }
        else
        {
            GUI.Label(new Rect(GUILayoutUtility.GetLastRect().x + 10.0f, GUILayoutUtility.GetLastRect().y + 40.0f, GUILayoutUtility.GetLastRect().width - 20.0f, GUILayoutUtility.GetLastRect().height - 50.0f), "", "InactiveRecordScreen");

        }

        GUILayout.EndHorizontal();

        if (SelectedPoseInTimeLine != null || SelectedPose == null || (SelectedPose != null && SelectedPose.SnapShot == null))
            GUI.enabled = false;
        if (GUI.Button(new Rect(390, 235, 75, 20), "Delete"))
        {
            if (SelectedPose != null && SelectedPose.SnapShot != null)
            {
                //Find all reference using this pose (in timelines and adjust them)
                foreach (Sequence seq in _sequenceList.Items)
                {
                    foreach (TimeMarker m in seq.TimeLine.Markers)
                    {
                        if (m.LinkedItem.Name.Equals(SelectedPose.Name))
                        {
                            if (m.LinkedItem is Pose)
                            {
                                ((Pose)m.LinkedItem).SnapShot = null;
                            }
                        }
                    }
                }

                SelectedPose.SnapShot = null;
                Log("Deleted snapshot from pose '" + SelectedPose.Name + "'");
            }
        }
        GUI.enabled = true;
        if (SelectedPoseInTimeLine != null || SelectedPose == null || (SelectedPose != null && SelectedPose.SnapShot != null))
            GUI.enabled = false;
        if (_WaitForSnapshot)
        {
            GUI.Label(new Rect(468, 233, 124, 24), "", "ActiveRecordScreen");
        }
        else
        {
            GUI.Label(new Rect(0, 0, 0, 0), "", "InactiveRecordScreen");
        }
        if (GUI.Button(new Rect(470, 235, 120, 20), "Remote Snapshot"))
        {
            _WaitForSnapshot = !_WaitForSnapshot;
        }
        if (GUI.Button(new Rect(595, 235, 75, 20), "Snapshot"))
        {
            if (SelectedPose != null && SelectedPose.SnapShot == null && KinectGestureRecorder.Instance.Record == true && KinectGestureRecorder.Instance.KnownIDs.Count > 0)
            {
                Snapshot();
            }
        }
        GUI.enabled = true;

        //Controls
        GUILayout.Space(10.0f);
        GUILayout.BeginHorizontal("GroupBox01");
        _editorSkin.label.alignment = TextAnchor.UpperCenter;
        GUILayout.Label("<size=15>Controls</size>", GUILayout.Height(23.0f));
        _editorSkin.label.alignment = TextAnchor.UpperLeft;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal("GroupBox01", GUILayout.Height(222.0f));
        GUILayout.Space(10.0f);
        GUILayout.BeginVertical();
        GUILayout.Space(10.0f);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Recording Device:", GUILayout.Width(140.0f));
        RecordDevice _prevDevice = _currentDevice;
        _currentDevice = (RecordDevice)EditorGUILayout.EnumPopup(_currentDevice, GUILayout.Width(100.0f));
        if (!_currentDevice.Equals(_prevDevice)) //If no type, recheck the bones
        {
            _synchedBones = false;
        }
        GUILayout.EndHorizontal();
        GUILayout.Space(10.0f);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            string path = UnityEditor.EditorUtility.SaveFilePanel("Save File", "dir", "GestureEditorSave", "xml");
            GestureEditorSerializer.Instance.SaveEditor(path);
        }
        if (GUILayout.Button("Load"))
        {
            string path = UnityEditor.EditorUtility.OpenFilePanel("Load File", "dir", "xml");
            GestureEditorSerializer.Instance.LoadEditor(path);
        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUILayout.Space(10.0f);
        GUILayout.EndHorizontal();

        //484 * 456
        GUILayout.EndVertical();
        GUILayout.Space(10.0f);
        //-------------------------------------------

        //----------------- CENTER COL -----------------
        GUILayout.BeginVertical(GUILayout.Width((Screen.width - 20.0f) / 3 + 45.0f));

        //Pose Parameters
        GUILayout.BeginHorizontal("GroupBox01");
        _editorSkin.label.alignment = TextAnchor.UpperCenter;
        GUILayout.Label("<size=15>Pose Parameters</size>", GUILayout.Height(23.0f));
        _editorSkin.label.alignment = TextAnchor.UpperLeft;
        GUILayout.EndHorizontal();

        GUILayout.Space(10.0f);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Pose Name:", GUILayout.Width(140.0f));
        if (SelectedPose != null)
        {
            if (SelectedPoseInTimeLine != null)
                GUI.enabled = false;

            string prevName = SelectedPose.Name;
            SelectedPose.Name = EditorGUILayout.TextField(SelectedPose.Name, GUILayout.Width(200.0f));

            if (!SelectedPose.Name.Equals(prevName))
            {
                //Check if name is allready taken, if it does name it unnamed
                bool allowed = true;
                foreach (Pose p in _posesList.Items)
                {
                    if (!p.Equals(SelectedPose) && p.Name.Equals(SelectedPose.Name))
                    {
                        Log("ERROR: name '" + SelectedPose.Name + "' already taken");
                        SelectedPose.Name = "Unnamed";
                        allowed = false;
                    }
                }
                if (allowed == true)
                {
                    //Find all reference using this pose (in timelines and adjust them)
                    foreach (Sequence seq in _sequenceList.Items)
                    {
                        foreach (TimeMarker m in seq.TimeLine.Markers)
                        {
                            if (m.LinkedItem.Name.Equals(prevName))
                            {
                                m.LinkedItem.Name = SelectedPose.Name;
                            }
                        }
                    }
                }
            }

            GUI.enabled = true;
            Repaint();
        }
        else
        {
            GUI.enabled = false;
            EditorGUILayout.TextField("", GUILayout.Width(200.0f));
            GUI.enabled = true;
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Duration:", GUILayout.Width(140.0f));
        if (SelectedPose != null)
        {
            if (SelectedPoseInTimeLine == null || (SelectedSequence != null && SelectedSequence.SequenceType == SequenceType.Continuous))
            {
                GUI.enabled = true;
                float duration = EditorGUILayout.FloatField(SelectedPose.Duration, GUILayout.Width(200.0f));
                if (duration <= 0)
                    duration = 0.1f;
                SelectedPose.Duration = duration;
                if (SelectedSequence != null && SelectedSequence.TimeLine.SelectedMarker != null)
                    SelectedSequence.TimeLine.SelectedMarker.Duration = SelectedPose.Duration;
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.FloatField(SelectedPoseInTimeLine.Duration, GUILayout.Width(200.0f));
                GUI.enabled = true;
            }
        }
        else
        {
            GUI.enabled = false;
            EditorGUILayout.FloatField(0.0f, GUILayout.Width(200.0f));
            GUI.enabled = true;
        }
        GUILayout.EndHorizontal();


        GUILayout.Space(25.0f);

        if (_currentSelectionType == SelectedItemType.Sequence)
        {
            //Sequence Parameters
            GUILayout.BeginHorizontal("GroupBox01");
            _editorSkin.label.alignment = TextAnchor.UpperCenter;
            GUILayout.Label("<size=15>Sequence Parameters</size>", GUILayout.Height(23.0f));
            _editorSkin.label.alignment = TextAnchor.UpperLeft;
            GUILayout.EndHorizontal();

            GUILayout.Space(10.0f);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Sequence Name:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.Name = EditorGUILayout.TextField(SelectedSequence.Name, GUILayout.Width(200.0f));

                //Check if name is allready taken, if it does name it unnamed
                bool allowed = true;
                foreach (Sequence s in _sequenceList.Items)
                {
                    if (!s.Equals(SelectedSequence) && s.Name.Equals(SelectedSequence.Name))
                    {
                        Log("ERROR: name '" + SelectedSequence.Name + "' already taken");
                        SelectedSequence.Name = "Unnamed";
                        allowed = false;
                    }
                }
                if (allowed == true)
                    SelectedSequence.TimeLine.Name = SelectedSequence.Name;
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.TextField("", GUILayout.Width(200.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Sequence Type:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                //TODO: implement continues types in the KinectGesturekRecognizer: line under here is the final line needed for the editor
                //SelectedSequence.SequenceType = (SequenceType)EditorGUILayout.EnumPopup(SelectedSequence.SequenceType, GUILayout.Width(100.0f));
                SelectedSequence.SequenceType = (SequenceType)EditorGUILayout.EnumPopup(SequenceType.Static, GUILayout.Width(100.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.EnumPopup(SequenceType.Static, GUILayout.Width(100.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Invoke Type:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.InvokeType = (InvokeType)EditorGUILayout.EnumPopup(SelectedSequence.InvokeType, GUILayout.Width(100.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.EnumPopup(InvokeType.SingleInvoke, GUILayout.Width(100.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Transition Type:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.TransitionType = (TransitionType)EditorGUILayout.EnumPopup(SelectedSequence.TransitionType, GUILayout.Width(130.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.EnumPopup(TransitionType.Rotation, GUILayout.Width(130.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Axis:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.PoseAxis = (PoseAxis)EditorGUILayout.EnumMaskField((PoseAxis)SelectedSequence.PoseAxis, GUILayout.Width(100.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.EnumMaskField(PoseAxis.X, GUILayout.Width(100.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Track Handstate:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.TrackHandState = EditorGUILayout.Toggle(SelectedSequence.TrackHandState, GUILayout.Width(200.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.Toggle(true, GUILayout.Width(200.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();


            GUILayout.BeginHorizontal();
            GUILayout.Label("Interlude Poses:", GUILayout.Width(140.0f));
            if (SelectedSequence != null)
            {
                GUI.enabled = true;
                SelectedSequence.MaxInterludePoses = EditorGUILayout.FloatField(SelectedSequence.MaxInterludePoses, GUILayout.Width(200.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.FloatField(0.0f, GUILayout.Width(200.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Active Bones:", GUILayout.Width(140.0f));
            GUILayout.EndHorizontal();

            GUILayout.Space(5.0f);
            _bonesList.Draw();
        }
        else if (_currentSelectionType == SelectedItemType.MatchPose)
        {
            GUILayout.BeginHorizontal("GroupBox01");
            _editorSkin.label.alignment = TextAnchor.UpperCenter;
            GUILayout.Label("<size=15>MatchPose Parameters</size>", GUILayout.Height(23.0f));
            _editorSkin.label.alignment = TextAnchor.UpperLeft;
            GUILayout.EndHorizontal();

            GUILayout.Space(10.0f);

            GUILayout.BeginHorizontal();
            GUILayout.Label("MatchPose Name:", GUILayout.Width(140.0f));
            if (SelectedMatchPose != null)
            {
                GUI.enabled = true;
                SelectedMatchPose.Name = EditorGUILayout.TextField(SelectedMatchPose.Name, GUILayout.Width(200.0f));
                string name = SelectedMatchPose.Name;
                //Check if name is allready taken, if it does name it unnamed
                bool allowed = true;
                int index = 0;
                do
                {
                    allowed = true;
                    foreach (MatchPose s in _matchPoseList.Items)
                    {
                        if (!s.Equals(SelectedMatchPose) && s.Name.Equals(SelectedMatchPose.Name))
                        {
                            if (index == 0)
                            {
                                Log("ERROR: name '" + SelectedMatchPose.Name + "' already taken, adding index");
                            }

                            ++index;
                            SelectedMatchPose.Name = String.Format(name + " ({0})", index);

                            allowed = false;
                        }
                    }
                } while (!allowed);



            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.TextField("", GUILayout.Width(200.0f));
                GUI.enabled = true;
            }
            GUILayout.EndHorizontal();

            //Pose select
            string[] poseNames = _posesList.Items.Select(x => x.Name).ToArray();
            bool valid = true;
            if (poseNames.Length == 0)
            {
                valid = false;
                poseNames = new string[] { "No Poses" };
            }


            Pose selected = SelectedMatchPose != null && SelectedMatchPose.Pose != null ? _posesList.Items.FirstOrDefault(p => p.Name == SelectedMatchPose.Pose.Name) as Pose : null;
            int selectedindex = valid && selected != null ? _posesList.Items.IndexOf(selected) : 0;

            int prevIndex = selectedindex;
            selectedindex = Mathf.Max(selectedindex, 0);

            GUILayout.BeginHorizontal();
            GUILayout.Label("Pose:", GUILayout.Width(140.0f));

            GUI.enabled = SelectedMatchPose != null;
            selectedindex = EditorGUILayout.Popup(selectedindex, poseNames);
            GUI.enabled = true;
            if (valid && SelectedMatchPose != null && selectedindex < poseNames.Length && selectedindex != prevIndex)
            {
                Pose p = (Pose)_posesList.Items[selectedindex];
                if (p.Name == "Unnamed")
                {
                    Log("ERROR: Cannot use a Pose with the name 'Unnamed'");
                    selectedindex = 0;
                }
                else if (p.SnapShot == null)
                {
                    Log("ERROR: Cannot use a Pose that has no snapshot.");
                    selectedindex = 0;
                }
                else
                {
                    SelectedMatchPose.Pose = p;
                }
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Ignore Bone Rotation:", GUILayout.Width(140.0f));

            if (SelectedMatchPose == null)
            {
                GUILayout.Toggle(true, "");
            }
            else
            {
                SelectedMatchPose.IgnoreBoneRotation = GUILayout.Toggle(SelectedMatchPose.IgnoreBoneRotation, "");
            }

            GUILayout.EndHorizontal();


            GUILayout.Space(5.0f);
            _bonesList.Draw();


        }


        //Log
        GUILayout.Space(25.0f);
        GUILayout.BeginHorizontal("GroupBox01");
        _editorSkin.label.alignment = TextAnchor.UpperCenter;
        GUILayout.Label("<size=15>Log</size>", GUILayout.Height(23.0f));
        _editorSkin.label.alignment = TextAnchor.UpperLeft;
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal("GroupBox01");
        //GUILayout.Label("", GUILayout.Height(255.0f));
        /*if (_logoTexture != null)
            GUI.DrawTexture(new Rect(GUILayoutUtility.GetLastRect().x + 6.0f, GUILayoutUtility.GetLastRect().y + 5.0f, _logoTexture.width * ((GUILayoutUtility.GetLastRect().height - 10.0f) / _logoTexture.height), GUILayoutUtility.GetLastRect().height - 10.0f), _logoTexture);*/
        GUILayout.TextArea(_logText, GUILayout.Height(235.0f));
        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        GUILayout.Space(10.0f);
        //-------------------------------------------

        //----------------- RIGHT COL -----------------
        GUILayout.BeginVertical();

        //Pose buttons
        GUILayout.BeginHorizontal("GroupBox01");
        if (GUILayout.Button("Add Pose"))
        {
            if (_posesList.Items.Any(x => x.Name.Equals("Unnamed")))
            {
                Log("ERROR: cannot add a new pose because there still is a pose 'Unnamed'!");
            }
            else
            {
                Pose item = new Pose("Unnamed");
                _posesList.AddItem(item);
                Log("Pose added");
            }
        }
        if (GUILayout.Button("Remove Pose"))
        {
            if (SelectedPose != null)
            {
                //Delete all references in timelines
                //Find all reference using this pose (in timelines and adjust them)
                foreach (Sequence seq in _sequenceList.Items)
                {
                    List<TimeMarker> flaggedMarkers = new List<TimeMarker>();
                    foreach (TimeMarker m in seq.TimeLine.Markers)
                    {
                        if (m.LinkedItem.Name.Equals(SelectedPose.Name))
                        {
                            flaggedMarkers.Add(m);
                        }
                    }

                    foreach (TimeMarker fm in flaggedMarkers)
                    {
                        seq.TimeLine.RemoveMarkerFromTimeLine(fm);
                    }
                }

                _posesList.RemoveItem(SelectedPose);
                Log("Pose '" + SelectedPose.Name + "' removed");
                SelectedPose = null;
            }
        }
        GUILayout.EndHorizontal();

        //Pose List controls
        GUILayout.Space(10.0f);
        _posesList.Draw();

        //Sequence buttons
        GUILayout.Space(25.0f);
        GUILayout.BeginHorizontal("GroupBox01");
        if (GUILayout.Button("Add Sequence"))
        {
            if (_sequenceList.Items.Any(x => x.Name.Equals("Unnamed")))
            {
                Log("ERROR: cannot add a new sequence because there still is a sequence 'Unnamed'!");
            }
            else
            {
                Sequence item = new Sequence("Unnamed");
                _sequenceList.AddItem(item);
                Log("Sequence added");
            }
        }
        if (GUILayout.Button("Remove Sequence"))
        {
            if (SelectedSequence != null)
            {
                _sequenceList.RemoveItem(SelectedSequence);
                Log("Sequence '" + SelectedSequence.Name + "' removed");
                SelectedSequence = null;
                _bonesList.Clear();

            }
        }
        GUILayout.EndHorizontal();

        //Sequence List controls
        GUILayout.Space(10.0f);
        _sequenceList.Draw();

        //MatchPose buttons
        GUILayout.Space(25.0f);
        GUILayout.BeginHorizontal("GroupBox01");
        if (GUILayout.Button("Add MatchPose"))
        {
            if (_matchPoseList.Items.Any(x => x.Name.Equals("Unnamed")))
            {
                Log("ERROR: cannot add a new matchpose because there still is a matchpose 'Unnamed'!");
            }
            else
            {
                MatchPose item = new MatchPose("Unnamed");
                _matchPoseList.AddItem(item);
                Log("MatchPose added");
            }
        }
        if (GUILayout.Button("Remove MatchPose"))
        {
            if (SelectedMatchPose != null)
            {
                _matchPoseList.RemoveItem(SelectedMatchPose);
                Log("Sequence '" + SelectedMatchPose.Name + "' removed");
                SelectedMatchPose = null;
                _bonesList.Clear();

            }
        }
        GUILayout.EndHorizontal();


        //Matchpose List controls
        GUILayout.Space(10f);
        _matchPoseList.Draw();

        GUILayout.EndVertical();
        GUILayout.Space(10.0f);


        //-------------------------------------------

        GUILayout.EndHorizontal();
        //--------------------------

        Repaint(); //Repaint because of input code in the draw


        //------- UPDATES --------
        if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            SelectedSequence.TimeLine.Update(); //Update timeline

        if (_currentDevice == RecordDevice.Kinect)
        {
            KinectGestureRecorder.Instance.Record = true;
        }
        else
        {
            KinectGestureRecorder.Instance.Record = false;
        }

        //------- INPUT --------
        //Selecting
        if (Event.current.type == EventType.mouseDown)
        {
        }
        //Dragging
        if (Event.current.type == EventType.mouseDrag)
        {
            //Dragging
            //Check for release on mouse up
            if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            {
                if (_posesList.DraggableItem != null && _posesList.DraggableItem.IsDragging == true)
                {
                    _posesList.DraggableItem.DraggingPosition = Event.current.mousePosition;
                    //If over timeline, preview ghost
                    if (SelectedSequence.TimeLine.VisualRect.Contains(Event.current.mousePosition))
                    {
                        SelectedSequence.TimeLine.ShowPreview = true;
                        SelectedSequence.TimeLine.PreviewOnTimeLine(Event.current.mousePosition);
                    }
                }
            }
        }

        //After mouse input
        if (Event.current.type == EventType.mouseUp)
        {
            //Dragging from poselist
            if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            {
                if (_posesList.DraggableItem != null && _posesList.DraggableItem.IsDragging == true)
                {
                    _posesList.DraggableItem.IsDragging = false;
                    _posesList.DraggableItem = null;
                    SelectedSequence.TimeLine.ShowPreview = false;
                }
            }
        }
        //Input keys
        if (Event.current.type == EventType.keyDown)
        {
            if (Event.current.keyCode == KeyCode.S)
            {
            }
        }

        //Repaint all the GUI after events
        Repaint();

        //---------- DRAWING ----------
        //TimeLine
        if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            SelectedSequence.TimeLine.Draw();

        //---------- LATE DRAWING ----------
        if (SelectedSequence != null && SelectedSequence.TimeLine != null)
            SelectedSequence.TimeLine.LateDraw();

        //Draw skeleton if recording
        //Get the body that is being captured and preview it
        if (_WaitForSnapshot)
        {
            UpdateRemoteSnapshot();
            if (KinectGestureRecorder.Instance.KnownIDs.Count > 0)
            {
                KinectBody kBody = KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.First());
                if (kBody != null)
                {
                    DrawKinectBody(kBody);

                }
            }

            Repaint();
        }
        else if (KinectGestureRecorder.Instance.KnownIDs.Count > 0 && KinectGestureRecorder.Instance.Record == true && SelectedPose != null && SelectedPose.SnapShot == null)
        {
            KinectBody kBody = KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.First());
            DrawKinectBody(kBody);
        }
        else if (SelectedSequence != null && SelectedSequence.TimeLine.SelectedMarker != null && (Pose)SelectedSequence.TimeLine.SelectedMarker.LinkedItem != null)
        {
            if (((Pose)SelectedSequence.TimeLine.SelectedMarker.LinkedItem).SnapShot != null)
                DrawBones(((Pose)SelectedSequence.TimeLine.SelectedMarker.LinkedItem).SnapShot.BonesSnapShot);
        }
        else if (SelectedMatchPose != null && SelectedMatchPose.Pose != null)
        {
            DrawBones(SelectedMatchPose.Pose.SnapShot.BonesSnapShot);
        }
        else if (SelectedPose != null && SelectedPose.SnapShot != null)
        {
            DrawBones(SelectedPose.SnapShot.BonesSnapShot);
        }

        if (Event.current.type == EventType.Repaint)
        {
            Update();
        }
    }
#endif

    public void DeselectAllPoseList()
    {
        if (_posesList != null)
            _posesList.DeselectAll();
    }

    public void DeselectTimeLineMarker()
    {
        if (SelectedSequence != null && SelectedSequence.TimeLine != null)
        {
            SelectedSequence.TimeLine.DeselectMarker();
        }
    }

#if UNITY_KINECT
    private void SynchKinectBones()
    {
        //Add items to active bones list with the names of the kinect joints
        if (_currentSelectionType == SelectedItemType.Sequence)
        {
            if (SelectedSequence != null)
            {
                if (_currentDevice == RecordDevice.Kinect)
                {
                    string[] namesBones = KinectGestureRecorder.Instance.GetAllBonesByName();
                    foreach (string name in namesBones)
                    {
                        ListItem i = null;
                        //If we already have one stored in our selected sequence push the one from the sequence
                        if (SelectedSequence.BonesList.Any(x => x.Name.Equals(name)))
                            i = SelectedSequence.BonesList.Find(x => x.Name.Equals(name));
                        else
                            i = new ListItem(name);

                        if (i != null)
                        {
                            i.ContainsExtraFloatValue = true;
                            _bonesList.AddItem(i);
                        }
                    }
                }
            }
        }
        else
        {
            if (SelectedMatchPose != null)
            {
                if (_currentDevice == RecordDevice.Kinect)
                {
                    string[] namesBones = KinectGestureRecorder.Instance.GetAllBonesByName();
                    foreach (string name in namesBones)
                    {
                        ListItem i = null;
                        //If we already have one stored in our selected sequence push the one from the sequence
                        if (SelectedMatchPose.BonesList.Any(x => x.Name.Equals(name)))
                            i = SelectedMatchPose.BonesList.Find(x => x.Name.Equals(name));
                        else
                            i = new ListItem(name, _standardAllowedOffset);

                        if (i != null)
                            _bonesList.AddItem(i);
                    }
                }
            }
        }
    }

    private void DrawKinectBody(KinectBody body)
    {
        if (body != null)
        {
            //Calculate offset based on spinebone 1
            float scale = 200.0f;
            Vector2 offset = new Vector2(300, 550) - new Vector2(0, body.Bones[0].StartPosition.y * scale);

            foreach (Bone bone in body.Bones)
            {
                if (!_bonesList.IsItemSelected(bone.Name) && !_WaitForSnapshot)
                    continue;


                Vector2 startPosition = new Vector2(bone.StartPosition.x * scale, bone.StartPosition.y * scale);
                Vector2 endPosition = Vector2.zero;
                if (bone.EndPosition.HasValue)
                {
                    endPosition = new Vector2(bone.EndPosition.Value.x * scale, bone.EndPosition.Value.y * scale);
                }
                else
                {
                    endPosition = new Vector2(startPosition.x, startPosition.y);
                }
                endPosition.x += offset.x;
                endPosition.y = ((endPosition.y + offset.y) - Screen.height) * -1;
                startPosition.x += offset.x;
                startPosition.y = ((startPosition.y + offset.y) - Screen.height) * -1;

                Color color = Color.red;
                if (bone.TrackingState == TrackingState.NotTracked)
                {
                    color = Color.grey;
                }
                else if (bone.TrackingState == TrackingState.Tracked && bone.Preview == true)
                {
                    color = Color.green;
                }
                else if (bone.TrackingState == TrackingState.Inferred)
                {
                    color = Color.red;
                }

                LineDrawer.DrawLine(startPosition, endPosition, color, 0.5f);
                LineDrawer.DrawRect(startPosition, 4.0f, Color.cyan);


            }


        }
    }
#endif

    private void DrawBones(List<Bone> bones, bool showAll = true)
    {
        //Calculate offset based on spinebone 1
        if (bones.Count <= 0)
            return;

        float scale = 200.0f;
        Vector2 offset = new Vector2(300, 550) - new Vector2(0, bones[0].StartPosition.y * scale);
        foreach (Bone bone in bones)
        {
            if ((SelectedSequence != null || SelectedMatchPose != null) && !_bonesList.IsItemSelected(bone.Name))
                continue;

            Vector2 startPosition = new Vector2(bone.StartPosition.x * scale, bone.StartPosition.y * scale);
            Vector2 endPosition = Vector2.zero;
            if (bone.EndPosition.HasValue)
            {
                endPosition = new Vector2(bone.EndPosition.Value.x * scale, bone.EndPosition.Value.y * scale);
            }
            else
            {
                endPosition = new Vector2(startPosition.x, startPosition.y);
            }
            endPosition.x += offset.x;
            endPosition.y = ((endPosition.y + offset.y) - Screen.height) * -1;
            startPosition.x += offset.x;
            startPosition.y = ((startPosition.y + offset.y) - Screen.height) * -1;

            Color color = Color.red;
            if (bone.TrackingState == TrackingState.NotTracked)
            {
                color = Color.grey;
            }
            else if (bone.TrackingState == TrackingState.Tracked)
            {
                color = Color.green;
            }
            else if (bone.TrackingState == TrackingState.Inferred)
            {
                color = Color.red;
            }

            LineDrawer.DrawLine(startPosition, endPosition, color, 0.5f);
            LineDrawer.DrawRect(startPosition, 4.0f, Color.cyan);
        }
    }

    public void ClearPoses()
    {
        _posesList.Clear();
    }

    public void ClearSequences()
    {
        _sequenceList.Clear();
    }
    public void ClearMatchPoses()
    {
        _matchPoseList.Clear();
    }

    public void AddPose(Pose p)
    {
        if (p != null)
        {
            _posesList.Items.Add(p);
        }
    }

    public void AddSequence(Sequence s)
    {
        if (s != null)
        {
            _sequenceList.Items.Add(s);
        }
    }

    public void AddMatchPose(MatchPose p)
    {
        if (p != null)
        {
            _matchPoseList.Items.Add(p);
        }
    }

    public void Log(string text)
    {
        if (_logCount >= _logMaxCount)
        {
            _logText = "";
            _logCount = 0;
        }

        string t = " " + DateTime.Now.ToString();
        t += ": ";
        t += text;
        t += "\n";
        _logText += t;
        ++_logCount;
    }
}

public class TimeLine
{
    //FIELDS
    private float _offset = 0.0f;
    private float _totalDuration = 5.0f;
    private float _lineBigWidth = 2.0f;
    private float _lineSmallWidth = 1.0f;
    private float _lineBigHeight = 15.0f;
    private float _lineSmallHeight = 7.5f;
    private Texture2D _linePixelTexture;
    private Texture2D _linePixelOverlayTexture;

    private List<TimeMarker> _markers = new List<TimeMarker>();
    private TimeMarker _selectedMarker = null;
    private TimeMarker _ghostMarker = null;
    private TimeMarker _droppingMarker = null;
    private bool _canDeselect = false;
    private bool _isOverlapping = false;
    private bool _previewNumbers = true;
    private float _xSelectionOffset = 0.0f;
    private EventType _previousType;

    //PROPERTIES
    public Rect Rect { get; set; }
    public Rect VisualRect { get; set; }
    public TimeMarker DropMarker { get { return _droppingMarker; } }
    public List<TimeMarker> Markers { get { return _markers; } set { _markers = value; } }
    public TimeMarker SelectedMarker { get { return _selectedMarker; } set { _selectedMarker = value; } }
    public Sequence LinkedSequence { get; set; }
    public float Offset { get { return _offset; } set { _offset = value; } }
    public float TotalDuration { get { return _totalDuration; } set { _totalDuration = value; } }
    public float Scale { get { return VisualRect.width / TotalDuration; } }
    public bool ShowPreview { get; set; }
    public string Name { get; set; }

    //CONSTRUCTOR
    public TimeLine(Rect r, float offset, Sequence linkedSequence)
    {
        Rect = r;
        VisualRect = new Rect(r.x + offset, r.y + offset, r.width - (2 * offset), r.height - (offset * 2));
        _offset = offset;
        if (linkedSequence != null)
        {
            LinkedSequence = linkedSequence;
            Name = LinkedSequence.Name;
        }

        //Create texture from 1*1 pixels for the lines
        _linePixelTexture = Resources.Load<Texture2D>("Pixel_Gray");
        _linePixelOverlayTexture = Resources.Load<Texture2D>("Pixel_Gray_T");


        //Create ghost
        if (_ghostMarker == null)
        {
            _ghostMarker = new TimeMarker(0.0f, 0.1f, new Vector2(0.0f, 0.0f));
        }

        //Create dropper
        if (_droppingMarker == null)
        {
            _droppingMarker = new TimeMarker(0.0f, 0.1f, new Vector2(0.0f, 0.0f));
            _droppingMarker.Active = false;
        }
    }

    //METHODS
    public void DestroyImmediate()
    {

    }

    public void Update()
    {
        //Update rect
        VisualRect = new Rect(Rect.x + _offset, Rect.y + _offset, Rect.width - (2 * _offset), Rect.height - (_offset * 2));

        //Selecting
        if (Event.current.type == EventType.mouseDown)
        {
            bool hittedAnElement = false;
            foreach (TimeMarker marker in _markers)
            {
                if (marker.Rect.Contains(Event.current.mousePosition))
                {
                    //If we already have one selected AND it is not this one, deselect it and select new one
                    if (_selectedMarker != null && !_selectedMarker.Equals(marker))
                    {
                        _selectedMarker.Active = false;
                        _selectedMarker = marker;
                        _selectedMarker.Active = true;
                        //Switch selected item in the editor (for parameters)
                        GestureEditor.Instance.SelectedPoseInTimeLine = (Pose)_selectedMarker.LinkedItem;

                        //Deselect the list
                        GestureEditor.Instance.DeselectAllPoseList();
                    }
                    else if (_selectedMarker != null && _selectedMarker.Equals(marker)) //Else if it is the same one, make it deselectable
                    {
                        _canDeselect = true;
                    }
                    else //if none is selected, select the new one
                    {
                        _selectedMarker = marker;
                        _selectedMarker.Active = true;

                        //Switch selected item in the editor (for parameters)
                        GestureEditor.Instance.SelectedPoseInTimeLine = (Pose)_selectedMarker.LinkedItem;

                        //Deselect the list
                        GestureEditor.Instance.DeselectAllPoseList();
                    }

                    //Calculate position of selected marker
                    if (_selectedMarker != null)
                        _xSelectionOffset = _selectedMarker.Position.x - Event.current.mousePosition.x + this.Offset;

                    if (hittedAnElement == false)
                        hittedAnElement = true;
                }
            }

            //If we didn't hit something and we have on selected, deselect it
            if (hittedAnElement == false && _selectedMarker != null)
            {
                _selectedMarker.Active = false;
                _selectedMarker = null;
                //Change selected item in the editor
                GestureEditor.Instance.SelectedPoseInTimeLine = null;
                GestureEditor.Instance.SelectedPose = null;
            }

            //Store previous type
            _previousType = EventType.mouseDown;
        }
        //Dragging
        if (Event.current.type == EventType.mouseDrag)
        {
            if (_selectedMarker != null)
            {
                if ((Event.current.mousePosition.x + _xSelectionOffset) > 0.0f //Left
                    && (Event.current.mousePosition.x + _selectedMarker.Size.x + _xSelectionOffset) < this.Rect.xMax) //Right
                {
                    //Reorder
                    _markers = _markers.OrderBy(x => x.StartTime).ToList();

                    //Move by calculating new startTime. Do not move if it hits an object left or right to it
                    float previousTime = _selectedMarker.StartTime;
                    _selectedMarker.StartTime = this.GetTimeFromHorizontalPosition(new Vector2(Event.current.mousePosition.x + _xSelectionOffset, _selectedMarker.Position.y));

                    //Check if it can move where it wants to move, if not reset it to previous start time
                    bool overlappingElement = false;
                    foreach (TimeMarker marker in _markers)
                    {
                        if (_selectedMarker.OverlapsTimeMarker(marker))
                        {
                            _ghostMarker.StartTime = _selectedMarker.StartTime;
                            _ghostMarker.Duration = _selectedMarker.Duration;

                            if (_selectedMarker.StartTime > previousTime)
                            {
                                _selectedMarker.StartTime = previousTime;
                                _selectedMarker.SnapToClosestRight(_markers);
                            }
                            else
                            {
                                _selectedMarker.StartTime = previousTime;
                                _selectedMarker.SnapToClosestLeft(_markers);
                            }

                            //If we have an overlap, store it locally for later use
                            overlappingElement = true;
                        }
                    }
                    //Check if overlapping an element and storing it
                    _isOverlapping = overlappingElement;
                }
                else if ((Event.current.mousePosition.x + _xSelectionOffset) <= 0.0f)
                {
                    //Lock left
                    if (_isOverlapping == false)
                        _selectedMarker.StartTime = 0.0f;
                    else
                        _ghostMarker.StartTime = 0.0f;
                }
                else if ((Event.current.mousePosition.x + _selectedMarker.Size.x + _xSelectionOffset) >= this.VisualRect.xMax)
                {
                    //Lock right
                    if (_isOverlapping == false)
                        _selectedMarker.StartTime = this.TotalDuration - _selectedMarker.Duration;
                    else
                        _ghostMarker.StartTime = this.TotalDuration - _selectedMarker.Duration;
                }
                //Store previous type
                _previousType = EventType.mouseDrag;
            }
        }
        //Mouse up (or after continues input)
        if (Event.current.type == EventType.mouseUp)
        {
            //Adding a marker if dropmarker is in the timeline and not overlapping
            if (this.VisualRect.Contains(Event.current.mousePosition) && DropMarker != null && ShowPreview == true && _isOverlapping == false)
            {
                //No dropping if selectedPose is unnamed
                if (GestureEditor.Instance.SelectedPose != null && GestureEditor.Instance.SelectedPose.Name.Equals("Unnamed"))
                {
                    GestureEditor.Instance.Log("ERROR: Cannot add a Pose with the name 'Unnamed'");
                }
                else if (GestureEditor.Instance.SelectedPose != null && GestureEditor.Instance.SelectedPose.SnapShot == null)
                {
                    GestureEditor.Instance.Log("ERROR: Cannot add a Pose that has no snapshot");
                }
                else
                {
                    TimeMarker marker = new TimeMarker(DropMarker.StartTime, DropMarker.Duration, DropMarker.Size);
                    marker.LinkedItem = (Pose)GestureEditor.Instance.SelectedPose.Clone();
                    GestureEditor.Instance.Log("Marker for pose '" + marker.LinkedItem.Name + "' added to Sequence '" + this.Name);
                    AddMarkerToTimeLine(marker);
                }
            }

            if (_selectedMarker != null && _selectedMarker.Rect.Contains(Event.current.mousePosition)
                && _previousType != EventType.mouseDrag && _canDeselect == true) //Else if it is the same one, deselect it
            {
                _selectedMarker.Active = false;
                _selectedMarker = null;
                _canDeselect = false;
                //Change selected item in the editor (for parameters)
                GestureEditor.Instance.SelectedPoseInTimeLine = null;
                GestureEditor.Instance.SelectedPose = null;
            }
            //If no input, don't display ghost
            _isOverlapping = false;

            //Store previous type
            _previousType = EventType.mouseUp;
        }
        //If a key is pressed
        if (Event.current.type == EventType.keyDown)
        {
            //If delete pressed + a marker selected = delete it
            if (Event.current.keyCode == KeyCode.Delete && _selectedMarker != null)
            {
                GestureEditor.Instance.Log("Marker for pose '" + _selectedMarker.LinkedItem.Name + "' removed from Sequence '" + this.Name);
                RemoveMarkerFromTimeLine(_selectedMarker);
                _selectedMarker = null;
            }
        }

        //If we are previewing, check if our pose is overlapping existing markers, if so preview red ghost and disable dropper
        bool dropperOverlapping = false;
        if (ShowPreview == true)
        {
            //Take the duration of the selected pose and preview the correct size
            if (GestureEditor.Instance.SelectedPose != null)
                _droppingMarker.Duration = (GestureEditor.Instance.SelectedPose).Duration;

            foreach (TimeMarker marker in _markers)
            {
                if (_droppingMarker.OverlapsTimeMarker(marker))
                {
                    dropperOverlapping = true;
                }
            }
            if (dropperOverlapping == true)
            {
                ShowPreview = false;
                _isOverlapping = true;

                _ghostMarker.StartTime = _droppingMarker.StartTime;
                _ghostMarker.Duration = _droppingMarker.Duration;
            }
            else
            {
                ShowPreview = true;
                _isOverlapping = false;
            }
        }
    }

    public void Draw()
    {
        //Time line
        GUI.Box(VisualRect, "", "TimeLine");

        //Adjust according to type
        if (LinkedSequence.SequenceType == SequenceType.Static)
        {
            TotalDuration = 10.0f;
            _previewNumbers = false;
        }
        else
        {
            _previewNumbers = true;
        }

        //Overlay lines
        float xStep = VisualRect.width / _totalDuration;
        int amount = (int)_totalDuration;
        for (int i = 1; i <= amount; ++i)
        {
            GUI.DrawTexture(new Rect(VisualRect.x + (i * xStep), VisualRect.y, _lineBigWidth, VisualRect.height), _linePixelOverlayTexture);
        }

        //Markers
        foreach (TimeMarker marker in _markers)
        {
            if (_previewNumbers == false)
            {
                marker.Duration = 0.5f;
                if (marker.LinkedItem is Pose)
                    ((Pose)marker.LinkedItem).Duration = 0.5f;
            }
            marker.Draw(this);
        }
        //Ghostmarker
        if (_ghostMarker != null && _isOverlapping == true && Event.current.type == EventType.repaint)
        {
            _ghostMarker.Draw(this, "TimeMarkerGhost");
        }
        //Dropmarker
        if (_droppingMarker != null && _droppingMarker.Active == true && ShowPreview == true && Event.current.type == EventType.repaint)
        {
            _droppingMarker.Draw(this, "TimeMarkerDrop");
        }
    }

    public void LateDraw()
    {
        //Time bar
        GUI.Label(new Rect(VisualRect.x, VisualRect.y + VisualRect.height - 4.0f, VisualRect.width, 30.0f), "", "TimeLineBottom");
        //Time lines
        float xStep = VisualRect.width / _totalDuration;
        float xSmallStep = xStep / 10.0f;
        int amount = (int)_totalDuration;
        int amountSmallLine = (int)(VisualRect.width / xSmallStep);

        //Big lines
        for (int i = 1; i <= amount; ++i)
        {
            //Hard lines
            GUI.DrawTexture(new Rect(VisualRect.x + (i * xStep), VisualRect.y + VisualRect.height - 4.0f, _lineBigWidth, _lineBigHeight), _linePixelTexture);
            //Numbers
            if (_previewNumbers == true)
            {
                GUI.color = Color.black;
                if (i < _totalDuration)
                {
                    GUI.Label(new Rect(VisualRect.x + (i * xStep) - 7.5f, VisualRect.height + 13.0f, 50.0f, 20.0f), (i).ToString("0.0"));
                }
                GUI.color = Color.white;
            }
        }

        //Small lines
        for (int j = 0; j <= amountSmallLine; ++j)
        {
            GUI.DrawTexture(new Rect(VisualRect.x + (j * xSmallStep), VisualRect.y + VisualRect.height - 4.0f, _lineSmallWidth, _lineSmallHeight), _linePixelTexture);
        }

        //Start and Endline
        GUI.DrawTexture(new Rect(VisualRect.x, VisualRect.y + VisualRect.height - 4.0f, _lineBigWidth, _lineBigHeight), _linePixelTexture);
        //GUI.DrawTexture(new Rect(VisualRect.x + VisualRect.width - _lineBigWidth, VisualRect.y + VisualRect.height - 4.0f, _lineBigWidth, _lineBigHeight), _linePixelTexture);
    }

    public float GetTimeFromHorizontalPosition(Vector2 position)
    {
        return ((position.x - VisualRect.x) / VisualRect.width) * TotalDuration;
    }

    public void PreviewOnTimeLine(Vector2 position)
    {
        //If no marker is selected, and no ghost is shown, preview the ghost of an possible marker before addition
        if (_selectedMarker == null)
        {
            _droppingMarker.Duration = 0.1f;
            _droppingMarker.StartTime = GetTimeFromHorizontalPosition(position);
            _droppingMarker.Active = true;
        }
    }

    public void DeselectMarker()
    {
        if (_selectedMarker != null)
        {
            _selectedMarker.Active = false;
            _selectedMarker = null;
        }
    }

    public void AddMarkerToTimeLine(TimeMarker marker)
    {
        _markers.Add(marker);
    }

    public void RemoveMarkerFromTimeLine(TimeMarker marker)
    {
        _markers.Remove(marker);
    }
}

public class TimeMarker
{
    //FIELDS
    private Vector2 _position = Vector2.zero;
    private Vector2 _size = Vector2.zero;
    private float _duration = 0.1f;
    private float _startTime = 0.0f;

    //PROPERTIES
    public Vector2 Position { get { return _position; } set { _position = value; } }
    public Vector2 Size { get { return _size; } set { _size = value; } }
    public float Duration { get { return _duration; } set { _duration = value; } }
    public float StartTime { get { return _startTime; } set { _startTime = value; } }
    public Rect Rect { get; set; }
    public bool Active { get; set; }
    public ListItem LinkedItem { get; set; }

    //CONSTRUCTOR
    public TimeMarker(float startTime, float duration, Vector2 size)
    {
        StartTime = startTime;
        Duration = duration;
        Size = size;
    }

    //METHODS
    public void Draw(TimeLine timeLine, GUIStyle style = null)
    {
        //Only draw on second gothrough
        if (Event.current.type != EventType.repaint)
        {
            return;
        }

        //Calculate size
        if (Duration > timeLine.TotalDuration)
            Duration = timeLine.TotalDuration;
        if (StartTime + Duration > timeLine.TotalDuration)
            StartTime = timeLine.TotalDuration - Duration;
        if (StartTime < 0.0f)
            StartTime = 0.0f;

        _size.x = Duration * timeLine.Scale;
        _size.y = timeLine.VisualRect.height;
        _position.x = _startTime * timeLine.Scale;
        Rect = new Rect(new Rect(timeLine.VisualRect.x + Position.x, timeLine.VisualRect.y,
            Size.x, Size.y));

        //Draw with the correct skin
        if (style != null)
            GUI.Box(Rect, "", style);
        else if (Active == false)
            GUI.Box(Rect, "", "TimeMarkerInactive");
        else
            GUI.Box(Rect, "", "TimeMarkerActive");
    }

    public bool OverlapsTimeMarker(TimeMarker marker)
    {
        if ((this.StartTime < (marker.StartTime + marker.Duration) && this.StartTime > marker.StartTime) ||
            ((this.StartTime + this.Duration) < (marker.StartTime + marker.Duration) && (this.StartTime + this.Duration) > marker.StartTime) ||
            (marker.StartTime < (this.StartTime + this.Duration) && marker.StartTime > this.StartTime) ||
            ((marker.StartTime + marker.Duration) < (this.StartTime + this.Duration) && (marker.StartTime + marker.Duration) > this.StartTime))
        {
            return true;
        }
        else
            return false;
    }

    public void SnapToClosestLeft(List<TimeMarker> markers) //left of selected marker
    {
        float maxRight = 0.0f;
        foreach (TimeMarker marker in markers)
        {
            if (marker.Equals(this))
                continue;

            float left = marker.StartTime;
            float right = marker.StartTime + marker.Duration;

            if (left >= this.StartTime)
                continue;

            if (right > maxRight)
            {
                maxRight = right;
            }
        }

        this.StartTime = maxRight;
    }

    public void SnapToClosestRight(List<TimeMarker> markers) //right of selected marker
    {
        float minLeft = Mathf.Infinity;
        foreach (TimeMarker marker in markers)
        {
            if (marker.Equals(this))
                continue;

            float left = marker.StartTime;
            float right = marker.StartTime + marker.Duration;

            if (right <= this.StartTime)
                continue;

            if (left < minLeft)
            {
                minLeft = left;
            }
        }

        this.StartTime = minLeft - this.Duration;
    }
}

public class ScrolList
{
    //FIELDS
    private List<ListItem> _itemList = new List<ListItem>(); //Items in the list
    private List<ListItem> _selectedItems = new List<ListItem>(); //If we allow multiple selection, these are the selected objects
    private Vector2 _poseListScrollPosition = Vector2.zero; //The position in the scrollarea
    private ListItem _selectedItem = null; //If only one selection is allowed this is the object
    private EventType _previousType; //Type of event captured
    private bool _canDeselect = false; //Check if I can deselect the current selected item

    //PROPERTIES
    public Vector2 ScrollPosition { get { return _poseListScrollPosition; } set { _poseListScrollPosition = value; } } //The position in the scrollarea
    public ListItem DraggableItem { get; set; } //The selected object that we can drag
    public float Height { get; set; } //Height of the scrollist
    public float Width { get; set; } //Width of the scrollist
    public bool MultipleSelectionAllowed { get; set; } //Allow multiple selection or not
    public List<ListItem> Items { get { return _itemList; } set { _itemList = value; } } //All the items in the list

    //CONSTRUCTOR
    public ScrolList(float width, float height)
    {
        Width = width;
        Height = height;
    }

    //METHODS
    public void Draw()
    {
        //Scrolling Part
        ScrollPosition = GUILayout.BeginScrollView(_poseListScrollPosition, "ScrollBox", GUILayout.Height(Height), GUILayout.Width(Width));
        //Items
        foreach (ListItem item in _itemList)
        {
            item.Draw();
        }

        //----- INPUT -----
        //Selecting
        if (Event.current.type == EventType.mouseDown)
        {
            foreach (ListItem item in _itemList)
            {
                if (item.Rect.Contains(Event.current.mousePosition))
                {
                    //If only one selection is allowed
                    if (MultipleSelectionAllowed == false)
                    {
                        //If one selected, deselect it and get the new one
                        if (_selectedItem != null && !_selectedItem.Equals(item))
                        {
                            _selectedItem.Active = false;
                            _selectedItem = item;
                            _selectedItem.Active = true;
                            //Change selected item in the editor
                            GestureEditor.Instance.SelectedPoseInTimeLine = null;
                        }
                        else if (_selectedItem != null && _selectedItem.Equals(item)) //Else if it is the same one, make it deselectable
                        {
                            _canDeselect = true;
                            //Change selected item in the editor
                            GestureEditor.Instance.SelectedPoseInTimeLine = null;
                            GestureEditor.Instance.SelectedPose = null;
                        }
                        else //if none is selected, select the new one
                        {
                            _selectedItem = item;
                            _selectedItem.Active = true;
                            //Change selected item in the editor
                            GestureEditor.Instance.SelectedPoseInTimeLine = null;
                        }

                        //If mouse is down make it dragging
                        _selectedItem.IsDragging = true;
                        _selectedItem.DraggingPosition = Event.current.mousePosition;
                        DraggableItem = _selectedItem;
                    }
                    else
                    {
                        //Select new
                        if (!_selectedItems.Contains(item))
                        {
                            _selectedItem = item;
                            _selectedItem.Active = true;
                            //Add it to list
                            _selectedItems.Add(item);
                            //Change selected item in the editor
                            GestureEditor.Instance.SelectedPoseInTimeLine = null;
                        }
                        else //Else if it is the same one, make it deselectable
                        {
                            _canDeselect = true;
                            _selectedItem = item;
                            //Change selected item in the editor
                            GestureEditor.Instance.SelectedPoseInTimeLine = null;
                            GestureEditor.Instance.SelectedPose = null;
                        }

                        //If mouse is down make it dragging
                        _selectedItem.IsDragging = true;
                        _selectedItem.DraggingPosition = Event.current.mousePosition;
                        DraggableItem = _selectedItem;
                    }
                }
            }

            //Store previous type
            _previousType = EventType.mouseDown;
        }
        //Letting go, letting go, letting gooooooooooooooo
        if (Event.current.type == EventType.mouseUp)
        {
            //Deselection
            if (_selectedItem != null && _selectedItem.Rect.Contains(Event.current.mousePosition)
                && _previousType != EventType.mouseDrag && _canDeselect == true) //Else if it is the same one, deselect it
            {
                _selectedItem.Active = false;
                _selectedItem.IsDragging = false;
                if (_selectedItems.Contains(_selectedItem))
                    _selectedItems.Remove(_selectedItem);
                _selectedItem = null;
                DraggableItem = null;
                _canDeselect = false;
            }

            //Store previous type
            _previousType = EventType.mouseUp;
        }

        GUILayout.EndScrollView();
    }

    public void DeselectAll()
    {
        if (_selectedItem != null)
        {
            _selectedItem.Active = false;
            _selectedItem = null;
        }

        foreach (ListItem item in _selectedItems)
        {
            item.Active = false;
        }
        _selectedItems.Clear();
    }

    public void SelectAll()
    {
        if (MultipleSelectionAllowed == true)
        {
            foreach (ListItem item in _itemList)
            {
                _selectedItems.Add(item);
                item.Active = true;
            }
        }
    }

    public void AddItem(ListItem item)
    {
        if (item != null)
            _itemList.Add(item);
    }

    public void RemoveItem(ListItem item)
    {
        if (_itemList.Contains(item))
        {
            _selectedItem.Active = false;
            _selectedItem = null;
            _itemList.Remove(item);
        }
    }

    public bool IsItemSelected(string name)
    {
        ListItem item = _itemList.Find(x => x.Name.Equals(name));
        if (item != null)
        {
            if (item.Active == true)
                return true;
            else
                return false;
        }
        return false;
    }

    public void Clear()
    {
        _itemList.Clear();
        _selectedItems.Clear();
        _selectedItem = null;
    }

    public ListItem GetSelectedItem()
    {
        return _selectedItem;
    }
}
#endif
public class ListItem : ICloneable
{
    //FIELDS

    //PROPERTIES
    public string Name { get; set; }
    public Rect Rect { get; set; }
    public Vector2 DraggingPosition { get; set; }
    public bool Active { get; set; }
    public bool IsDragging { get; set; }
    public bool ContainsExtraFloatValue { get; set; }
    public float ExtraFloatValue { get; set; }
    //CONSTRUCTOR
    public ListItem(string name, float defaultExtraFloatValue = 0.0f)
    {
        Name = name;
        ExtraFloatValue = defaultExtraFloatValue;
    }

    //METHODS
    public void Update()
    {
    }
#if UNITY_EDITOR
    public void Draw()
    {
        GUILayout.BeginHorizontal();

        //Draw a label
        if (Active == true)
            GUILayout.Label(Name, "SelectedItem");
        else
            GUILayout.Label(Name, "Item");

        //Draw extra variable
        if (ContainsExtraFloatValue == true)
        {
            if (Active == true)
            {
                ExtraFloatValue = EditorGUILayout.FloatField(ExtraFloatValue, GUILayout.Width(40.0f));
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.FloatField(ExtraFloatValue, GUILayout.Width(40.0f));
                GUI.enabled = true;
            }
        }

        GUILayout.EndHorizontal();

        //Store it's rect for input detection
        Rect = GUILayoutUtility.GetLastRect();
    }
#endif

    public object Clone()
    {
        return this.MemberwiseClone();
    }
}
