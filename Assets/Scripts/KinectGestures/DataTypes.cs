﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_KINECT
using Kinect = Windows.Kinect;
#endif
using System;

[System.Flags]
public enum PoseAxis
{
    X = 0x1,
    Y = 0x2,
    Z = 0x4
}

public enum TrackingState
{
    Inferred = 0,
    NotTracked = 1,
    Tracked = 2
}

public enum HandTrackingState
{ 
    Closed = 0,
    Lasso = 1,
    NotTracked = 2,
    Open = 4, 
    Unknown = 8
}

public enum TransitionType
{
    Rotation
}

public enum SequenceType
{
    Continuous,
    Static
}

public enum InvokeType
{
    ContinuousInvoke,
    SingleInvoke
}

public enum RecordDevice
{
    None,
    Kinect
}

public class Bone
{
    //FIELDS
    public Vector3 StartPosition = Vector3.zero;
    public Vector3? EndPosition = null;
    public Vector4 Orientation = Vector4.zero;
    public TrackingState TrackingState = TrackingState.NotTracked;
    public bool Preview = true;
    public string Name = "";

    //CONSTRUCTOR
    public Bone(Vector3 startPosition, Vector3? endPosition, Vector4 orientation, TrackingState trackingState, string name)
    {
        StartPosition = startPosition;
        EndPosition = endPosition;
        Orientation = orientation;
        TrackingState = trackingState;
        Name = name;
    }

    //METHODS
    public override bool Equals(object obj)
    {
        string other = (string)obj;

        return this.Name.GetHashCode().Equals(other.GetHashCode());
    }

    public override int GetHashCode() //Override hashcode method to no collide with logic of equals
    {
        return this.Name.GetHashCode();
    }
}

//public class MatchPose: ListItem
//{
    
//    public Pose Pose { get; set; }

//    public bool IgnoreBoneRotation { get; set; }
//    public MatchPose(string name): base (name)
//    {

//    }
//    public List<ListItem> BonesList = new List<ListItem>();
    
//}
//public class Sequence : ListItem
//{
//    //PROPERTIES
//    public PoseAxis PoseAxis { get; set; }
//    public float MaxInterludePoses { get; set; }
//    public TransitionType TransitionType { get; set; }
//    public SequenceType SequenceType { get; set; }
//    public InvokeType InvokeType { get; set; }
//    public TimeLine TimeLine { get; set; }
//    public List<ListItem> BonesList = new List<ListItem>();

//    //CONSTRUCTOR
//    public Sequence(string name) : base(name)
//    {
//        TimeLine = new TimeLine(new Rect(0.0f, 0.0f, 0.0f, 0.0f), 5.0f, this);
//        MaxInterludePoses = 5.0f;
//    }

//    public void DestroyImmediate()
//    {
//        TimeLine.DestroyImmediate();
//    }
//}

//public class Pose : ListItem
//{
//    //PROPERTIES
//    public SnapShot SnapShot { get; set; }
//    public float Duration { get; set; }

//    //CONSTRUCTOR
//    public Pose(string name) : base(name)
//    {
//        Duration = 0.1f;
//    }
//}

public class SnapShot
{
    //PROPERTIES
    public List<Bone> BonesSnapShot { get; private set; }

    public HandTrackingState LeftHandState { get; private set; }
    public HandTrackingState RightHandState { get; private set; }


    //CONSTRUCTOR
    public SnapShot(List<Bone> bones, HandTrackingState leftHandState = HandTrackingState.Unknown, HandTrackingState rightHandState = HandTrackingState.Unknown)
    {
        BonesSnapShot = bones;
        LeftHandState = leftHandState;
        RightHandState = rightHandState;
    }
}

#if UNITY_KINECT
public class KinectBody
{
   
    //FIELDS
    private List<Bone> _bones = new List<Bone>();
    private Dictionary<Kinect.JointType, Kinect.JointType> _boneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    //PROPERTIES
    public ulong TrackingID { get; set; }
    public List<Bone> Bones { get { return _bones; } }
    public HandTrackingState LeftHandState { get; private set; }
    public HandTrackingState RightHandState { get; private set; }

    //CONSTRUCTOR
    public KinectBody(ulong trackingID)
    {
        TrackingID = trackingID;
    }

    //METHODS
    public void RefreshBody(Kinect.Body body)
    {
        //Loop over the joints and refresh it's data
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            //Get the sourcejoint and set the targetjoint to null so we can check for it later
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.JointOrientation sourceJointOrientation = body.JointOrientations[jt];
            Kinect.Joint? targetJoint = null;

            //If we have the target joint, get it
            if (_boneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_boneMap[jt]];
            }

            //Get bone data
            Vector3 position = new Vector3(sourceJoint.Position.X, sourceJoint.Position.Y, sourceJoint.Position.Z);
            Vector3? endPosition = null;
            if (targetJoint.HasValue)
                endPosition = new Vector3?(new Vector3(targetJoint.GetValueOrDefault().Position.X, targetJoint.GetValueOrDefault().Position.Y, targetJoint.GetValueOrDefault().Position.Z));
            Vector4 orientation = new Vector4(sourceJointOrientation.Orientation.X, sourceJointOrientation.Orientation.Y, sourceJointOrientation.Orientation.Z, sourceJointOrientation.Orientation.W);
            TrackingState trackingState = TrackingState.NotTracked;
            if (sourceJoint.TrackingState == Kinect.TrackingState.Tracked)
                trackingState = TrackingState.Tracked;
            else if (sourceJoint.TrackingState == Kinect.TrackingState.NotTracked)
                trackingState = TrackingState.NotTracked;
            else if (sourceJoint.TrackingState == Kinect.TrackingState.Inferred)
                trackingState = TrackingState.Inferred;

            //Check if bone is already stored, if not create and store it
            if (!_bones.Any(x => x.Equals(sourceJoint.JointType.ToString())))
            {
                _bones.Add(new Bone(position, endPosition, orientation, trackingState, sourceJoint.JointType.ToString()));
            }
            else //else update the data
            {
                Bone bone = _bones.Find(x => x.Equals(sourceJoint.JointType.ToString()));
                bone.StartPosition = position;
                bone.EndPosition = endPosition;
                bone.Orientation = orientation;
                bone.TrackingState = trackingState;
            }
        }

        /*Track the state of the hands*/
        switch (body.HandLeftState)
        {
            case Kinect.HandState.Closed:
                LeftHandState = HandTrackingState.Closed;
                break;
            case Kinect.HandState.Lasso:
                LeftHandState = HandTrackingState.Lasso;
                break;
            case Kinect.HandState.NotTracked:
                LeftHandState = HandTrackingState.NotTracked;
                break;
            case Kinect.HandState.Open:
                LeftHandState = HandTrackingState.Open;
                break;
            case Kinect.HandState.Unknown:
                LeftHandState = HandTrackingState.Unknown;
                break;
        }

        switch (body.HandRightState)
        {
            case Kinect.HandState.Closed:
                RightHandState = HandTrackingState.Closed;
                break;
            case Kinect.HandState.Lasso:
                RightHandState = HandTrackingState.Lasso;
                break;
            case Kinect.HandState.NotTracked:
                RightHandState = HandTrackingState.NotTracked;
                break;
            case Kinect.HandState.Open:
                RightHandState = HandTrackingState.Open;
                break;
            case Kinect.HandState.Unknown:
                RightHandState = HandTrackingState.Unknown;
                break;
        }

    }
}
#endif