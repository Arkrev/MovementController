﻿#if UNITY_KINECT
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Windows.Kinect;
using System.Linq;

[ExecuteInEditMode]
public class KinectGestureRecorder : Singleton<KinectGestureRecorder> 
{
    private const float _timeBeforeForget = 2f;
    //FIELDS
    private KinectSensor _sensor; //reference to the sensor
    private BodyFrameReader _reader; //body reader
    private Body[] _bodiesData = null; //data of bodies captured, but we'll use only one body for the gesture recorder
    private Dictionary<ulong, KinectBody> _kinectBodies = new Dictionary<ulong, KinectBody>(); //list with all the bodies and their data
    private List<ulong> _knownIDs = new List<ulong>(); //List containing all the known ID's
   
    private Dictionary<ulong, bool> _trackedBodies = new Dictionary<ulong, bool>();

    //PROPERTIES
    public List<ulong> KnownIDs { get { return _knownIDs; } }
    public bool Record { get; set; }

    //METHODS
	void Start () 
    {
	    //Get the sensor device
        _sensor = KinectSensor.GetDefault();
        //If we've received the sensor, get it's reader and open the sensor if not open already
        if (_sensor != null)
        {
            _reader = _sensor.BodyFrameSource.OpenReader();

            if (!_sensor.IsOpen)
            {
                _sensor.Open();
            }
        } 
	}

	
	void Update ()
    {
        //If we have a valid reader linked to the current sensor
        if (_reader != null && Record == true)
        {
            //Get the latest frame recorded by the device
            BodyFrame frame = _reader.AcquireLatestFrame();
            //If we have a frame, check if we have bodies, if not create them, and get the data from all the recorded bodies
            if (frame != null)
            {
                if (_bodiesData == null)
                {
                    _bodiesData = new Body[_sensor.BodyFrameSource.BodyCount];
                }

                //If already have bodies, refresh with the latest data
                frame.GetAndRefreshBodyData(_bodiesData);

                //Dispose the frame until we have a new frame
                frame.Dispose();
                frame = null;
            }

            if (_bodiesData == null)
                return;

            //Capture the ID's of the bodies captured by the device in this frame
            List<ulong> trackedIds = new List<ulong>();
            foreach (Body body in _bodiesData)
            {
                //If null, continue the loop
                if (body == null)
                {
                    continue;
                }
                //If the body ies tracked add it to the list of tracked ID's
                if (body.IsTracked)
                {
                    trackedIds.Add(body.TrackingId);
                }
            }

            //First delete all the untracked bodies that are already stored
            for (int i = _knownIDs.Count - 1; i >= 0; --i )
            {
                ulong trackingId = _knownIDs[i];
                if (!trackedIds.Contains(trackingId))
                {
                    _kinectBodies.Remove(trackingId);
                    _knownIDs.RemoveAt(i);
                }

        
               
            }

            //Add TrackedIds
            foreach (ulong trackingId in trackedIds)
            { 
                if(!_knownIDs.Contains(trackingId))
                {
                    _knownIDs.Add(trackingId);
                }
            }

            //If not stored already, create a new body
            foreach (Body body in _bodiesData)
            {
                //If null, continue the loop
                if (body == null)
                {
                    continue;
                }

                //If the body is being tracked, check if it is in our list
                if (body.IsTracked)
                {
                    KinectBody storedBody = null;

                    if (!_kinectBodies.ContainsKey(body.TrackingId))
                    {
                        //If not, store it in our list
                        storedBody = new KinectBody(body.TrackingId);
                        _kinectBodies.Add(body.TrackingId, storedBody);
                    }
                    else
                    {
                        _kinectBodies.TryGetValue(body.TrackingId, out storedBody);
                    }

                    //And refresh it's data (or add it to the object if first time)
                    if (storedBody != null)
                        storedBody.RefreshBody(body);
                }
            }
        }    
	}

    public bool IsTracked(ulong ID)
    {
        return _trackedBodies.ContainsKey(ID) && _trackedBodies[ID];
    }

    public KinectBody GetKinectBody(ulong id,  bool refresh = false)
    {
        
        KinectBody kBody = null;
        if (_kinectBodies.ContainsKey(id))
            _kinectBodies.TryGetValue(id, out kBody);

        if (kBody != null && refresh)
        { 
            //Get the latest frame recorded by the device
            BodyFrame frame = _reader.AcquireLatestFrame();
            //If we have a frame, check if we have bodies, if not create them, and get the data from all the recorded bodies
            if (frame != null)
            {
                if (_bodiesData == null)
                {
                    _bodiesData = new Body[_sensor.BodyFrameSource.BodyCount];
                }

                //If already have bodies, refresh with the latest data
                frame.GetAndRefreshBodyData(_bodiesData);

                //Dispose the frame until we have a new frame
                frame.Dispose();
                frame = null;
            }

            Body b = _bodiesData.FirstOrDefault(x=>x.TrackingId == kBody.TrackingID);
            if (b != null)
                kBody.RefreshBody(b);
        }

        return kBody;
    }

    new public void OnDestroy() //Runtime destroy
    {
        Destroy();
    }

    public void DestroyImmediate() //Editor destroy
    {
        Destroy();
    }

    private void Destroy()
    {
        //If we destory our object, clear our captured references
        if (_reader != null)
        {
            _reader.Dispose();
            _reader = null;
        }

        if (_sensor != null)
        {
            if (_sensor.IsOpen)
            {
                //Close the sensor if opened
                _sensor.Close();
            }

            _sensor = null;
        }
    }

    public string[] GetAllBonesByName()
    {
        List<string> names = new List<string>();
        //Add items to active bones list with the names of the kinect joints
        for (JointType jt = JointType.SpineBase; jt <= JointType.ThumbRight; jt++)
        {
            names.Add(jt.ToString());
        }
        return names.ToArray();
    }

	//ADDED METHODS FOR PLAYERS DISTANCE CHECKING

	public bool DebugBodyPositions = false;

	public KinectBody GetKinectBodyNearestToPos(Vector3 pos, float radius = 0)
		
	{
		ulong id= _knownIDs.OrderBy(x => { return (_kinectBodies[x].Bones.Find(b => b.Name == "SpineBase").StartPosition - pos + 
			                                           new Vector3(0, -(_kinectBodies[x].Bones.Find(b => b.Name == "SpineBase").StartPosition.y - pos.y), 0)).sqrMagnitude; }).FirstOrDefault();

		if (radius > 0) {
			float distance = (GetKinectBody (id).Bones.Find (b => b.Name == "SpineBase").StartPosition - pos + 
				new Vector3(0, -( GetKinectBody (id).Bones.Find (b => b.Name == "SpineBase").StartPosition.y - pos.y), 0)).sqrMagnitude;
			//distance = GetKinectBody (id).Bones.Find (b => b.Name == "SpineBase").StartPosition- pos)sqrMagnitude;
			if ( distance * 2 > radius){
				return null;
			}
		}


		return GetKinectBody(id);
		
	}

	public void OnGUI()
		
	{		
		if(!DebugBodyPositions)return;
		
		GUI.skin.label.fontSize = 50;
		
		foreach (var id in _knownIDs)
			
		{
			
			var rootbonePos =  _kinectBodies[id].Bones.Find(b => b.Name == "SpineBase").StartPosition;
			
			//GUILayout.Label("Id: " + id.ToString() + " - Pos:" + rootbonePos.ToString(),GUILayout.Height(30f));
			//Debug.Log(rootbonePos.ToString());
			GUI.Label( new Rect(0, 0, 1080, 100), "Id: " + id.ToString() + " - Pos:" + rootbonePos.ToString());
			
		}
		
	}
}
#endif