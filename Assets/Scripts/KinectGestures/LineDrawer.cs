using UnityEngine;
using System.Collections;

public static class LineDrawer
{
	private static Material _mat;

	public static void DrawLine(Vector2 Apos, Vector2 Bpos, Color color, float lineWidth = 3.0f)
	{
        if (!_mat)
		{
            /* Credit:  */
            _mat = new Material(Shader.Find("Custom/GizmoShader"));

            _mat.hideFlags = HideFlags.HideAndDontSave;

            _mat.shader.hideFlags = HideFlags.HideAndDontSave;
			
		}
        _mat.SetPass(0);
		
		GL.Begin(GL.TRIANGLE_STRIP);
		
		GL.Color(color);
		
		
		Vector2 dPos = Apos - Bpos;
		Vector2 tan = new Vector2(dPos.y, -dPos.x);
		tan = tan.normalized*lineWidth;
		
		
		GL.Vertex3(Apos.x - tan.x, Apos.y - tan.y, 0);
		GL.Vertex3(Apos.x + tan.x, Apos.y + tan.y,0 );
		GL.Vertex3(Bpos.x - tan.x, Bpos.y - tan.y, 0);
		GL.Vertex3(Bpos.x + tan.x, Bpos.y + tan.y,0 );
		
		GL.End();	
	}

    public static void DrawRect(Vector2 pos, float size, Color color)
    {
        if (!_mat)
        {
            /* Credit:  */
            _mat = new Material(Shader.Find("Custom/GizmoShader"));

            _mat.hideFlags = HideFlags.HideAndDontSave;

            _mat.shader.hideFlags = HideFlags.HideAndDontSave;

        }
        _mat.SetPass(0);

        GL.Begin(GL.TRIANGLE_STRIP);

        GL.Color(color);

        GL.Vertex3(pos.x - size / 2, pos.y - size / 2, 0);
        GL.Vertex3(pos.x - size / 2, pos.y + size / 2, 0);
        GL.Vertex3(pos.x + size / 2, pos.y - size / 2, 0);
        GL.Vertex3(pos.x + size / 2, pos.y + size / 2, 0);

        GL.End();
    }
}

