﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Linq;

public enum ShapeRotation
{
	Undefined = 0,
	Fixed = 1,
	Dynamic = 2
}

public class Shape
{
	//FIELDS
	public LinkedList<Vector2> points;
	public string name;
	public ShapeRotation rotation;
	public bool flippable;

	public Shape()
	{
		points = new LinkedList<Vector2> ();
		name = "";
		rotation = ShapeRotation.Undefined;
		flippable = false;
	}
}

public static class GestureRecognizer
{
	//FIELDS
	private static List<Shape> _storedShapes = new List<Shape> ();
	public static LinkedList<Vector2> simplifiedFigure = new LinkedList<Vector2> ();

	//PROPERTIES
	public static bool Initialized { get; private set; }
	public static List<Shape> StoredShapes { get { return _storedShapes; } set { _storedShapes = value; } }
	
	public static void LoadPossibleShapes(string fileLocation)
	{
		//Read in the possible shapes
		_storedShapes.Clear ();
		
		//Load the xml file
        
		TextAsset textXML = (TextAsset)Resources.Load (fileLocation, typeof(TextAsset));

        if (textXML == null)
        {
            return;
        }

		XmlReader reader = XmlReader.Create(new MemoryStream(textXML.bytes));
		_storedShapes = XDocument.Load(reader).Element("shapes").Elements("shape").Select(
			s=>{ 
				//convert XElement "shape" to Shape()
				return new Shape()
				{
					name = s.Attribute("name").Value,
					rotation = (ShapeRotation)int.Parse(s.Attribute("rotation").Value),
					flippable = bool.Parse(s.Attribute("flippable").Value),

					//use linq to get all points and store in a new linkedlist
					points = new LinkedList<Vector2>(s.Elements("point").Select(
						p=>
						{
					return new Vector2(float.Parse(p.Attribute("x").Value,System.Globalization.CultureInfo.InvariantCulture),float.Parse(p.Attribute("y").Value,System.Globalization.CultureInfo.InvariantCulture));
						})),
				};
			}
		).ToList();

		Initialized = true;
	}

	//METHODS
	public static Shape Recognize(TrackedInput input)
	{
		if(Initialized == false)
			return null;

		//check if we can recognize
		int minimumAmountPoints = 0;
		if(input.TrackedPositions.Count < minimumAmountPoints+1)
			return null;

		//Store information if required before we do our calculations
		Vector2 startPositionFigure = input.TrackedPositions.First.Value;
		
		//STEP 1: Get amount points we want to sample to, starting from the tail
		int amountSamplePoints = input.TrackedPositions.Count; //20
		LinkedList<Vector2> pointsToCheck = GetNthElementsFromTail(input.TrackedPositions, amountSamplePoints);

		//STEP 2: Translate the received figure to 0,0, based on the first point
		TranslateCloud (ref pointsToCheck, new Vector2 (0, 0));

		//STEP 3: Scale the received figure
		//float scaleFactor = 1.0f;
		//ScalePointCloud (ref pointsToCheck, scaleFactor);
        
        //STEP 4: Simplify our figure, based on a difference angle between points
		float diffAngle = 67.5f;
		SimplifyCloud (ref pointsToCheck, diffAngle);

		//TODO test for visualizing
		simplifiedFigure = pointsToCheck;
		TranslateCloud (ref simplifiedFigure, startPositionFigure);

		//TODO make this dynamic.... We will use only last 3 points now to test the smash
		List<LinkedList<Vector2>> simplifiedFigures = new List<LinkedList<Vector2>> ();
		int minimumPoints = 2;
		int maximimPoints = 4;
		for(int i = minimumPoints; i < maximimPoints + 1; ++i)
		{
			simplifiedFigures.Add(GetNthElementsFromTail(simplifiedFigure, i));
		}
		//simplifiedFigure = GetNthElementsFromTail(simplifiedFigure, 4);

		//STEP 5: Compare our simplified figure with the shapes in our database with the same amount of verts
		//TODO make dynamic angle
		Shape matchesShape = null;
		foreach(LinkedList<Vector2> figure in simplifiedFigures)
		{
			matchesShape = Compare(figure, 35.0f);

			if(matchesShape != null)
				return matchesShape;
		}

		//Return either the shape it matches, or null
		return matchesShape;
	}

	public static LinkedList<Vector2> GetNthElementsFromTail(LinkedList<Vector2> list, int n)
	{
		if(list.Count <= 0)
			return null;

		if(list.Count < n)
			n = list.Count;

		//Add current to the list
		LinkedListNode<Vector2> currentNode = list.Last;

		LinkedList<Vector2> newList = new LinkedList<Vector2> ();
		newList.AddLast (currentNode.Value);
		n = n - 1;

		for(; n > 0; --n)
		{
			currentNode = currentNode.Previous;
			newList.AddFirst(currentNode.Value);
		}

		return newList;
	}

	private static void ScalePointCloud(ref LinkedList<Vector2> list, float factorX, float factorY)
	{
		if(list != null && list.Count <= 0)
			return;
		for(LinkedListNode<Vector2> begin = list.First; begin != null; begin = begin.Next)
		{
			begin.Value = new Vector2((begin.Value.x * factorX) / Screen.width, (begin.Value.y * factorY) / Screen.height);
		}
	}

	private static void TranslateCloud(ref LinkedList<Vector2> list, Vector2 pos)
	{
		if(list != null && list.Count <= 0)
			return;
        Vector2 difference = pos - list.First.Value;
		for(LinkedListNode<Vector2> begin = list.First; begin != null; begin = begin.Next)
		{
			begin.Value = new Vector2(begin.Value.x + difference.x, begin.Value.y + difference.y);
        }
	}

	private static void RotateCloud(ref LinkedList<Vector2> list, float angle)
	{
		if(list != null && list.Count <= 0)
			return;

		Vector2 originPoint = list.First.Value;
		for(LinkedListNode<Vector2> p = list.First; p != null; p = p.Next)
		{
			Vector2 rotatedVert = p.Value;
			rotatedVert.x = Mathf.Cos(angle) * (rotatedVert.x - originPoint.x) - Mathf.Sin(angle) * (rotatedVert.y - originPoint.y) + originPoint.x;
			rotatedVert.y = Mathf.Sin(angle) * (rotatedVert.x - originPoint.x) + Mathf.Cos(angle) * (rotatedVert.y - originPoint.y) + originPoint.y;
			p.Value = rotatedVert;
		}
	}

	private static void SimplifyCloud(ref LinkedList<Vector2> list, float diffAngle)
	{
		//Check if we have enough points, min 2
		if(list.Count < 2)
			return;

		//Create new container for simplified figure, get first node and store it already
		LinkedList<Vector2> simplifiedFigure = new LinkedList<Vector2> ();
		LinkedListNode<Vector2> currentNode = list.First;
		simplifiedFigure.AddLast (currentNode.Value);

		//For all the points in our figure, compare it with the last added point and if the 
		//difference angle is bigger then defined one, store it and continue using that one.
		for(LinkedListNode<Vector2> point = list.First; point != null; point = point.Next)
        {
			//If this point is the same as the one stored, go to the next one
			if(currentNode == point)
				continue;

			//If our point should be the last one in the list, store it no matter what difference
			if(point == list.Last)
			{
				simplifiedFigure.AddLast(point.Value);
			}
			else
			{
				//Calculate difference angle between both points and use the absolute value
				Vector2 line1 = currentNode.Next.Value - currentNode.Value;
				Vector2 line2 = point.Next.Value - point.Value;
				float currentAngle = Vector2.Angle(line1, line2);

				//If difference is big enough, store it
				if(currentAngle >= diffAngle)
				{
					simplifiedFigure.AddLast(point.Value);
					currentNode = point;
				}
			}
		}

		//In the end, clear the input list and replace with simplified version
		list.Clear ();
		list = simplifiedFigure;
	}

	//TODO was a ref List, now a value like one
	private static Shape Compare(LinkedList<Vector2> list, float maxErrorDifferenceAngle)
	{
		//Iterate over all the shapes in the database with the same amount of of verts
		int amountPointsFigure = list.Count;

		foreach(Shape shape in _storedShapes)
		{
            if (shape.points.Count != amountPointsFigure)
                continue;

			bool matched = CheckFigureWithShape(ref list, shape, maxErrorDifferenceAngle);

			//If matched, return true, else we continue our search
			if(matched == true)
				return shape;
        }

		//But if we didn't return true, none was matched
		return null;
	}

	private static bool CheckFigureWithShape(ref LinkedList<Vector2> figure, Shape shape, float maxErrorDifferenceAngle)
	{
		//Recheck if our shape and figure has same amount of verts and a minimum of 2
		if((figure.Count != shape.points.Count) || figure.Count < 2)
			return false;

		//Check if our figure may be rotated, based on the shape. If rotation is dynamic, we rotate
		//our shape to the figure (based on first point vector).
		if(shape.rotation == ShapeRotation.Dynamic)
		{
			//Create the first vector, to defined the angle we need to rotate our figure
			Vector2 dirFigureVert = figure.First.Next.Value - figure.First.Value;
			Vector2 dirShapeVert = shape.points.First.Next.Value - shape.points.First.Value;

			float angle = Vector2.Angle(dirFigureVert.normalized, dirShapeVert.normalized);
			RotateCloud(ref shape.points, -angle);
		}

		//Compare all the verts with eachother, using the angle between them as error that is allowed.
		LinkedListNode<Vector2> currentNodeFigure = figure.First;
		LinkedListNode<Vector2> currentNodeShape = shape.points.First;
		//For loop, using external variables, 2 condition checks and 2 "increments"
		for(; (currentNodeFigure != null) && (currentNodeShape != null); 
		    currentNodeFigure = currentNodeFigure.Next , currentNodeShape = currentNodeShape.Next)
		{
			Vector2 dirFigureVert = Vector2.zero;
			Vector2 dirShapeVert = Vector2.zero;
			//Create the vector between this point and the next point (direction)
			//If there is no next node, base on this vert and the previous vert
			if(currentNodeFigure.Next != null)
			{
				dirFigureVert = currentNodeFigure.Next.Value - currentNodeFigure.Value;
				dirShapeVert = currentNodeShape.Next.Value - currentNodeShape.Value;
			}
			else
			{
				dirFigureVert = currentNodeFigure.Value - currentNodeFigure.Previous.Value;
				dirShapeVert = currentNodeShape.Value - currentNodeShape.Previous.Value;
			}
			
			//Calculate angle and check with the error if it is not too big
			float angle = Vector2.Angle(dirFigureVert.normalized, dirShapeVert.normalized);

			//If it is flippable, check it with the angle calculated with the flipped direction (x only)
			if(shape.flippable == true)
			{
				float flippedAngle = Vector2.Angle(dirFigureVert.normalized, new Vector2(dirShapeVert.normalized.x * -1.0f, dirShapeVert.normalized.y));
				if((angle > Mathf.Abs(maxErrorDifferenceAngle)) && (flippedAngle > Mathf.Abs(maxErrorDifferenceAngle)))
				{
					return false; //We no it can not match, so stop iterating
				}
			}
			else if(shape.flippable == false)
			{
				if(angle > Mathf.Abs(maxErrorDifferenceAngle))
				{
					return false; //We no it can not match, so stop iterating
				}
			}
			else
				return false;
        }

		//If we did not break, we have matched it!
		return true;
    }
}
