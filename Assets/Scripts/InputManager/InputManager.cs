//Usings
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;
using System.Xml.Linq;
using System.Xml;
using System.Linq;

//-----------------------
//ENUM
//-----------------------
public enum GestureType
{
	None = 0,
	Custom = 1,
	Tap = 2,
	PressedTap = 4,
	Motion = 8,
	RawInput = 16,
	Touch = 32,
    KinectSequence = 64,
    KinectPose = 128
}

public enum RecordingType
{
	TimeBased = 0,
	PixelBased = 1
}

//-----------------------
//DATA TYPES
//-----------------------
public class Gesture
{
	//FIELDS
	LinkedList<Vector2> _storedPositions = new LinkedList<Vector2>();

	//PROPERTIES
	public GestureType Type { get; private set; }
	public string Name { get; private set; }
	public TrackedInput InputSource { get; private set; }
	public LinkedList<Vector2> StoredPositions { get { return _storedPositions; } set{_storedPositions = value;} }
	
	//CONSTRUCTOR
	public Gesture(GestureType type, TrackedInput inputSource, string name = "")
	{
		this.Type = type;
		this.Name = name;
		this.InputSource = inputSource;
	}
}

//-----------------------
//BASE CLASSES
//-----------------------
public class TrackedInput 
{
	//FIELDS
	private Touch? _touchInput = null;
	private Vector3 _lastMousePosition = Vector3.zero;
	private LinkedList<Vector2> _trackedPositions = new LinkedList<Vector2> ();
	private float _trackingTimer = 0.0f;
	private float _prevPointTrack = 0.0f;
	private float _minDistanceBetweenTrackPoint = 30.0f;
	
	//PROPERTIES
	public int ID { get; private set; }
	public Vector2 Position { get; private set; }
	public Vector2 StartPosition { get; private set; }
	public Vector2 PreviousPosition { get { return _lastMousePosition; } }
	public float DeltaTime { get; private set; }
	public Vector2 DeltaPosition { get; private set; }
	public TouchPhase PreviousPhase { get; private set; }
	public TouchPhase Phase { get; private set; }
	public int TapCount { get; private set; }
	public float TimeInUse { get; private set; }
	public Vector2 Travel { get; private set; }
	public bool DeleteRecordAfterTime { get; private set; }
	public Vector2 NormalizedTravel 
	{ 
		get 
		{
			return Travel == Vector2.zero ? Vector2.zero : new Vector2(Travel.x / Screen.width, Travel.y / Screen.height);
		}
	}
	public LinkedList<Vector2> TrackedPositions { get{return _trackedPositions;} }
	
	public bool DestroyAfterUse { get; private set; }
	public bool TrackingInput { get; set; }
	public int MaximumAmountTrackablePoints { get; set; }
	public float TimeTrackedPointsAreStored { get; set; }
	public float NormalizedDisplacementBetweenTracking { get; set; }
	public RecordingType RecordingFormat { get ; set; }
	public bool FlaggedForDeletion { get; set; }
	
	//CONSTRUCTORS
	public TrackedInput(Touch touchInput, bool destroyAfterUse = true)
	{
		if(((Touch?)touchInput).HasValue)
		{
			//Store reference object
			_touchInput = touchInput;
			//Set values
			ID = _touchInput.Value.fingerId;
			StartPosition = _touchInput.Value.position;
			Position = _touchInput.Value.position;
			DeltaTime = _touchInput.Value.deltaTime;
			DeltaPosition = _touchInput.Value.deltaPosition;
			Phase = _touchInput.Value.phase;
			PreviousPhase = Phase;
			TapCount = _touchInput.Value.tapCount;
			Travel = Vector2.zero;
			
			//Handling values
			DestroyAfterUse = destroyAfterUse;
			TrackingInput = true;
			MaximumAmountTrackablePoints = 100; //Must be less if pixel based : 20
			TimeTrackedPointsAreStored = 0.5f;
			NormalizedDisplacementBetweenTracking = 0.05f;
			RecordingFormat = RecordingType.PixelBased;
		}
	}
	
	public TrackedInput(int mouseID, bool destroyAfterUse = true)
	{
		//Set values
		ID = mouseID;
		StartPosition = Input.mousePosition;
		Position = Input.mousePosition;
		_lastMousePosition = Position;
		DeltaTime = Time.deltaTime;
		DeltaPosition = Input.mousePosition - _lastMousePosition;
		Phase = (Input.GetMouseButtonDown(ID) ? TouchPhase.Began :
		        (this.DeltaPosition.sqrMagnitude > 1.0f ? TouchPhase.Moved :
		        TouchPhase.Stationary));
		PreviousPhase = Phase;
		TapCount = 1;
		Travel = Vector2.zero;
		
		//Handling values
		DestroyAfterUse = destroyAfterUse;
		TrackingInput = true;
		MaximumAmountTrackablePoints = 100; //Must be less if pixel based : 20
		TimeTrackedPointsAreStored = 0.5f;
		NormalizedDisplacementBetweenTracking = 0.05f;
		RecordingFormat = RecordingType.TimeBased;
	}
	
	//METHOD
	public void Update()
	{
		//Update variables
		if(_touchInput.HasValue)
		{
			//Update it's reference to the touch, if still active
			List<Touch> activeTouches = new List<Touch>(Input.touches);
			_touchInput = (Touch?)activeTouches.Find(x => x.fingerId == this.ID);
			if(!_touchInput.HasValue)
			{
				FlaggedForDeletion = true;
				return;
			}

			if(PreviousPhase != Phase)
				PreviousPhase = Phase;
			Phase = _touchInput.Value.phase;

			if(Phase == TouchPhase.Began)
			{
				_lastMousePosition = Position;
				Travel = Vector2.zero;
				_prevPointTrack = 0.0f;
				_trackedPositions.Clear();
			}

			if(Phase != TouchPhase.Ended)
			{
				_lastMousePosition = Position;
				DeltaPosition = _touchInput.Value.deltaPosition;
			}

			Position = _touchInput.Value.position;
			DeltaTime = _touchInput.Value.deltaTime;
			TapCount = _touchInput.Value.tapCount;
			
			if((DestroyAfterUse == false) && 
			   (Phase == TouchPhase.Ended || Phase == TouchPhase.Canceled))
				TimeInUse = 0.0f;
			else
				TimeInUse += Time.deltaTime;

			//TODO implement travel and Previous Position
			//Calculate travel
			if(Phase == TouchPhase.Moved)
				Travel += new Vector2(Mathf.Abs(DeltaPosition.x), Mathf.Abs(DeltaPosition.y));
		}
		else
		{
			if(PreviousPhase != Phase)
				PreviousPhase = Phase;

			if(Phase == TouchPhase.Began)
			{
				_lastMousePosition = Position;
				Travel = Vector2.zero;
				_prevPointTrack = 0.0f;
				_trackedPositions.Clear();
				_trackedPositions.AddLast(Position);
			}

			_lastMousePosition = Position;
			Position = Input.mousePosition;
			DeltaTime = Time.deltaTime;
			DeltaPosition = Input.mousePosition - _lastMousePosition;
			Phase = (Input.GetMouseButtonDown(ID) ? TouchPhase.Began :
			         (((Input.GetMouseButton(ID)) && (this.DeltaPosition.sqrMagnitude > 1.0f)) ? TouchPhase.Moved :
			 (Input.GetMouseButtonUp(ID) ? TouchPhase.Ended :
			 (Input.GetMouseButton(ID) ? TouchPhase.Stationary :
			 TouchPhase.Canceled))));
			TapCount = 1;
			
			if((DestroyAfterUse == false) && (Phase == TouchPhase.Canceled))
				TimeInUse = 0.0f;
			else
				TimeInUse += Time.deltaTime;

			//Calculate travel
			if(Phase == TouchPhase.Moved)
				Travel += new Vector2(Mathf.Abs(DeltaPosition.x), Mathf.Abs(DeltaPosition.y));
		}

		//Record
		if(TrackingInput == true)
			RecordInput (TrackingInput);
	}
	
	public void Destroy()
	{
		_trackedPositions.Clear ();
		_touchInput = null;
	}
	
	private void RecordInput(bool record)
	{
		if(record == false)
			return;
		
		//Record our displacement if moving (keep in mind our maximum and min distance)
		if(Phase == TouchPhase.Moved)
		{
			if(RecordingFormat == RecordingType.TimeBased)
				TimeBasedTracking();
			else if(RecordingFormat == RecordingType.PixelBased)
				PixelBasedTracking();

			_trackingTimer = 0.0f;
		}
		else if (DeleteRecordAfterTime == true) //If our input is "stationary" for x amount of time, get rid of stored points
		{
			_trackingTimer += Time.deltaTime;
			
			if(_trackingTimer >= TimeTrackedPointsAreStored)
			{
				_trackedPositions.Clear();
				_trackingTimer = 0.0f;
			}
		}
	}

	private void PixelBasedTracking()
	{
		if(_trackedPositions.Count == 0)
			_trackedPositions.AddLast(Position);

		//TODO change with scaling... right now pixel coded not checking the resolution
		float currentDistance = (_trackedPositions.Last.Value - Position).sqrMagnitude;
		
		if(currentDistance >= (_minDistanceBetweenTrackPoint * _minDistanceBetweenTrackPoint))
		{
			int numPoints = (int)(Mathf.Sqrt(currentDistance) / _minDistanceBetweenTrackPoint);
			
			for(int i = 1; i <= numPoints; ++i)
			{
				if(_trackedPositions.Count > MaximumAmountTrackablePoints)
					_trackedPositions.RemoveFirst();
				
				Vector2 dir = (Position - _trackedPositions.Last.Value).normalized;
				Vector2 newPoint = _trackedPositions.Last.Value + dir * _minDistanceBetweenTrackPoint;
				_trackedPositions.AddLast(newPoint);
			}
		}
	}

	private void TimeBasedTracking()
	{
		float diffPrevTrackedPoint = Travel.magnitude - _prevPointTrack;
		if(diffPrevTrackedPoint >= NormalizedDisplacementBetweenTracking)
		{
			//If we reached our maximum pop first position
			if(_trackedPositions.Count > MaximumAmountTrackablePoints)
				_trackedPositions.RemoveFirst();
			
			_trackedPositions.AddLast(new Vector2(Position.x, Position.y));
			
			_prevPointTrack += NormalizedDisplacementBetweenTracking; //keep to track of previous target to check for next one
		}
	}
}

public class InputLogFrame
{
	public List<Vector2> Figure = new List<Vector2>();
	public List<Vector2> SimplifiedFigure = new List<Vector2> ();
	public float RegisteredForce = 0.0f;
	public string DT;
	public bool success = false;
}

public class InputManager : MonoBehaviour 
{
	//-----------------------
	//INPUT MANAGER
	//-----------------------
	//EDITOR FIELDS
	private Texture2D dotTexture;
    private Texture2D dotTexture2;
	public Action EditorMotionGestureFound = null;
	public bool EditorRecording { get; set; }

    //FIELDS
	private List<TrackedInput> _activeInput = new List<TrackedInput> ();
	private bool _createdMouseInput = false;
	private GestureType _trackingMask = GestureType.None; //The gesture we don't to check for can be set here using binary flags
	private List<Gesture> _activeGestures = new List<Gesture> ();
	private List<Gesture> _unknownGestures = new List<Gesture> ();

#if UNITY_DEBUG_LOGGING
	private bool _loggingEnabled = true;
	private List<InputLogFrame> _loggedInfo = new List<InputLogFrame>();
    private List<KeyValuePair<Vector2,float>> _debugTaps = new List<KeyValuePair<Vector2, float>>();
	private List<KeyValuePair<Vector2,float>> _debugPressedTaps = new List<KeyValuePair<Vector2,float>>();
#endif

    //PROPERTIES
	public float MaximumOneTapHold { get; set; }
	public float MinimumPressedTapHold { get; set; }
	public List<Gesture> ActiveGestures { get { return _activeGestures; } }
	public List<Gesture> UnknownGestures { get { return _unknownGestures; } }
    public List<TrackedInput> TrackedInput { get { return _activeInput; } }
	public int AmountTrackedInputs { get {return _activeInput.Count;} }
	public bool SupportMultiTouch { get; set; }
	
    //FIELDS
    private Dictionary<string, float> _matchPoses = new Dictionary<string, float>();

	//Singleton
	private static InputManager _defaultManager;
	public static InputManager Default
	{
		get
		{
			if(_defaultManager == null)
			{
				//Check if manager is not allready created, and if it has, store the
				//first one created.
				_defaultManager = (InputManager)FindObjectOfType(typeof(InputManager));
				
				if ( FindObjectsOfType(typeof(InputManager)).Length > 1 )
				{
					Debug.LogError("InputManager: there should never be more than 1 InputManager!");
					return _defaultManager;
				}
				
				//If we didn't find one, make one
				if(_defaultManager == null)
				{
					GameObject inputManagerObject = new GameObject("Default InputManager");
					_defaultManager = inputManagerObject.AddComponent<InputManager>();
				}
			}
			return _defaultManager;
		}
	}

	//METHODS
	void Start()
	{
		//Disable raw input temp
		EnableRawInput (false);

		//Set variables
		MaximumOneTapHold = 0.25f;
		MinimumPressedTapHold = 0.1f; //was 0.25fS

		//Read in all the gesture shapes
		GestureRecognizer.LoadPossibleShapes ("GestureShapes");
	}

	void Update()
	{
		_activeGestures.Clear ();
		_unknownGestures.Clear ();

		//Create/Store input that needs to be tracked if necessary
		TrackInput ();

		//Analise all possible gestures
		AnalyseRawInput ();
		AnalyseOneTap ();
		AnalyseTouch();
		AnalysePressedTap ();
		AnalyseMotion ();
       
		AnalyseCustomGestures ();
        

#if UNITY_KINECT
        AnalyseKinectPoses();
        AnalyseKinectSequences();
#endif

		//Logging
#if UNITY_DEBUG_LOGGING
		LogInput ();

		//Timer + remove timed out
		for(int i = _debugTaps.Count -1; i >= 0; --i)
		{
			KeyValuePair<Vector2,float> dbTap = _debugTaps[i];
			float timer = dbTap.Value - Time.deltaTime;

			if(timer <= 0f)
			{
				_debugTaps.RemoveAt(i);
			}
			else
			{
				_debugTaps[i] = new KeyValuePair<Vector2,float>(dbTap.Key,timer);
			}
		}

		//Add debugtaps
		foreach(Vector2 tap in _activeGestures.Where(g=>g.Type == GestureType.Touch).Select(t=>t.InputSource.Position))
		{
			_debugTaps.Add(new KeyValuePair<Vector2,float>(tap,2.0f));
		}

		//Timer + remove timed out
		for(int i = _debugPressedTaps.Count -1; i >= 0; --i)
		{
			KeyValuePair<Vector2,float> dbTap = _debugPressedTaps[i];
			float timer = dbTap.Value - Time.deltaTime;
			
			if(timer <= 0f)
			{
				_debugPressedTaps.RemoveAt(i);
			}
			else
			{
				_debugPressedTaps[i] = new KeyValuePair<Vector2,float>(dbTap.Key,timer);
			}
		}
		
		//Add Pressedtaps
		foreach(Vector2 tap in _activeGestures.Where(g=>g.Type == GestureType.PressedTap).Select(t=>t.InputSource.Position))
		{
			_debugPressedTaps.Add(new KeyValuePair<Vector2,float>(tap,2.0f));
		}
#endif
    }

	void LateUpdate()
	{}

	void OnDisable()
	{
		//SerializeLogInput ();
    }

	private void TrackInput()
	{
		//First check if input has ended or if it has been flagged, if it does release it from it's duties
		List<TrackedInput> flaggedForRemoval = new List<TrackedInput>();
		foreach(TrackedInput input in _activeInput)
		{
			if((input.Phase == TouchPhase.Ended || input.PreviousPhase == TouchPhase.Ended
			    || input.Phase == TouchPhase.Canceled) && input.DestroyAfterUse == true
			    || input.FlaggedForDeletion == true)
				flaggedForRemoval.Add(input);
		}
		
		//Cleanup unused inputs
		foreach(TrackedInput flaggedInput in flaggedForRemoval)
		{
			_activeInput.Remove(flaggedInput);
		}
		flaggedForRemoval.Clear ();

		//Check if we must create input for a mouse (this should only be done once because we keep track of it)
		if(_createdMouseInput == false)
		{
			//Create mouse input for all Windows, Linux and OSX devices
			//|| UNITY_WINRT
			#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
				_activeInput.Add(new TrackedInput(0, false)); //left mouse button
				//_activeInput.Add(new TrackedInput(1, false)); //right mouse button
			#endif
			_createdMouseInput = true;
		}

		//Check for all the active touches if they are being tracked
		List<Touch> touches = new List<Touch> (Input.touches);
		foreach(Touch t in touches)
		{
			//Find if input is being tracked
			TrackedInput ti = _activeInput.Find(x => x.ID == t.fingerId);
			//if not, track it
			if(ti == null)
				_activeInput.Add(new TrackedInput(t, true));
		}

		//Update all the current input
		foreach(TrackedInput input in _activeInput)
		{
			input.Update();
		}
	}

	private void AnalyseRawInput()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.RawInput) == GestureType.RawInput)
			return;
		
		//Check if we've tapped the screen and released it in a given interval
		foreach(TrackedInput input in _activeInput)
		{
			Gesture gesture = new Gesture(GestureType.RawInput, input);
			_activeGestures.Add(gesture);

			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}
	}

	private void AnalyseTouch()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.Touch) == GestureType.Touch)
			return;
		foreach(TrackedInput input in _activeInput)
		{
			if(input.Phase == TouchPhase.Began)
			{
				
				Gesture gesture = new Gesture(GestureType.Touch, input);
				_activeGestures.Add(gesture);
			}
			
			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}
	}

	private void AnalyseOneTap()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.Tap) == GestureType.Tap)
			return;

		//Check if we've tapped the screen and released it in a given interval
		foreach(TrackedInput input in _activeInput)
		{
			if(input.Phase == TouchPhase.Ended && input.PreviousPhase != TouchPhase.Moved) //Still need to check if we haven't moved too much
			{
				if(input.TimeInUse >= MaximumOneTapHold)
					return;

				//Debug.Log("I've TAPPED");
				//Create gesture and store it
				Gesture gesture = new Gesture(GestureType.Tap, input);
				_activeGestures.Add(gesture);
			}


			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}

	}

	private void AnalysePressedTap()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.PressedTap) == GestureType.PressedTap)
			return;
		
		//Check if we've tapped the screen and released it in a given interval
		foreach(TrackedInput input in _activeInput)
		{
			if(input.Phase != TouchPhase.Ended && input.PreviousPhase != TouchPhase.Canceled) //Still need to check if we haven't moved to much
			{
				if(input.TimeInUse <= MinimumPressedTapHold)
					return;
				
				//Debug.Log("I'm holding the tab");
				//Create gesture and store it
				Gesture gesture = new Gesture(GestureType.PressedTap, input);
				_activeGestures.Add(gesture);
			}

			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}
	}

	private void AnalyseMotion()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.Motion) == GestureType.Motion)
			return;

		//Check if we are "swiping" the screen, we call it motion now :)
		foreach(TrackedInput input in _activeInput)
		{
			//COULD ADD: input.Phase == TouchPhase.Began ||
			if(input.Phase == TouchPhase.Moved || input.Phase == TouchPhase.Ended)
			{
				//Create gesture and store it
				//Debug.Log("I'm moving... creating motion baby!");
				Gesture gesture = new Gesture(GestureType.Motion, input);
				_activeGestures.Add(gesture);
			}

			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}
	}


	private void AnalyseCustomGestures()
	{
		//If we flagged it as disabled, return
		if((_trackingMask & GestureType.Custom) == GestureType.Custom)
			return;
		
		//Check if we are making a figure
		foreach(TrackedInput input in _activeInput)
		{
			//input.Phase == TouchPhase.Moved
			if(true) //Apply a test if you want to. Eg: now only check if we are moving
			{
				//Check if we've made a shape
				Shape matchingShape = GestureRecognizer.Recognize (input);

				if(matchingShape != null)
				{
					//Create gesture and store it
					//Debug.Log("I made the '" + matchingShape.name + "' gesture!!");
					Gesture gesture = new Gesture(GestureType.Custom, input, matchingShape.name);
					_activeGestures.Add(gesture);

					if(input.Phase == TouchPhase.Ended)
					{
						if(EditorMotionGestureFound != null && EditorRecording == true)
						{
							//Also store the points to visualize in editor
							gesture.StoredPositions = matchingShape.points;
							//Store the gesture
							_unknownGestures.Add(gesture);
                            //Invoke the action for the editor
							EditorMotionGestureFound.Invoke();
                        }
                    }
                }
				else
				{
					//If shape not found, check if our definer is open. If it is, check if linked and inform it
					//of the new shape
					if(input.Phase == TouchPhase.Ended)
					{
						if(EditorMotionGestureFound != null && EditorRecording == true)
						{
							//Store the gesture
							Gesture gesture = new Gesture(GestureType.None, input, "");
							_unknownGestures.Add(gesture);
							//Invoke the action for the editor
							EditorMotionGestureFound.Invoke();
						}
					}
				}
			}

			//If only one touch/input is allowed, return from loop
			if(SupportMultiTouch == false)
				return;
		}
	}
    private void AnalyseKinectPoses()
    {
        if ((_trackingMask & GestureType.KinectPose) == GestureType.KinectPose)
            return;

        _matchPoses = KinectGestureRecognizer.Instance.RecognizeMatchPoses();
                
    }
    private void AnalyseKinectSequences()
    {
        //If we flagged it as disabled, return
        if ((_trackingMask & GestureType.KinectSequence) == GestureType.KinectSequence)
            return;

        //Check if we've completed any sequences
        List<ComparableSequence> completedSequences = new List<ComparableSequence>();
        KinectGestureRecognizer.Instance.RecognizeSequences(ref completedSequences);

        //For all the sequences we've completed, store a gesture so we can handle it from here on
        foreach(ComparableSequence completedSequence in completedSequences)
        {
            if (completedSequence == null)
                continue;

            //Tracked input is null because we don't have any independend input. Our device is the kinect
            //but the input is raw input from the kinect that has been processed.
            //TODO: create a new trackedinput variant that stores sequence info, then we have more additional info about the gesture
            //then the name alone.
            Gesture gesture = new Gesture(GestureType.KinectSequence, null, completedSequence.Name);
            _activeGestures.Add(gesture);
        }
    }

	private void DebugTest()
	{
		TrackedInput i = _activeInput[0];
		if(i.Phase == TouchPhase.Began && i.PreviousPhase != i.Phase)
			Debug.Log("Mouse " + i.ID + " has been Triggered!");
		if(i.Phase == TouchPhase.Moved)
			Debug.Log("Mouse " + i.ID + " has Moved!");
		if(i.Phase == TouchPhase.Stationary && i.PreviousPhase != i.Phase)
			Debug.Log("Mouse " + i.ID + " is Stationary!");
		if(i.Phase == TouchPhase.Ended && i.PreviousPhase != i.Phase)
			Debug.Log("Mouse " + i.ID + " is Ended!");
		if(i.Phase == TouchPhase.Canceled && i.PreviousPhase != i.Phase)
			Debug.Log("Mouse " + i.ID + " is Canceled!");
    }

	//SETTERS
	public void EnableOneTouch(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.Touch;
		else
			_trackingMask |= GestureType.Touch;
	}

	public void EnableOneTab(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.Tap;
		else
			_trackingMask |= GestureType.Tap;
	}

	public void EnablePressedTab(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.PressedTap;
		else
			_trackingMask |= GestureType.PressedTap;
	}

	public void EnableMotion(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.Motion;
		else
			_trackingMask |= GestureType.Motion;
	}

	public void EnableCustomGestures(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.Custom;
		else
			_trackingMask |= GestureType.Custom;
	}

    public void EnableKinectSequences(bool isEnabled)
    {
        if (isEnabled)
            _trackingMask &= ~GestureType.KinectSequence;
        else
            _trackingMask |= GestureType.KinectSequence;
    }

	public void EnableRawInput(bool isEnabled)
	{
		if(isEnabled)
			_trackingMask &= ~GestureType.RawInput;
		else
			_trackingMask |= GestureType.RawInput;
    }
    
    //GETTER
    public float GetMatchPosePercent(string pose)
    {
        if (!_matchPoses.ContainsKey(pose)) return 0f;
        return _matchPoses[pose];
    }

    public Gesture FindGesture(string name)
    {
        return ActiveGestures.Find(x => x.Name == name);
    }

	//DEBUG LOGGING
#if UNITY_DEBUG_LOGGING
	private void LogInput()
	{
		if(_loggingEnabled == true)
		{
			foreach(TrackedInput input in _activeInput)
			{
				if(input.Phase == TouchPhase.Ended)
				{
					Gesture foundGesture = _activeGestures.Find(x => x.InputSource == input && x.Type == GestureType.Custom);
					//Check on custom gesture. Atm this is only smash. Later we would need to check for shape aswell
					/*if(foundGesture != null)
						Debug.Log("GestureType: " + foundGesture.Type);*/
					if(foundGesture != null && foundGesture.Type == GestureType.Custom)
					{
						//If we found the gesture, store the figure and the simplified 
						InputLogFrame f = new InputLogFrame();
						f.Figure = new List<Vector2>(input.TrackedPositions);
						f.SimplifiedFigure = new List<Vector2>(GestureRecognizer.simplifiedFigure);

						//Calculate the force and store it
						Vector2 dir = foundGesture.InputSource.DeltaPosition;
						float angle = Vector2.Angle(dir.normalized, new Vector2(0,-1));
						Vector2 yVelocity = new Vector2(0.0f, foundGesture.InputSource.DeltaPosition.y);
						float strength = yVelocity.magnitude;// / Screen.height;
						
						//TODO recreate power, no disabled for issue with touch
						//requiredSmashPower = (Screen.height / requiredSmashPower) / Screen.height;
						strength = yVelocity.magnitude / Screen.height;
						f.RegisteredForce = strength;

						f.DT = DateTime.Now.ToString();

						float requiredSmashPower = 20.0f / Screen.height;
						if(angle <= 45.0f && (strength > requiredSmashPower))
						{
							f.success = true;
						}

						//ADD IT
						_loggedInfo.Add(f);

						//Extra Log for GUI debugger
						LogData.SmashDetected = true;
						LogData.RequiredForce = requiredSmashPower;
						LogData.SmashForce = strength;
					}
					else
						LogData.SmashDetected = false;
				}
			}
		}
	}

	private void SerializeLogInput()
	{
		if(_loggingEnabled == true)
		{
			//Write to XML, so we can parse it later
			XmlDocument doc = new XmlDocument();
			XmlElement b = doc.CreateElement("Root");

			foreach(InputLogFrame figure in _loggedInfo)
			{
				XmlElement root = doc.CreateElement("LoggedInfo");
				XmlElement f = doc.CreateElement("Figure");
				XmlElement sf = doc.CreateElement("SimplifiedFigure");
				XmlElement force = doc.CreateElement("Force");
				XmlElement date = doc.CreateElement("Date");
				XmlElement s = doc.CreateElement("Success");

				foreach(Vector2 v in figure.Figure)
				{
					XmlElement vE = doc.CreateElement("Point");
					vE.SetAttribute("x", v.x.ToString());
					vE.SetAttribute("y", v.y.ToString());

					f.AppendChild(vE);
				}

				foreach(Vector2 v in figure.SimplifiedFigure)
				{
					XmlElement vE = doc.CreateElement("Point");
					vE.SetAttribute("x", v.x.ToString());
					vE.SetAttribute("y", v.y.ToString());
					
					sf.AppendChild(vE);
				}

				force.SetAttribute("value", figure.RegisteredForce.ToString());
				date.SetAttribute("value", figure.DT);
				s.SetAttribute("value", figure.success.ToString());

				root.AppendChild(f);
				root.AppendChild(sf);
				root.AppendChild(force);
				root.AppendChild(date);
				root.AppendChild(s);

				b.AppendChild(root);
			}



			doc.AppendChild(b);

			string path = Application.persistentDataPath; //c:/users/dae_surface0/appdata/local/packages/zombieswipeclean_pzq3xp76mxafg/localstate/log_session_60231578.8223.xml
			doc.Save(path +"/Log_Session_"+DateTime.Now.TimeOfDay.TotalMilliseconds.ToString()+".xml");
            Debug.Log(path);
			Debug.Log("Log saved");
		}
    }

	//TEST
	void OnGUI()
	{
		foreach(KeyValuePair<Vector2,float> tapPair in _debugTaps)
		{
			float pct = tapPair.Value*0.5f;
			GUI.color = new Color(1f,1f,1f,pct);
			Vector2 tap = tapPair.Key;
			GUI.DrawTexture(new Rect(tap.x - 10, Screen.height - tap.y - 10, 20,20),dotTexture);
		
		}
		foreach(KeyValuePair<Vector2,float> tapPair in _debugPressedTaps)
		{
			float pct = tapPair.Value*0.5f;
			GUI.color = new Color(1f,1f,1f,pct);
			Vector2 tap = tapPair.Key;
			GUI.DrawTexture(new Rect(tap.x - 10, Screen.height - tap.y - 10, 20,20),dotTexture2);
			
		}

		GUI.color = Color.white;
	}
#endif
}