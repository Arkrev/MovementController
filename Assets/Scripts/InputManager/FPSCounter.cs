﻿using UnityEngine;
using System.Collections;

#if UNITY_DEBUG_LOGGING
public static class LogData
{
	public static bool SmashDetected = false;
	public static float RequiredForce = 0.0f;
	public static float SmashForce = 0.0f;
}

public class FPSCounter : MonoBehaviour 
{

	float updateInterval = 0.5f;

	private float accum = 0f;
	private uint UpdateFrames = 0;
	private uint RenderFrames = 0;
	private uint FUFrames = 0;
	private float timeLeft;
    private float LowestUpdateFrames = 60;

	private string  fpsUpdate = "";
	private string fpsRender = "";
	private string fpsPhysX = "";

	private float _highestDeltaY = 0.0f;

	// Use this for initialization
	void Start () 
    {
		timeLeft = updateInterval;
	}
	
	// Update is called once per frame
	void Update ()
	{
		timeLeft -= Time.deltaTime;
		accum += Time.deltaTime;
		++UpdateFrames;

		if(timeLeft <= 0f)
		{
			timeLeft += updateInterval;

			fpsUpdate = "Update:  " + (UpdateFrames/accum).ToString("f2");
			fpsRender = "Render:  " + (RenderFrames/accum).ToString("f2");
			fpsPhysX = "Physics: " + (FUFrames/accum).ToString("f2");

			accum = 0f;
			UpdateFrames = 0;
			RenderFrames = 0;
			FUFrames = 0;
		}

        if (UpdateFrames / accum < LowestUpdateFrames)
            LowestUpdateFrames = UpdateFrames / accum;
	}

	void FixedUpdate()
	{
		++FUFrames;
	}

	void OnPostRender()
	{
		++RenderFrames;
	}

	void OnGUI()
	{
        GUI.Label(new Rect(10, 70, 200, 40), fpsUpdate);
		GUI.Label(new Rect(10,90,200,40),fpsRender);
        /*GUI.contentColor = Color.green;
		if(showFPSupdate)
			GUI.Label(new Rect(10,70,200,40),fpsUpdate);
		if(showFPSrender)
			GUI.Label(new Rect(10,90,200,40),fpsRender);
		if(showFPSphysx)
			GUI.Label(new Rect(10,110,200,40),fpsPhysX);
		GUI.Label(new Rect(10,130,200,40),"Inputs: " + InputManager.Default.AmountTrackedInputs);

		GUI.Label(new Rect(10,150,200,40),"Figure: " + LogData.SmashDetected);
		GUI.Label (new Rect (10, 170, 200, 40), "ReqForce: " + LogData.RequiredForce);
		GUI.Label(new Rect(10,190,200,40),"Force: " + LogData.SmashForce);

		float yDelta = 0.0f;
		Gesture motion = InputManager.Default.ActiveGestures.Find (x => x.Type == GestureType.Motion);
		if(motion != null)
		{
			float dt = Time.deltaTime;
			if(float.IsNaN(dt) || float.IsInfinity(dt))
				dt = 1.0f;

			yDelta = (motion.InputSource.DeltaPosition.y) * dt;
			//Debug.Log("DEBUG dt; " + Time.deltaTime);
			//Debug.Log("DEBUG: " + motion.InputSource.DeltaPosition.y);

			yDelta = Mathf.Abs(yDelta / Screen.height);
			yDelta *= 100;

			if(yDelta > _highestDeltaY)
				_highestDeltaY = yDelta;
		}*/
	}
}
#endif