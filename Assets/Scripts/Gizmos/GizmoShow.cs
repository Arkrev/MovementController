﻿//---------------------------------------------------
using UnityEngine;
using System.Collections;
//---------------------------------------------------
public class GizmoShow : MonoBehaviour 
{
	//---------------------------------------------------
	//Should we display gizmos for object?
	public bool ShowGizmos = true;
    
	//Object range of sight
	[Range(0f,100f)]
	public float Range = 10f;

	//Display forward vector
	//---------------------------------------------------
	void OnDrawGizmos() 
	{
		//Exit if gizmo drawing is disabled
		if(!ShowGizmos)return;
        
		//Draw color wire sphere
		Gizmos.color = Color.green;        
		Gizmos.DrawWireSphere(transform.position, Range);

        //Draw forward vector
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * Range);

        //Draw up vector
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.up * Range);
    }
	//---------------------------------------------------
}
//---------------------------------------------------