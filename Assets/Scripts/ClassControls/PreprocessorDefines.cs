﻿//--- Defines for the classes ---
//Unlike C and C++, you cannot assign a numeric value to a symbol; the #if 
//statement in C# is Boolean and only tests whether the symbol has been defined or not.
//So, if we don't want certain code to be triggered, don't define the Symbol in the project settings.
//If we want to include it in our project, eg: logging the InputManager, use the following steps:
// - Open project setting, under the Build tab, add the Name to the Conditional Compilation Symbols!
//You can add it only to one file by using '#define name'. Warning though, if you do this we cannot 
//guarantee all code will be executed correctly because some classes won't implement the required
//features.

// Here you can find the list with the usable define names
// - UNITY_DEBUG_LOGGING -: Enables Debug with logging using our own log framework
// - UNITY_KINECT -: Enables the usage of the Kinect (make sure the Kinect SDK is installed)

//Usings to make the file/class recognizable
using UnityEngine;

//Class for usage if you want to implement compiler  stuff
public static class PreprocessorDefines
{ }