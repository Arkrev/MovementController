﻿#region using...
using System.Linq;
using System.Xml.Linq;
using UnityEngine;
#endregion

public static class PlayerInputManager
{
    #region Settings...
    //_____________________________________________________________________
    // Settings to know which devices are connected.
    private static bool isLeapEnabled = false;
    private static bool isKeyboardEnabled = false;
    private static bool isGamepadEnabled = false;
    private static bool isKinectEnabled = false;

    //_____________________________________________________________________
    // Settings to know what is the current gesture recognize by the leapmotion.
    private static bool isLeapMovingForward = false;
    private static bool isLeapMovingBackward = false;
    private static bool isLeapMovingRight = false;
    private static bool isLeapMovingLeft = false;
    private static bool isLeapMovingUp = false;
    private static bool isLeapMovingDown = false;
    private static bool isLeapPointing = false;
    private static bool isLeapRotatingHand = false;
    private static bool isLeapTaking = false;
    private static bool isLeapDropping = false;
    private static bool isLeapPaused = false;
    private static bool isLeapManipulatingObject = false;

    //_____________________________________________________________________
    // Settings for Kinect managment
    private static bool currentObservationGestureState = false;
    private static bool oldObservationGestureState = false;
    private static bool isKinectRotationModeON = false;
    private static string personHeight = "H188cm";
    private static KinectBody myKinectBody = null;
    private static float playerFieldRadius;
    private static Vector3 playerFieldPos = Vector3.zero;
    private static float matchForward = 0.1f;

    //_____________________________________________________________________
    // Settings can be used as well as by the kinect or by the leap.
    // There are for gecture management in general.
    private static bool startTakeGesture = false;
    private static bool stopTakeGesture = false;
    private static bool releaseTakeGesture = false;
    private static bool pickGesture = false;
    private static bool flyBirdActivateGesture = false;
    #endregion

    #region Body...
    //_____________________________________________________________________
    /// <summary>
    /// Finds which devices are said as connected in GameModule.xml.
    /// </summary>
    public static void WhichDeviceIsSelected()
    {
        //---------------------------------------------------------------------
        // Read Game Module Config XML.
        XDocument xml = XDocument.Load(Application.dataPath + "/Resources/GameModule.xml");

        //---------------------------------------------------------------------
        // If file is found.
        if (xml != null)
        {
            //---------------------------------------------------------------------
            // Get the root
            XElement root = xml.Root;

            //---------------------------------------------------------------------
            // Read the information.
            XElement gameController = root.Element(XName.Get("GameController"));
            if (gameController != null)
            {
                try
                {
                    IsKeyboardEnabled = gameController.Attribute(XName.Get("Keyboard")).Value == "1" ? true : false;
                    IsGamepadEnabled = gameController.Attribute(XName.Get("Gamepad")).Value == "1" ? true : false;
                    IsLeapEnabled = gameController.Attribute(XName.Get("Leap")).Value == "1" ? true : false;
                    IsKinectEnabled = gameController.Attribute(XName.Get("Kinect")).Value == "1" ? true : false;
                }
                catch
                {
                    Debug.LogWarning("You may have put some error on the line : \n<GameController Kinect=\"0\" Gamepad=\"1\" Keyboard=\"1\" Leap=\"0\"/> " +
                        "<!-- Put 0 or 1 after the game controller you want to deactivate(0) or activate(1) : Kinect, Gamepad, Keyboard, Leap -->");
                }
            }
            if (IsKinectEnabled)
            {
                XElement playerField = root.Element(XName.Get("PlayerField"));
                if (playerField != null)
                {
                    try
                    {
                        playerFieldRadius = float.Parse(playerField.Attribute(XName.Get("Radius")).Value);
                        playerFieldPos = new Vector3(0, 0, float.Parse(playerField.Attribute(XName.Get("Distance")).Value, System.Globalization.CultureInfo.InvariantCulture));
                    }
                    catch
                    {
                        Debug.LogWarning("You may have put some error on the line : \n<PlayerField Distance=\"2.2\" Radius=\"0.3\"/> <!-- 3.3 far, 2.2 for close -->");
                    }
                }
            }
        }
        else
        {
            Debug.LogWarning("No xml found.");
        }

    }

    //_____________________________________________________________________
    /// <summary>
    /// KinectGestures Debug.
    /// </summary>
    public static void WatchKinectGestures()
    {
        if (IsKinectEnabled)
        {
            bool leftWRightGesture = InputManager.Default.GetMatchPosePercent("LookLeftwRightArm_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("leftWRightGesture " + leftWRightGesture);
            bool leftWLeftGesture = InputManager.Default.GetMatchPosePercent("LookLeftwLeftArm_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("leftWLeftGesture " + leftWLeftGesture);
            bool rightGesture = InputManager.Default.GetMatchPosePercent("LookRight_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("rightGesture " + rightGesture);

            bool openArmGesture = InputManager.Default.GetMatchPosePercent("StopTakeOpenArms_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("openArmGesture " + openArmGesture);

            bool lookUpGesture = InputManager.Default.GetMatchPosePercent("LookUp_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("lookUpGesture " + lookUpGesture);
            bool lookDownGesture = InputManager.Default.GetMatchPosePercent("LookDown_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("lookDownGesture " + lookDownGesture);

            bool amountFwd1 = InputManager.Default.GetMatchPosePercent("ForwardsFastest_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("amountFwd1 " + amountFwd1);
            bool amountFwd2 = InputManager.Default.GetMatchPosePercent("ForwardsFast_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("amountFwd2 " + amountFwd2);
            bool amountFwd3 = InputManager.Default.GetMatchPosePercent("ForwardsMiddle_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("amountFwd3 " + amountFwd3);
            bool amountFwd4 = InputManager.Default.GetMatchPosePercent("ForwardsSlow_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("amountFwd4 " + amountFwd4);

            bool forwardArmsGesture = InputManager.Default.GetMatchPosePercent("StartTake_Match_D3m_" + personHeight) > 0.025f ? true : false;
            Debug.Log("forwardArmsGesture " + forwardArmsGesture);
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Manages the input for horizontal movement
    /// </summary>
    /// <returns>0 if no horizontal move; 1 if it's right input ON; -1 if it's left input ON</returns>
    public static float MainHorizontal()
    {
        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float result = 0.0f;

        //---------------------------------------------------------------------
        if (IsLeapEnabled)
        {
            if (IsLeapMovingRight)
            {
                result += 1f;
            }
            else if (IsLeapMovingLeft)
            {
                result -= 1f;
            }
        }
        //---------------------------------------------------------------------
        if (IsKeyboardEnabled)
        {
            result += Input.GetAxis("K_MainHorizontal");
        }
        //---------------------------------------------------------------------
        if (IsGamepadEnabled)
        {
            result += Input.GetAxis("J_MainHorizontal");
        }
        //---------------------------------------------------------------------
        if (IsKinectEnabled)
        {
            result += KinectRotateHorizontal();
        }

        //---------------------------------------------------------------------
        // If there are multiple game controller enabled it's better 
        // to clamp the result to not have a too high one.
        result = Mathf.Clamp(result, -1f, 1f);
        //Debug.Log("result Horizontal = " + result);
        return result;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Manages the input for vertical movement
    /// </summary>
    /// <returns>0 if no vertical move; 1 if it's Up input ON; -1 if it's Down input ON</returns>
    public static float MainVertical()
    {
        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float result = 0.0f;

        //---------------------------------------------------------------------
        if (IsLeapEnabled && !IsLeapPaused)
        {
            if (IsLeapMovingForward)
            {
                result += 1;
            }
            else if (IsLeapMovingBackward)
            {
                result -= 1;
            }
        }
        //---------------------------------------------------------------------
        if (IsGamepadEnabled)
        {
            result += Input.GetAxis("J_MainVertical");
        }
        //---------------------------------------------------------------------
        if (IsKeyboardEnabled)
        {
            result += Input.GetAxis("K_MainVertical");
        }
        if (IsKinectEnabled)
        {
            result += KinectMoveVertical();
        }

        //---------------------------------------------------------------------
        // If there are multiple game controller enabled it's better 
        // to clamp the result to not have a too high one.
        result = Mathf.Clamp(result, -1.0f, 1.0f);
        //Debug.Log("result Vertical = " + result);
        return result;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotation Up & Down For Leap
    /// </summary>
    /// <returns>0 if no vertical rotation; 1 if it's Up input ON; -1 if it's Down input ON</returns>
    public static float LeapRotateVertical()
    {
        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float result = 0;

        //---------------------------------------------------------------------
        if (IsLeapMovingUp)
        {
            result += 1;
        }
        else if (IsLeapMovingDown)
        {
            result -= 1;
        }

        //---------------------------------------------------------------------
        return result;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotation Right & Left For Kinect
    /// </summary>
    /// <returns>0 if no horizontal rotation; 1 if it's Right input ON; -1 if it's Left input ON</returns>
    private static float KinectRotateHorizontal()
    {
        float lookRightPct = 0f;
        float lookLeftPct = 0f;
        bool openArmGesture = false;
        if (IsKinectEnabled)
        {
            //---------------------------------------------------------------------
            // Get the percentage of matching with a pose.
            lookRightPct = InputManager.Default.GetMatchPosePercent("LookRight_Match_D3m_" + personHeight);
            lookLeftPct = Mathf.Max(InputManager.Default.GetMatchPosePercent("LookLeftwRightArm_Match_D3m_" + personHeight),
                                   InputManager.Default.GetMatchPosePercent("LookLeftwLeftArm_Match_D3m_" + personHeight));

            openArmGesture = InputManager.Default.GetMatchPosePercent("StopTakeOpenArms_Match_D3m_" + personHeight) > 0.025f ? true : false;
        }

        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float horizontal = 0f;

        //---------------------------------------------------------------------
        if (!openArmGesture)
        {
            if (lookRightPct > 0.05f || lookLeftPct > 0.01f)
            {
                if (lookRightPct > 0)
                    horizontal = 0.75f;
                else
                    horizontal = -0.75f;
            }
        }

        //---------------------------------------------------------------------
        return horizontal;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotation Up & Down For Kinect
    /// </summary>
    /// <returns>0 if no vertical rotation; 1 if it's Up input ON; -1 if it's Down input ON</returns>
    public static float KinectRotateVertical()
    {
        bool lookUpGesture = false;
        bool lookDownGesture = false;

        if (IsKinectEnabled)
        {
            //---------------------------------------------------------------------
            // Get the percentage of matching with a pose.
            lookUpGesture = Mathf.Max(InputManager.Default.GetMatchPosePercent("LookUp_Match_D3m_" + personHeight),
                                       InputManager.Default.GetMatchPosePercent("LookUpStretched_Match_D3m_" + personHeight)) > 0.01f ? true : false;
            lookDownGesture = Mathf.Max(InputManager.Default.GetMatchPosePercent("LookDown_Match_D3m_" + personHeight),
                                       InputManager.Default.GetMatchPosePercent("LookDownStretched_Match_D3m_" + personHeight)) > 0.4f ? true : false;
        }

        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float vertical = 0.0f;

        //---------------------------------------------------------------------
        if (lookDownGesture)
            vertical = -0.5f;
        else if (lookUpGesture)
            vertical = 0.5f;

        //---------------------------------------------------------------------
        return vertical;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Move Forward & Backward For Kinect
    /// </summary>
    /// <returns>0 if no vertical move; 1 if it's Forward input ON; -1 if it's Backward input ON</returns>
    private static float KinectMoveVertical()
    {
        float amountFwd1 = 0;
        float amountFwd2 = 0;
        float amountFwd3 = 0;
        float amountFwd4 = 0;
        bool forwardArmsGesture = false;


        if (IsKinectEnabled)
        {
            //---------------------------------------------------------------------
            // Get the percentage of matching with a pose.
            amountFwd1 = InputManager.Default.GetMatchPosePercent("ForwardsFastest_Match_D3m_" + personHeight);
            amountFwd2 = InputManager.Default.GetMatchPosePercent("ForwardsFast_Match_D3m_" + personHeight);
            amountFwd3 = InputManager.Default.GetMatchPosePercent("ForwardsMiddle_Match_D3m_" + personHeight);
            amountFwd4 = InputManager.Default.GetMatchPosePercent("ForwardsSlow_Match_D3m_" + personHeight);

            forwardArmsGesture = InputManager.Default.GetMatchPosePercent("StartTake_Match_D3m_" + personHeight) > 0.005f ? true : false;
        }

        //---------------------------------------------------------------------
        // Initialize the result to 0 because if there are no input ON we have to return 0.
        float vertical = 0.0f;

        //---------------------------------------------------------------------
        if (!forwardArmsGesture && KinectRotateVertical() == 0 && KinectRotateHorizontal() == 0)
        {
            float max = Mathf.Max(amountFwd1, amountFwd2, amountFwd3, amountFwd4);
            if (max >= matchForward)
            {
                if (amountFwd1 == max)
                    vertical = 1.5f;
                if (amountFwd2 == max)
                    vertical = 1.25f;
                if (amountFwd3 == max)
                    vertical = 1f;
                if (amountFwd4 == max)
                    vertical = 0.75f;
            }
        }

        //---------------------------------------------------------------------
        return vertical;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Move for all devices
    /// </summary>
    /// <returns>Vector3 containing horizontal and vertical move</returns>
    public static Vector3 MainJoyStick()
    {
        return new Vector3(MainHorizontal(), 0, MainVertical());
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotation for Gamepad (xbox-one)
    /// </summary>
    /// <returns>Vector3 containing horizontal and vertical move</returns>
    public static Vector3 RightJoyStick()
    {
        float hori = 0;
        float vert = 0;
        if (IsGamepadEnabled)
        {
            hori = Input.GetAxis("J_RightHorizontal");
            vert = Input.GetAxis("J_RightVertical");
        }
        else if (IsKeyboardEnabled)
        {
            hori = Input.GetKey(KeyCode.X) ? 1 : 0;
            vert = Input.GetKey(KeyCode.Y) ? -1 : 0;
        }
        //Debug.Log("JoystickDroit h=" + h + " v=" + v);
        return new Vector3(hori, 0, vert);
    }

    //_____________________________________________________________________
    // Buttons for Keyboard and Gamepad
    public static bool AButton()
    {
        return Input.GetButtonDown("A_Button");
    }
    public static bool BButton()
    {
        return Input.GetButton("B_Button");
    }
    public static bool XButton()
    {
        return Input.GetButton("X_Button");
    }
    public static bool YButton()
    {
        return Input.GetButtonDown("Y_Button");
    }
    public static bool KeyCtrl()
    {
        return Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftControl);
    }
    public static bool KeyMaj()
    {
        return Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift);
    }
    public static bool KeyF()
    {
        return Input.GetKeyDown(KeyCode.F);
    }
    public static bool RightBumper()
    {
        return Input.GetButton("Right_Bumper");
    }
    public static bool LeftBumper()
    {
        return Input.GetButton("Left_Bumper");
    }

    //_____________________________________________________________________
    // KinectGestures manager
    private static void WatchObservationGestureState()
    {
        if (currentObservationGestureState && !StopTakeGesture)
        {
            oldObservationGestureState = currentObservationGestureState;
        }
        else if (!currentObservationGestureState && StartTakeGesture)
        {
            oldObservationGestureState = currentObservationGestureState;
            currentObservationGestureState = true;
        }
        else
        {
            oldObservationGestureState = currentObservationGestureState;
            currentObservationGestureState = false;
        }
    }

    public static bool IsKinectRotationModeON
    {
        get
        {
            return isKinectRotationModeON;
        }

        set
        {
            isKinectRotationModeON = value;
        }
    }

    public static bool IsObservationGestureSwitched()
    {
        WatchObservationGestureState();
        return (currentObservationGestureState != oldObservationGestureState);
    }

    //_____________________________________________________________________
    // Kinect Cursor for selecting object Gestures
    public static void ControlCursorWithKinect(out Quaternion rawRotation, out float angleBewteenArms)
    {
        KinectBody body = MyKinectBody;
        if (body != null)
        {
            //Rotation is average of 2 wrist bones
            Bone bone1 = body.Bones.Find(x => x.Equals("WristRight"));
            Bone bone2 = body.Bones.Find(x => x.Equals("WristLeft"));
            Bone bone3 = body.Bones.Find(x => x.Equals("ElbowRight"));
            Bone bone4 = body.Bones.Find(x => x.Equals("ElbowLeft"));

            Quaternion rotation1 = new Quaternion(bone1.Orientation.x, bone1.Orientation.y, bone1.Orientation.z, bone1.Orientation.w);
            Quaternion rotation2 = new Quaternion(bone2.Orientation.x, bone2.Orientation.y, bone2.Orientation.z, bone2.Orientation.w);
            Quaternion rotation3 = new Quaternion(bone3.Orientation.x, bone3.Orientation.y, bone3.Orientation.z, bone3.Orientation.w);
            Quaternion rotation4 = new Quaternion(bone4.Orientation.x, bone4.Orientation.y, bone4.Orientation.z, bone4.Orientation.w);

            //Average of 2 direction vectors
            Vector3 forward = rotation1 * Vector3.up + rotation2 * Vector3.up + rotation3 * Vector3.up + rotation4 * Vector3.up;
            forward.z = -forward.z;

            // Returns
            rawRotation = Quaternion.LookRotation(forward);

            angleBewteenArms = Vector3.Angle(rotation1 * Vector3.up, rotation4 * Vector3.up);
        }
        else
        {
            // Returns
            rawRotation = Quaternion.identity;

            angleBewteenArms = Vector3.Angle(Vector3.left, Vector3.right);
        }
    }

    public static void objectManipulationGestures(out Vector3 dir, out Vector3 up)
    {
        KinectBody body = MyKinectBody;
        dir = Vector3.zero;
        up = Vector3.zero;
        if (body != null)
        {
            //Rotation of object based on hand positions
            Bone handBoneLeft = body.Bones.Find(x => x.Name == "HandLeft");
            Bone handBoneRight = body.Bones.Find(x => x.Name == "HandRight");

            Vector3 handsDir = (handBoneLeft.StartPosition - handBoneRight.StartPosition).normalized;
            handsDir.z = -handsDir.z;
            dir = handsDir;

            // Average Up vector for hands;
            Quaternion handsRot1 = new Quaternion(handBoneLeft.Orientation.x, handBoneLeft.Orientation.y, handBoneLeft.Orientation.z, handBoneLeft.Orientation.w);
            Quaternion handsRot2 = new Quaternion(handBoneRight.Orientation.x, handBoneRight.Orientation.y, handBoneRight.Orientation.z, handBoneRight.Orientation.w);

            Vector3 handsUp = (handsRot1 * Vector3.up + handsRot2 * Vector3.up).normalized;
            handsUp.z = -handsUp.z;
            up = handsUp;
        }
    }

    public static KinectBody MyKinectBody
    {
        get
        {
            if (KinectGestureRecognizer.Instance.HasActiveBody())
            {
                myKinectBody = KinectGestureRecorder.Instance.GetKinectBodyNearestToPos(PlayerFieldPos, PlayerFieldRadius);
            }
            else
            {
                myKinectBody = KinectGestureRecorder.Instance.GetKinectBody(KinectGestureRecorder.Instance.KnownIDs.FirstOrDefault());
            }
            return myKinectBody;
        }

        set
        {
            myKinectBody = value;
        }
    }
    public static float PlayerFieldRadius
    {
        get
        {
            return playerFieldRadius;
        }

        set
        {
            playerFieldRadius = value;
        }
    }
    public static Vector3 PlayerFieldPos
    {
        get
        {
            return playerFieldPos;
        }

        set
        {
            playerFieldPos = value;
        }
    }

    //_____________________________________________________________________
    // Generic Gestures boolean Get/set
    public static bool StartTakeGesture
    {
        get
        {
            bool temp = false;
            if (IsKinectEnabled)
            {
                temp = temp || InputManager.Default.GetMatchPosePercent("StartTake_Match_D3m_" + personHeight) > 0.025f ? true : false;
                if (InputManager.Default.GetMatchPosePercent("StartTake_Match_D3m_" + personHeight) > 0.025f ? true : false)
                    Debug.Log("Start take Gesture with Kinect");
            }
            startTakeGesture = temp;
            return startTakeGesture;
        }
    }
    public static bool StopTakeGesture
    {
        get
        {
            if (IsKinectEnabled)
                stopTakeGesture = InputManager.Default.GetMatchPosePercent("StopTake_Match_D3m_" + personHeight) > 0.025f ? true : false;
            return stopTakeGesture;
        }
    }
    public static bool ReleaseTakeGesture
    {
        get
        {
            if (IsKinectEnabled)
            {
                releaseTakeGesture = Mathf.Max(InputManager.Default.GetMatchPosePercent("ReleaseObject_Match_D3m_" + personHeight),
                                                InputManager.Default.GetMatchPosePercent("ReleaseObjectBehind_Match_D3m_" + personHeight)
                                              ) > 0.005f ? true : false;
                releaseTakeGesture = releaseTakeGesture && KinectGestureRecognizer.Instance.HasActiveBody();
            }

            return releaseTakeGesture;
        }
    }
    public static bool PickGesture
    {
        get
        {
            if (IsKinectEnabled)
                pickGesture = InputManager.Default.GetMatchPosePercent("PickObject_Match_D3m_" + personHeight) > 0.2f ? true : false;
            return pickGesture;
        }
    }
    public static bool FlyBirdActivateGesture
    {
        get
        {
            if (IsKinectEnabled)
                flyBirdActivateGesture = InputManager.Default.ActiveGestures.Find(x => x.Name == "FlyActivate_Seq_D3m_" + "H168cm" /*personHeight*/) != null ? true : false;
            return flyBirdActivateGesture;
        }
    }

    //_____________________________________________________________________
    // Leap boolean get/set
    public static bool IsLeapMovingForward
    {
        get
        {
            return isLeapMovingForward;
        }

        set
        {
            isLeapMovingForward = value;
        }
    }
    public static bool IsLeapMovingBackward
    {
        get
        {
            return isLeapMovingBackward;
        }

        set
        {
            isLeapMovingBackward = value;
        }
    }
    public static bool IsLeapMovingRight
    {
        get
        {
            return isLeapMovingRight;
        }

        set
        {
            isLeapMovingRight = value;
        }
    }
    public static bool IsLeapMovingLeft
    {
        get
        {
            return isLeapMovingLeft;
        }

        set
        {

            isLeapMovingLeft = value;
        }
    }
    public static bool IsLeapMovingUp
    {
        get
        {
            return isLeapMovingUp;
        }

        set
        {
            isLeapMovingUp = value;
        }
    }
    public static bool IsLeapMovingDown
    {
        get
        {
            return isLeapMovingDown;
        }

        set
        {
            isLeapMovingDown = value;
        }
    }
    public static bool IsLeapPointing
    {
        get
        {
            return isLeapPointing;
        }

        set
        {
            isLeapPointing = value;
        }
    }
    public static bool IsLeapRotatingHand
    {
        get
        {
            return isLeapRotatingHand;
        }

        set
        {
            isLeapRotatingHand = value;
        }
    }
    public static bool IsLeapTaking
    {
        get
        {
            return isLeapTaking;
        }

        set
        {
            isLeapTaking = value;
        }
    }
    public static bool IsLeapDropping
    {
        get
        {
            return isLeapDropping;
        }

        set
        {
            isLeapDropping = value;
        }
    }
    public static bool IsLeapPaused
    {
        get
        {
            return isLeapPaused;
        }

        set
        {
            isLeapPaused = value;
        }
    }
    public static bool IsLeapManipulatingObject
    {
        get
        {
            return isLeapManipulatingObject;
        }

        set
        {
            isLeapManipulatingObject = value;
        }
    }

    //_____________________________________________________________________
    // Connected Device boolean Get/set
    public static bool IsKinectEnabled
    {
        get
        {
            return isKinectEnabled;
        }

        set
        {
            isKinectEnabled = value;
        }
    }
    public static bool IsGamepadEnabled
    {
        get
        {
            return isGamepadEnabled;
        }

        set
        {
            isGamepadEnabled = value;
        }
    }
    public static bool IsKeyboardEnabled
    {
        get
        {
            return isKeyboardEnabled;
        }

        set
        {
            isKeyboardEnabled = value;
        }
    }
    public static bool IsLeapEnabled
    {
        get
        {
            return isLeapEnabled;
        }

        set
        {
            isLeapEnabled = value;
        }
    }





    #endregion
}
