﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PickObject
{
    public ObjectSettings objSettings;
    public Collider collider;
    public Button button;
}

public class ObjectManager : MonoBehaviour
{

    //_____________________________________________________________________
    // Settings for managing the object -> TODO : maybe put it on a Objectmanager script ?
    private bool isLookingAtObject = false;
    private GameObject go = null;
    private GameObject currentGameObject = null;
    private Dictionary<int, PickObject> pickedObjectDict = new Dictionary<int, PickObject>();
    private static ObjectManager _instance;
    private List<PickObject> pickObjectList = new List<PickObject>();
    private int amountPicked = -1;

    //---------------------------------------------------------------------
    // Singleton
    public static ObjectManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ObjectManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        if (_instance == null)
        {
            //If I am the first instance, make me the Singleton
            _instance = this;
        }
        else
        {
            //If a Singleton already exists and you find
            //another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }

    //---------------------------------------------------------------------
    // Init
    void Start()
    {
        ResetPickObjectList();
    }

    public void ResetPickObjectList()
    {
        //Objects from layer 'PickableObject' 
        GameObject[] objWithLayer = GameObject.FindGameObjectsWithTag("Pickable");
        //Debug.Log("Start ObjManager");
        foreach (GameObject obj in objWithLayer)
        {
            //Debug.Log("Obj" + obj.name);

            PickObject pickObj = new PickObject();
            pickObj.objSettings = obj.GetComponent<ObjectSettings>();
            pickObj.collider = obj.GetComponent<Collider>();
            PickObjectList.Add(pickObj);
        }
    }

    public PickObject FindPickedObjectByNameInList(string name)
    {
        foreach (PickObject pickObj in PickObjectList)
        {
            if (pickObj.objSettings.gameObject.name == name)
            {
                return pickObj;
            }
        }
        return null;
    }

    public void removePickedObjectFromDict(PickObject pickedObject)
    {
        int keyOfPickedObjToRemove = -1;
        foreach (KeyValuePair<int, PickObject> pickObj in PickedObjectDict)
        {
            if (pickObj.Value == pickedObject)
            {
                keyOfPickedObjToRemove = pickObj.Key;
                AmountPicked--;
                break;
            }
        }
        if (keyOfPickedObjToRemove >= 0)
        {
            PickedObjectDict.Remove(keyOfPickedObjToRemove);
        }
    }

    public void addPickedObjectToDict(PickObject pickedObject)
    {
        AmountPicked++;
        PickedObjectDict.Add(AmountPicked, pickedObject);
    }

    //_____________________________________________________________________
    // get/set any device ObjectSelection.
    public bool IsLookingAtObject
    {
        get
        {
            return isLookingAtObject;
        }

        set
        {
            isLookingAtObject = value;
        }
    }

    public GameObject Go
    {
        get
        {
            return go;
        }

        set
        {
            go = value;
        }
    }

    public Dictionary<int, PickObject> PickedObjectDict
    {
        get
        {
            return pickedObjectDict;
        }

        set
        {
            pickedObjectDict = value;
        }
    }

    public List<PickObject> PickObjectList
    {
        get
        {
            return pickObjectList;
        }

        set
        {
            pickObjectList = value;
        }
    }
    
    public int AmountPicked
    {
        get
        {
            return amountPicked;

        }
        set
        {
            amountPicked = value;
        }
    }
    public GameObject CurrentGameObject
    {
        get
        {
            return currentGameObject;
        }

        set
        {
            currentGameObject = value;
        }
    }
}
