﻿#region using...
using UnityEngine;
using System.Collections.Generic;
using System;
#endregion

public class TestPlayerInputManager : MonoBehaviour
{
    #region Settings...
    //_____________________________________________________________________
    // Settings for mode (moving or observing).
    private bool isObservationMode = false;
    private bool isDetailedObservationMode = false;

    //_____________________________________________________________________
    // Settings for managing the speed of the player 
    // or the speed of the rotation of the camera.
    [SerializeField]
    private float moveSpeed = /*0.1f;*/ /*350f;*/ 3f;
    [SerializeField]
    private float rotateSpeed = /*1.25f;*/ 60f;
    [SerializeField]
    private float upDownSpeed = 20f;

    //_____________________________________________________________________
    // Settings for managing the object rotation speed 
    // and the cursor translation speed.
    [SerializeField]
    private float objectRotationSpeed = 300f;
    [SerializeField]
    private float cursorSpeed = /*7f;*/ 250f;

    //_____________________________________________________________________
    // Settings for managing the player position and rotation
    private Rigidbody myRigidBody;
    private float rotVertical = 0f;
    [SerializeField]
    private Vector3 predifinedPosition = new Vector3(0.0f, 2.0f, 0.0f);
    [SerializeField]
    private float maxVerticalRotationEulerAngle = 85f;

    //_____________________________________________________________________
    // Settings for managing the pickables objects
    private GameObject currentGameObject;
    private GameObject watchedObject;
    private BookAnimation bookAnimation;
    private Vector3 initialTransformOfCurrentGameObjectPosition;
    private Quaternion initialTransformOfCurrentGameObjectRotation;
    private int MaxIconsPerColumn = 3;
    public float distanceFromCamera = 1;
    private GameObject watchedObjectPosition;

    // Variables used to validate how long an object as been pointed
    private bool isLooking = false;
    private float startLooking;
    private float stopLooking;
    private GameObject previousTargetObject;
    private GameObject currentTargetObject;

    //_____________________________________________________________________
    // Settings for managing the cursor
    public Texture2D AimTexture = null;
    private bool isCursorOnScreen = false;
    private float cursorHorizontalPosition = Screen.width * 0.5f;
    private float cursorVerticalPosition = Screen.height * 0.5f;
    private Vector2 _AimPos = Vector2.zero;     //Aim position on screen
    private Vector3 _AimDir = Vector3.zero;     //Aim direction in world    
    private Quaternion _aimRot = Quaternion.identity;

    //local Variable of controller Enabled.
    private bool isLeapEnabled = false;
    private bool isKeyboardEnabled = false;
    private bool isGamepadEnabled = false;
    private bool isKinectEnabled = false;

    //_____________________________________________________________________
    // Settings for managing the flying mode
    private bool isFlyingMode;

    //_____________________________________________________________________
    // Setting for leap UI
    //GameObject LeapMenu;

    #endregion

    #region Body...
    //_____________________________________________________________________
    /// <summary>
    ///  Initializes the player script by getting his body 
    ///  and which divice can control him.
    /// </summary>
    private void Start()
    {
        myRigidBody = GetComponent<Rigidbody>();
        PlayerInputManager.WhichDeviceIsSelected();
        watchedObjectPosition = GameObject.FindGameObjectWithTag("WatchedObjectPosition");
        isLeapEnabled = PlayerInputManager.IsLeapEnabled;
        isKeyboardEnabled = PlayerInputManager.IsKeyboardEnabled;
        isGamepadEnabled = PlayerInputManager.IsGamepadEnabled;
        isKinectEnabled = PlayerInputManager.IsKinectEnabled;

        //LeapMenu = GameObject.FindGameObjectWithTag("LeapUI");
        //if (LeapMenu != null)
        //{
        //    LeapMenu.SetActive(false);
        //}
    }

    //_____________________________________________________________________
    /// <summary>
    /// Update is called once per frame.
    /// </summary>
    private void Update()
    {
        //Debug.Log("Update running");
        //PlayerInputManager.WatchKinectGestures();
        MovePlayer();
    }

    //_____________________________________________________________________
    /// <summary>
    /// It's called by Update.
    /// It gets the user Input and then applies them.
    /// </summary>
    void MovePlayer()
    {
        //Debug.Log("MovePlayer");

        //---------------------------------------------------------------------
        if (PlayerInputManager.IsLeapEnabled || PlayerInputManager.IsKinectEnabled)
        {
            //Debug.Log("Leap or Kinect enabled");
            movePlayerForLeapOrKinect();
        }

        //---------------------------------------------------------------------
        else if (PlayerInputManager.IsKeyboardEnabled || PlayerInputManager.IsGamepadEnabled)
        {
            //Debug.Log("Keyboard or Gamepad enabled");
            movePlayerForKeyboardOrGamepad();
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Applies the Input in the right way for the Leapmotion and the Kinect.
    /// </summary>
    private void movePlayerForLeapOrKinect()
    {
        // TODO: Flying mode.
        // TODO: Find an unstuck Gesture
        //Debug.Log("movePlayerForLeapOrKinect");
        //---------------------------------------------------------------------
        // Let the player enter and leave the observation mode anytime.
        if (PlayerInputManager.IsObservationGestureSwitched())
        {
            switchObservationMode();
            Debug.Log("isObservationMode = " + isObservationMode);
        }

        //---------------------------------------------------------------------
        // Switch to detail observation mode and put the watch object behind us.
        if (ObjectManager.instance != null)
        {
            if (ObjectManager.instance.IsLookingAtObject && !isDetailedObservationMode)
            {
                currentGameObject = ObjectManager.instance.Go;
                ObjectManager.instance.CurrentGameObject = currentGameObject;
                resetCameraToStandard();
                resetCursor();
                createViewOfObject();
                switchDetailedObservation();
                //if (isLeapEnabled)
                //{
                //    LeapMenu.SetActive(true);
                //}
            }
        }

        //---------------------------------------------------------------------
        // Resets the cursor if we're not in observation mode 
        // and if the cursor is not already resetted.
        if (!isObservationMode)
        {
            if (isCursorOnScreen)
                resetCursor();
        }
        //---------------------------------------------------------------------
        // Let the player target an object.
        if (isKinectEnabled && isObservationMode && !isDetailedObservationMode)
        {
            cursorMoveAndRaycast();
        }
        //---------------------------------------------------------------------
        // Let the player look more closely to an object.
        else if (isDetailedObservationMode)
        {
            //Debug.Log("We're observing in details");
            // Let the player take an object with him.
            if (isLeapEnabled)
            {
                if (PlayerInputManager.IsLeapTaking)
                {
                    storeObject();
                    resetSelectionVariables();
                    switchDetailedObservation();
                    //LeapMenu.SetActive(false);
                    PlayerInputManager.IsLeapTaking = false;
                }
                else if (PlayerInputManager.IsLeapDropping)
                {
                    releaseObject();
                    resetSelectionVariables();
                    switchDetailedObservation();
                    //LeapMenu.SetActive(false);
                    PlayerInputManager.IsLeapDropping = false;
                }
            }
            /*if (isLeapEnabled)
            {
                if (currentGameObject.tag == "Observable" || currentGameObject.tag == "Pickable")
                {
                    Vector3 rotation = new Vector3(PlayerInputManager.MainJoyStick().x, PlayerInputManager.LeapRotateVertical(), PlayerInputManager.MainJoyStick().z);
                    rotateObject(rotation, currentGameObject);
                }
            }*/
            if (isKinectEnabled)
            {
                if (PlayerInputManager.PickGesture)
                {
                    storeObject();
                    resetSelectionVariables();
                    switchDetailedObservation();
                }
                else if (PlayerInputManager.ReleaseTakeGesture)
                {
                    releaseObject();
                    resetSelectionVariables();
                    switchDetailedObservation();
                }

               else if (currentGameObject.tag == "Observable" || currentGameObject.tag == "ObsBotParent" || currentGameObject.tag == "PickableBotParent" || currentGameObject.tag == "Pickable")
                    rotateObjectForKinect(currentGameObject);
            }
        }
        //---------------------------------------------------------------------
        // If we are not observing (then we are moving ).
        else
        {
            //Debug.Log("We're moving");
            Vector3 rotation = new Vector3(
                        0.0f,
                        0.0f,
                        Mathf.Clamp(
                            PlayerInputManager.LeapRotateVertical()
                                + PlayerInputManager.KinectRotateVertical(),
                            -1f, 1f)
                        );
            moveCameraAroundPlayer(rotation);
            Vector3 movement = PlayerInputManager.MainJoyStick();
            movePlayer(movement);
        }
    }


    /// <summary>
    /// Applies the Input in the right way for the Keyboard and the Gamepad.
    /// </summary>
    private void movePlayerForKeyboardOrGamepad()
    {
        if ((isKeyboardEnabled && PlayerInputManager.KeyF()) || (PlayerInputManager.YButton() && !isDetailedObservationMode))
        {
            switchFlyingMode();
            Debug.Log("Isflying : " + IsFlyingMode);
            myRigidBody.useGravity = !myRigidBody.useGravity;
        }

        //---------------------------------------------------------------------
        // Let the player unstuck anytime.
        if (!isDetailedObservationMode && PlayerInputManager.AButton())
        {
            unstuckPlayer();
            //Debug.Log("Unstuck happended");
        }
        //---------------------------------------------------------------------
        // switch to detail observation mode and put the watch object behind us.
        if (ObjectManager.instance != null)
        {
            if (ObjectManager.instance.IsLookingAtObject && !isDetailedObservationMode)
            {
                currentGameObject = ObjectManager.instance.Go;
                resetCameraToStandard();
                createViewOfObject();
                resetCursor();
                switchDetailedObservation();
            }
        }
        //---------------------------------------------------------------------
        // Rotation of the camera.
        if (((isKeyboardEnabled && PlayerInputManager.KeyCtrl()) || PlayerInputManager.BButton()) && !isDetailedObservationMode)
        {
            resetCursor();
            moveCameraAroundPlayer(PlayerInputManager.MainJoyStick());
        }
        else if (isGamepadEnabled && !((isKeyboardEnabled && PlayerInputManager.KeyMaj()) || PlayerInputManager.XButton()) && !isDetailedObservationMode)
        {
            resetCursor();
            moveCameraAroundPlayer(PlayerInputManager.RightJoyStick());
        }
        //---------------------------------------------------------------------
        // Normal behaviour of the player.
        if (!isDetailedObservationMode && !IsFlyingMode)
        {
            //Debug.Log("normal mode");
            normalMoveForKeyboardAndGamepad();
        }
        else if (isDetailedObservationMode)
        {
            //Debug.Log("isDetailedObservationMode");
            ObserveInDetailsAnObject();
        }
        //---------------------------------------------------------------------
        // Let the player fly
        else
        {
            //Debug.Log("flying mode");
            iBelieveICanFly();
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Applies the Input in the right way for the Keyboard and the Gamepad.
    /// It's applied for the non flying mode.
    /// </summary>
    private void normalMoveForKeyboardAndGamepad()
    {
        //---------------------------------------------------------------------
        // Mode viewfinder with cursor
        if ((isKeyboardEnabled && PlayerInputManager.KeyMaj()) || PlayerInputManager.XButton())
        {
            cursorMoveAndRaycast();
        }
        //---------------------------------------------------------------------
        // Move the player
        else if (!((isKeyboardEnabled && PlayerInputManager.KeyCtrl()) || PlayerInputManager.BButton()))
        {
            resetCursor();
            movePlayer(PlayerInputManager.MainJoyStick());
        }
    }

    #region Move and Rotate player and camera

    //_____________________________________________________________________
    /// <summary>
    /// Resets the camera forward the player to move more easily.
    /// </summary>
    void resetCameraToStandard()
    {
        //transform.rotation = Quaternion.Euler(new Vector3(0.0f, transform.rotation.y, 0.0f));
        rotVertical = 0f;
        Camera.main.transform.localEulerAngles = new Vector3(rotVertical, Camera.main.transform.localEulerAngles.y, Camera.main.transform.localEulerAngles.z);
    }

    //_____________________________________________________________________
    /// <summary>
    /// Moves the player (following by his camera) according to a Vector3.
    /// It is used in Standard Movement Mode.
    /// </summary>
    /// <param name="movement">Vector3(horizontalInput, 0, verticalInput)</param>
    void movePlayer(Vector3 movement)
    {
        transform.Rotate(movement.x * Vector3.up * rotateSpeed * Time.deltaTime);
        myRigidBody.velocity = movement.z * transform.forward * moveSpeed /*/ Time.deltaTime*/;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotates the camera the around horizontal axis of the player 
    /// to look Up or Down. It is used in Observation Mode.
    /// </summary>
    /// <param name="rotation">Vector3(horizontalInput, 0, verticalInput)</param>
    void moveCameraAroundPlayer(Vector3 rotation)
    {
        //---------------------------------------------------------------------
        // Horizontal rotation
        float rotHorizontal = rotation.x * rotateSpeed * Time.deltaTime;
        transform.Rotate(0, rotHorizontal, 0, Space.World);


        //---------------------------------------------------------------------
        // Vertical rotation
        rotVertical += rotation.z * rotateSpeed * Time.deltaTime;
        //Debug.Log("new  rotVertical = " + rotVertical);
        rotVertical = Mathf.Clamp(rotVertical, -maxVerticalRotationEulerAngle, maxVerticalRotationEulerAngle);
        //Debug.Log("clamped rotVertical = " + rotVertical);
        Camera.main.transform.localEulerAngles = new Vector3(-rotVertical, Camera.main.transform.localEulerAngles.y, Camera.main.transform.localEulerAngles.z);
        //Debug.Log("transform  =" +Camera.main.transform.localEulerAngles);
    }

    #endregion

    #region Cursor things...

    //_____________________________________________________________________
    /// <summary>
    /// Resets the cursor on its initial position that is the middle of the sreen.
    /// </summary>
    void resetCursor()
    {
        isCursorOnScreen = false;
        cursorHorizontalPosition = Screen.width * 0.5f;
        cursorVerticalPosition = Screen.height * 0.5f;
        _AimPos = new Vector2(cursorHorizontalPosition, cursorVerticalPosition);
    }

    //_____________________________________________________________________
    /// <summary>
    /// Moves the cursor on the screen (clamped by the size of the window)
    /// and raycasts on the world to find pickables objects.
    /// </summary>
    void cursorMoveAndRaycast()
    {
        isCursorOnScreen = true;
        if (isKinectEnabled)
        {
            kinectControlCursor();
        }
        else if (isKeyboardEnabled || isGamepadEnabled)
        {
            moveCursorOnScreen();
        }
        raycastWithCursor();
    }

    //_____________________________________________________________________
    /// <summary>
    /// Moves the cursor on the screen (clamped by the size of the window).
    /// </summary>
    void moveCursorOnScreen()
    {
        Vector3 movement = PlayerInputManager.MainJoyStick() * cursorSpeed * Time.deltaTime;

        cursorHorizontalPosition += movement.x;
        cursorHorizontalPosition = Mathf.Clamp(cursorHorizontalPosition, 0f, Screen.width);

        cursorVerticalPosition -= movement.z;
        cursorVerticalPosition = Mathf.Clamp(cursorVerticalPosition, 0f, Screen.height);

        _AimPos = new Vector2(cursorHorizontalPosition, cursorVerticalPosition);
    }

    //_____________________________________________________________________
    /// <summary>
    /// Controls the cursor to grab object thanks to kinect gestures.
    /// </summary>
    private void kinectControlCursor()
    {
        Debug.Log("kinect control the cursor");
        //Raw rotation -> Jitterish
        Quaternion rawRotation;
        if (isKinectEnabled && KinectGestureRecognizer.Instance.HasActiveBody())
        {
            float angleBetweenArms;
            PlayerInputManager.ControlCursorWithKinect(out rawRotation, out angleBetweenArms);
            //Debug.Log("rawrotation 0 = " + rawRotation);
            MovingCursorByFollowingRawRotation(rawRotation);
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Calculates the _AimPos of the cursor on the screen by following the rawRotation
    /// </summary>
    /// <param name="rawRotation"></param>    
    private void MovingCursorByFollowingRawRotation(Quaternion rawRotation)
    {
        //Debug.Log("MovingCursorByFollowingRawRotation");
        if (_aimRot.Equals(Quaternion.identity))
        {
            // First time aiming -> take the raw aim position.
            _aimRot = rawRotation;
        }
        else
        {
            // Smooth transition between previous and current aim position to minimize jittering.
            _aimRot = Quaternion.Lerp(_aimRot, rawRotation, Mathf.Clamp01(Time.deltaTime * 3f));
        }
        // Calculate aim direction in world space (with camera rotation included).
        Vector3 _AimDir = Camera.main.transform.rotation * _aimRot * Vector3.forward * 0.5f;

        // Aim position on screen (offset from camera position).
        var temp = Camera.main.transform.position + _AimDir * 2f;
        _AimPos = Camera.main.WorldToScreenPoint(temp);
        _AimPos.y = Screen.height - _AimPos.y;
        // Clamp the _AimPos coordinate for that the cursor cannot go out of the screen.
        _AimPos = new Vector2(Mathf.Clamp(_AimPos.x, 0f, Screen.width), Mathf.Clamp(_AimPos.y, 0f, Screen.height));
        //Debug.Log("_AimPos with Kinect = " + temp + " = " + GameManager.instance.FPSCam.ScreenToWorldPoint(_AimPos));
    }



    //_____________________________________________________________________
    /// <summary>
    /// Raycasts on the world to find pickables objects.
    /// </summary>
    void raycastWithCursor()
    {
        Ray ray = Camera.main.ScreenPointToRay(new Vector2(_AimPos.x, Screen.height - _AimPos.y));
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        bool isHit;
        isHit = Physics.Raycast(ray, out hit, 100f);
        if (isHit)
        {
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 0.25f, false);
        }
        else
        {
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue, 0.25f, true);
        }
        if (isHit && (hit.transform.gameObject.tag == "Observable" || hit.transform.gameObject.tag == "PickableBotParent" || hit.transform.gameObject.tag == "ObservableBottle" || hit.transform.gameObject.tag == "ObsBotParnet" || hit.transform.gameObject.tag == "Pickable" || hit.transform.gameObject.tag == "Readable"))
        {

            currentTargetObject = hit.transform.gameObject;
            
            //Bottle Debug
            if (currentTargetObject.tag == "ObservableBottle")
            {
                currentTargetObject.GetComponent<Rigidbody>().useGravity = false;
                currentTargetObject.GetComponent<Rigidbody>().isKinematic = true;
                currentTargetObject = currentTargetObject.transform.parent.gameObject;
            }

            Debug.Log("hit : " + currentTargetObject.name);
            if (isLooking == false)
            {
                isLooking = true;
                startLooking = Time.time;
                previousTargetObject = currentTargetObject;
            }
            else
            {
                stopLooking = Time.time;
                if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                {
                    ObjectManager.instance.IsLookingAtObject = true;
                    ObjectManager.instance.Go = currentTargetObject;
                    //Debug.Log("did this " + PlayerInputManager.IsLookingAtObject +" and " + PlayerInputManager.ObjectName);
                    //Debug.Log("Has been looking forever now "+ (stopLooking-startLooking) + " with " + startLooking + " and stop at "+stopLooking);
                }
                else if (currentTargetObject != previousTargetObject)
                {
                    resetSelectionVariables();
                }
            }
        }
        else if (isHit && (hit.transform.gameObject.tag == "Page" || hit.transform.gameObject.tag == "TextArea"))
        {
            currentTargetObject = hit.transform.gameObject;
            if (hit.transform.gameObject.tag == "TextArea")
            {
                if (isLooking == false)
                {
                    isLooking = true;
                    startLooking = Time.time;
                    previousTargetObject = currentTargetObject;
                }
                else
                {
                    stopLooking = Time.time;
                    if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                    {
                        Debug.Log("start audio clip");
                        AudioSource audi = currentTargetObject.GetComponent<AudioSource>();
                        audi.PlayOneShot(audi.clip, 1);
                        startLooking = stopLooking;
                    }
                    else if (currentTargetObject != previousTargetObject)
                    {
                        resetSelectionVariables();
                    }
                }
            }
            Debug.Log("hit : " + currentTargetObject.name);
        }
        // No Collision with Ray.
        else if (isHit && (hit.transform.gameObject.tag == "Page" || hit.transform.gameObject.tag == "TextArea"))
        {
            currentTargetObject = hit.transform.gameObject;
            if (hit.transform.gameObject.tag == "TextArea")
            {
                if (isLooking == false)
                {
                    isLooking = true;
                    startLooking = Time.time;
                    previousTargetObject = currentTargetObject;
                }
                else
                {
                    stopLooking = Time.time;
                    if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                    {
                        Debug.Log("start audio clip");
                        AudioSource audi = currentTargetObject.GetComponent<AudioSource>();
                        audi.PlayOneShot(audi.clip, 1);
                        startLooking = stopLooking;
                    }
                    else if (currentTargetObject != previousTargetObject)
                    {
                        resetSelectionVariables();
                    }
                }
            }
            Debug.Log("hit : " + currentTargetObject.name);
        }
        else
        {
            //Debug.Log("No Collision with Ray");
            ObjectManager.instance.IsLookingAtObject = false;
            if (isLooking == true)
            {
                resetSelectionVariables();
            }
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Just resets some variable usefull for grabbing a targeted object.
    /// </summary>
    private void resetSelectionVariables()
    {
        isLooking = false;
        startLooking = 0;
        stopLooking = 0;
        currentTargetObject = null;
        previousTargetObject = null;
        ObjectManager.instance.IsLookingAtObject = false;
        ObjectManager.instance.Go = null;
    }

    #endregion

    //_____________________________________________________________________
    /// <summary>
    /// Resets the position of the player to predifinedPosition.
    /// </summary>
    void unstuckPlayer()
    {
        myRigidBody.MovePosition(predifinedPosition);
    }

    #region Switch modes...

    //_____________________________________________________________________
    /// <summary>
    /// Switches between observation mode and movement mode.
    /// isObservationMode = true when we are in observation mode, 
    /// else isObservationMode = false.
    /// </summary>
    void switchObservationMode()
    {
        isObservationMode = !isObservationMode;
        PlayerInputManager.IsKinectRotationModeON = (isObservationMode || isDetailedObservationMode);
    }

    /// <summary>
    /// Switches between detailed observation mode and observation mode.
    /// isDetailedObservationMode = true when we are in detailed observation mode, 
    /// else isDetailedObservationMode = false.
    /// </summary>
    void switchDetailedObservation()
    {
        isDetailedObservationMode = !isDetailedObservationMode;
        PlayerInputManager.IsLeapManipulatingObject = !PlayerInputManager.IsLeapManipulatingObject;
        PlayerInputManager.IsKinectRotationModeON = (isObservationMode || isDetailedObservationMode);
    }

    /// <summary>
    /// Switches between normal mode and flying mode.
    /// IsFlyingMode = true when we are in flying mode, 
    /// else IsFlyingMode = false.
    /// </summary>
    void switchFlyingMode()
    {
        IsFlyingMode = !IsFlyingMode;
    }

    #endregion

    #region Object interactions...

    //_____________________________________________________________________
    /// <summary>
    /// Allows to observe in details an object by turning it or reading it.
    /// </summary>
    private void ObserveInDetailsAnObject()
    {
        // Let the player take an object with him.
        if (PlayerInputManager.AButton())
        {
            switchDetailedObservation();
            storeObject();
            resetSelectionVariables();
        }
        else if (PlayerInputManager.BButton())
        {
            switchDetailedObservation();
            releaseObject();
            resetSelectionVariables();
        }
        else
        {
            if (currentGameObject.tag == "Observable" || currentGameObject.tag == "ObsBotParent" || currentGameObject.tag == "Pickable" || currentGameObject.tag == "PickableBotParent")
            {
                Vector3 rotation = new Vector3(PlayerInputManager.MainJoyStick().x, PlayerInputManager.RightJoyStick().x+ PlayerInputManager.RightJoyStick().z, PlayerInputManager.MainJoyStick().z);
                rotateObject(rotation, currentGameObject);
            }
            else if (currentGameObject.tag == "Readable")
            {
                if ((isKeyboardEnabled && PlayerInputManager.KeyMaj()) || PlayerInputManager.XButton())
                {
                    cursorMoveAndRaycast();
                }
                else
                {
                    resetCursor();
                }
            }
        }
    }


    //_____________________________________________________________________
    /// <summary>
    /// Stores an object into the user storage.
    /// </summary>
    void storeObject()
    {
        if (currentGameObject.tag == "Readable")
        {
            if (bookAnimation != null)
            {
                bookAnimation.StopCoroutine("AnimeBook");
                bookAnimation.StartCoroutine("CloseBook");
                bookAnimation.StopCoroutine("CloseBook");
            }
        }
        Debug.Log("pick :" + currentGameObject.name + " tag :" + currentGameObject.tag);
        ObjectManager.instance.ResetPickObjectList();
        PickObject pickedObject = ObjectManager.instance.FindPickedObjectByNameInList(currentGameObject.name);
        if (pickedObject != null)
        {
            ObjectManager.instance.addPickedObjectToDict(pickedObject);
            //GameObject.Destroy(watchedObject);
            currentGameObject.SetActive(false);
            watchedObjectPosition.transform.rotation = Quaternion.identity;
        }
        else releaseObject();
    }

    //_____________________________________________________________________
    /// <summary>
    /// Drops an object from the user storage.
    /// </summary>
    void dropStoredObject(PickObject pickedObject)
    {
        if (pickedObject != null)
        {
            ObjectManager.instance.removePickedObjectFromDict(pickedObject);
            GameObject go = pickedObject.objSettings.gameObject;
            go.SetActive(true);
            go.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 2;
            if (go.GetComponent<Rigidbody>() != null)
            {
                go.GetComponent<Rigidbody>().useGravity = true;
                go.GetComponent<Rigidbody>().isKinematic = false;
            }
            if (go.GetComponent<Collider>() != null)
            {
                go.GetComponent<Collider>().enabled = true;
            }
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Release a watched object.
    /// </summary>
    void releaseObject()
    {
        if (currentGameObject.tag == "Readable")
        {
            if (bookAnimation != null)
            {
                bookAnimation.StopCoroutine("AnimeBook");
                bookAnimation.StartCoroutine("CloseBook");
                bookAnimation.StopCoroutine("CloseBook");
            }
        }
        SmoothMove watchedSmoothMove = currentGameObject.GetComponent<SmoothMove>();
        if (watchedSmoothMove != null)
        {
            currentGameObject.transform.SetPositionAndRotation(currentGameObject.transform.position, initialTransformOfCurrentGameObjectRotation);
            watchedSmoothMove.Target = initialTransformOfCurrentGameObjectPosition;
        }
        else
        {
            Debug.LogWarning("Don't find the movesmooth");
        }
        if (currentGameObject.GetComponent<Rigidbody>() != null)
        {
            currentGameObject.GetComponent<Rigidbody>().useGravity = true;
            currentGameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
        if (currentGameObject.GetComponent<Collider>() != null)
        {
            currentGameObject.GetComponent<Collider>().enabled = true;
        }
        watchedObjectPosition.transform.rotation = Quaternion.identity;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotates te object object.
    /// </summary>
    /// <param name="movement">The rotation to apply to the object</param>
    /// <param name="target">The object to rotate</param>
    void rotateObject(Vector3 movement, GameObject target)
    {
        if (PlayerInputManager.IsLeapRotatingHand)
        {
            /*Frame frame = PlayerInputManager.Provider.CurrentFrame;
            Hand r_Hand = frame.Hands[0];
            Hand l_Hand = frame.Hands[1];

            Debug.Log("frame = " + frame + " r_hand = " + r_Hand + " l_hand = " + l_Hand);
            Debug.Log("look at : "+ l_Hand.PalmPosition.ToVector3());
            target.transform.LookAt(l_Hand.PalmPosition.ToVector3());*/
        }
        else
        {
            target.transform.Rotate(movement * objectRotationSpeed * Time.deltaTime);
        }
        //Debug.Log("target transform" + target + target.transform);
        Debug.Log("Main" + ObjectManager.instance.Go);
    }

    //_____________________________________________________________________
    /// <summary>
    /// Rotates te object object thanks to Kinect Gestures.
    /// </summary>
    /// <param name="target">The object to rotate</param>
    private void rotateObjectForKinect(GameObject target)
    {
        Quaternion rawRotation;
        Vector3 handsDir;
        Vector3 handsUp;
        PlayerInputManager.objectManipulationGestures(out handsDir, out handsUp);
        //rawRotation = Quaternion.LookRotation(handsDir, watchedObjectPosition.transform.rotation * Vector3.up) /** Quaternion.Euler(0f, -90f, 0f)*/;
        rawRotation = Quaternion.LookRotation(handsDir, watchedObjectPosition.transform.localRotation * Vector3.up) * Quaternion.Euler(0f, -90f, 0f);
        //Rotate pickup
        watchedObjectPosition.transform.localRotation= rawRotation;
        target.transform.rotation = watchedObjectPosition.transform.rotation;
        //target.transform.position = watchedObjectPosition.transform.position;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Moves the targeted object in front of the camera.
    /// </summary>
    void createViewOfObject()
    {
        if (currentGameObject != null)
        {
            initialTransformOfCurrentGameObjectPosition = currentGameObject.transform.position;
            initialTransformOfCurrentGameObjectRotation = currentGameObject.transform.rotation;
            if (currentGameObject.GetComponent<Rigidbody>() != null)
            {
                currentGameObject.GetComponent<Rigidbody>().useGravity = false;
                currentGameObject.GetComponent<Rigidbody>().isKinematic = true;
            }
            if (currentGameObject.GetComponent<Collider>() != null)
            {
                currentGameObject.GetComponent<Collider>().enabled = false;
            }
            //Debug
            //if (currentGameObject.GetComponent<MeshRenderer>() != null)
            //{
            //    currentGameObject.GetComponent<MeshRenderer>().enabled = false;
            //}
            //EndDebug
            SmoothMove watchedSmoothMove = currentGameObject.GetComponent<SmoothMove>();
            if (watchedSmoothMove != null)
            {
                if (currentGameObject.tag == "Readable")
                {
                    watchedSmoothMove.Target = Camera.main.transform.position + Camera.main.transform.forward * 0.25f + Camera.main.transform.right * 0.125f;
                    currentGameObject.transform.localEulerAngles = new Vector3(90, 180, 360 - this.transform.localEulerAngles.y);
                }
                else
                {
                    watchedSmoothMove.Target = watchedObjectPosition.transform.position; //Camera.main.transform.position + Camera.main.transform.forward * distanceFromCamera;
                }
            }
            else
            {
                Debug.LogWarning("Don't find the movesmooth");
            }

            if (currentGameObject.tag == "Readable")
            {
                bookAnimation = currentGameObject.GetComponent<BookAnimation>();
                if (bookAnimation != null)
                {
                    Debug.Log("Start StartCoroutine");
                    bookAnimation.StartCoroutine("AnimeBook");
                }
            }
        }
    }

    #endregion


    #region Fly...

    //_____________________________________________________________________
    /// <summary>
    /// Get and Set for isFlyingMode.
    /// </summary>
    public bool IsFlyingMode
    {
        get
        {
            return isFlyingMode;
        }

        set
        {
            isFlyingMode = value;
        }
    }

    //_____________________________________________________________________
    /// <summary>
    /// Lets the player fly !
    /// </summary>
    void iBelieveICanFly()
    {
        myRigidBody.useGravity = false;

        if (!PlayerInputManager.BButton() && !(isKeyboardEnabled && PlayerInputManager.KeyCtrl()))
        {
            Vector3 movement = PlayerInputManager.MainJoyStick();
            //---------------------------------------------------------------------
            // Move the player forward or backward.
            myRigidBody.velocity = movement.z * moveSpeed * Camera.main.transform.forward;
            //---------------------------------------------------------------------
            // Rotate the player left or right
            if (!isKeyboardEnabled)
            {
                myRigidBody.velocity += movement.x * moveSpeed * Camera.main.transform.right; //Move the player left or right
                Vector3 rotation = PlayerInputManager.RightJoyStick();
                //---------------------------------------------------------------------
                // Rotate the camera up and down.
                moveCameraAroundPlayer(new Vector3(rotation.x, 0f, 0f));
                //---------------------------------------------------------------------
                // Rotate the player left or right
                moveCameraAroundPlayer(new Vector3(0f, 0f, rotation.z));
            }
            else
            {
                transform.Rotate(movement.x * rotateSpeed * Vector3.up * Time.deltaTime, Space.World);
            }

            //Vector3 rotation = PlayerInputManager.RightJoyStick();
            ////---------------------------------------------------------------------
            //// Rotate the camera up and down.
            //moveCameraAroundPlayer(new Vector3(rotation.x, 0f, 0f));

            ////---------------------------------------------------------------------
            //// Rotate the player left or right
            //moveCameraAroundPlayer(new Vector3(0f, 0f, rotation.z));

            if (PlayerInputManager.RightBumper())
            {
                myRigidBody.velocity += 1f * upDownSpeed * Vector3.up /** Time.deltaTime*/;
            }
            else if (PlayerInputManager.LeftBumper())
            {
                myRigidBody.velocity -= 1f * upDownSpeed * Vector3.up /** Time.deltaTime*/;
            }
        }
    }

    #endregion

    //_____________________________________________________________________
    /// <summary>
    /// Update the enabled Input settings
    /// </summary>
    public void updateEnabledInterface()
    {
        PlayerInputManager.WhichDeviceIsSelected();
        isLeapEnabled = PlayerInputManager.IsLeapEnabled;
        isKeyboardEnabled = PlayerInputManager.IsKeyboardEnabled;
        isGamepadEnabled = PlayerInputManager.IsGamepadEnabled;
        isKinectEnabled = PlayerInputManager.IsKinectEnabled;
    }

    //_____________________________________________________________________
    /// <summary>
    /// Prints a cursor on the screen when we are on targeting mode.
    /// And prints the inventory.
    /// </summary>
    public void OnGUI()
    {
        if (IsFlyingMode)
        {
            GUIStyle myStyle = new GUIStyle();
            myStyle.fontSize = Screen.height / 100;
            GUI.Label(new Rect(10, 0, Screen.height, 100), "I believe I'm flying !");
        }
        // Cursor
        if (isCursorOnScreen)
        {
            float aimTexWidth = 100f;
            GUI.DrawTexture(new Rect(_AimPos.x - aimTexWidth * 0.5f, _AimPos.y - aimTexWidth * 0.5f, aimTexWidth, aimTexWidth), AimTexture);
        }

        // Inventory
        int row = 1;
        int column = 0;
        if (ObjectManager.instance != null)
        {
            foreach (KeyValuePair<int, PickObject> pickObj in ObjectManager.instance.PickedObjectDict)
            {
                if (pickObj.Key / row >= MaxIconsPerColumn)
                {
                    row += 1;
                    column = 0;
                }
                column += 1;
                PickObject myObj = pickObj.Value;
                ObjectSettings myObjSettings = myObj.objSettings;
                Texture2D textureOfMyObj = myObjSettings.GuiIcon;
                bool isClick = GUI.Button(
                    new Rect(
                        Screen.width - ((Screen.height / 10 + 10) * row),
                        10 + ((Screen.height / 10 + 10) * (column - 1)),
                        Screen.height / 10,
                        Screen.height / 10
                        ),
                        textureOfMyObj);
                if (isClick)
                {
                    //Debug.Log("CLick" + myObjSettings.name);
                    dropStoredObject(myObj);
                }
            }
        }
    }
    #endregion
}
