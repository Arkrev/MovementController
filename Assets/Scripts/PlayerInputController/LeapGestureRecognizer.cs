﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leap;
using Leap.Unity;
using Leap.Unity.Attributes;


public class LeapGestureRecognizer : MonoBehaviour
{

    //LEAP MOTION
    [AutoFind]
    public LeapServiceProvider provider;
    GameObject cube;
    GameObject dummy;
    //Transform InitialDummyTranform;

    //private void Start()
    //{
    //    InitialDummyTranform = dummy.transform;
    //}

    public void StartMoveForward()
    {
        PlayerInputManager.IsLeapMovingForward = true;
    }
    public void StopMoveForward()
    {
        PlayerInputManager.IsLeapMovingForward = false;
    }
    public void StartMoveBackward()
    {
        PlayerInputManager.IsLeapMovingBackward = true;
    }
    public void StopMoveBackward()
    {
        //Debug.Log("Backward");
        PlayerInputManager.IsLeapMovingBackward = false;
    }
    public void StartRotateRight()
    {
        PlayerInputManager.IsLeapMovingRight = true;
    }
    public void StopRotateRight()
    {
        PlayerInputManager.IsLeapMovingRight = false;
    }
    public void StartRotateLeft()
    {
        PlayerInputManager.IsLeapMovingLeft = true;
    }
    public void StopRotateLeft()
    {
        PlayerInputManager.IsLeapMovingLeft = false;
    }
    public void StartRotateUp()
    {
        //Debug.Log("StartRotateUp()");
        PlayerInputManager.IsLeapMovingUp = true;
    }
    public void StopRotateUp()
    {
        //Debug.Log("StopRotateUp()");
        PlayerInputManager.IsLeapMovingUp = false;
    }
    public void StartRotateDown()
    {
        //Debug.Log("StartRotateDown()");
        PlayerInputManager.IsLeapMovingDown = true;
    }
    public void StopRotateDown()
    {
        //Debug.Log("StopRotateDown()");
        PlayerInputManager.IsLeapMovingDown = false;
    }
    public void StartPointing()
    {
        //Debug.Log("StartPointing()");
        PlayerInputManager.IsLeapPointing = true;
    }
    public void StopPointing()
    {
        //Debug.Log("StopPointing()");
        PlayerInputManager.IsLeapPointing = false;
    }
    public void StartRotatingHand()
    {
        Debug.Log("StartRotating()");
        PlayerInputManager.IsLeapRotatingHand = true;
        StartCoroutine("LeapHandRotation");
    }
    public void StopRotatingHand()
    {
        Debug.Log("StopRotating()");
        PlayerInputManager.IsLeapRotatingHand = false;
        StopCoroutine("LeapHandRotation");
    }
    public void StartTaking()
    {
        PlayerInputManager.IsLeapTaking = true;
    }
    public void StopTaking()
    {
        PlayerInputManager.IsLeapTaking = false;
    }
    public void StartDropping()
    {
        PlayerInputManager.IsLeapDropping = true;
    }
    public void StopDropping()
    {
        PlayerInputManager.IsLeapDropping = false;
    }
    public void StartPause()
    {

        PlayerInputManager.IsLeapPaused = true;
        //float start = Time.time;
        //Debug.Log("Pause Star at :" + start);
        StartCoroutine("LeapPause");
    }
    public void StopPause()
    {
        PlayerInputManager.IsLeapPaused = false;
    }

    IEnumerator LeapPause()
    {
        yield return new WaitForSeconds(0.5f);
        StopPause();
    }

    private void Update()
    {
        if (PlayerInputManager.IsLeapManipulatingObject)
        {
            Debug.Log("toto");
            Transform cameraTransform = Camera.main.transform;

            if (dummy == null)
            {

                dummy = new GameObject();
                dummy.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

            }
            dummy.transform.position = Camera.main.transform.position + Camera.main.transform.forward * 0.4f;
            GameObject currentGameObject = ObjectManager.instance.CurrentGameObject;

            Frame frame = provider.CurrentFrame;
            if (frame.Hands.Count >= 2)
            {
                Hand r_Hand = frame.Hands[0];
                Hand l_Hand = frame.Hands[1];
                
                
                if (l_Hand != null && r_Hand != null)
                {
                    PlayerInputManager.IsLeapTaking = false;
                    PlayerInputManager.IsLeapDropping = false;
                    Vector3 handsDir = Vector3.zero;
                    float handsDist = 0;
                    handsDir = (l_Hand.PalmPosition.ToVector3() - r_Hand.PalmPosition.ToVector3()).normalized;
                    handsDist = (Vector3.Distance(l_Hand.PalmPosition.ToVector3(), r_Hand.PalmPosition.ToVector3()));

                    Debug.Log("Dummy trans :" + dummy.transform.localRotation.eulerAngles);
                    dummy.transform.localRotation = Quaternion.LookRotation(handsDir, dummy.transform.localRotation * Vector3.up) * Quaternion.Euler(0f, -90f, 0f);
                    currentGameObject.transform.rotation = dummy.transform.rotation;
                    //Debug.Log("dist between hands : " + handsDist);

                    if (handsDist < 0.12f)
                    {
                        PlayerInputManager.IsLeapTaking = true;
                    }
                    else if(handsDist > 0.72f)
                    {
                        PlayerInputManager.IsLeapDropping = true;
                    }

                }
            }
            else
            {
                dummy.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
        }


    }



    private float getDistanceBetweenHand(Hand r_Hand, Hand l_Hand)
{
    float rx = r_Hand.Direction.x;
    float ry = r_Hand.Direction.y;
    float rz = r_Hand.Direction.z;
    float lx = l_Hand.Direction.x;
    float ly = l_Hand.Direction.y;
    float lz = l_Hand.Direction.z;

    Vector3 l_HandPosition = l_Hand.PalmPosition.ToVector3();
    Vector3 r_HandPosition = r_Hand.PalmPosition.ToVector3();
    float distx = Mathf.Pow((Mathf.Sqrt(Mathf.Abs(l_HandPosition.x - r_HandPosition.x))), 2);
    float disty = Mathf.Pow((Mathf.Sqrt(Mathf.Abs(l_HandPosition.y - r_HandPosition.y))), 2);
    float distz = Mathf.Pow((Mathf.Sqrt(Mathf.Abs(l_HandPosition.z - r_HandPosition.z))), 2);

    float distTotal = Mathf.Sqrt(distx + disty + distz) /*/ 3*/;

    return distTotal;
}
}
