﻿using UnityEngine;
using Leap;
using Leap.Unity;
using Leap.Unity.Attributes;

using System.Collections.Generic;
using System;
using System.Collections;

public class LaserScript : MonoBehaviour
{
    LineRenderer line;
    [AutoFind]
    public LeapServiceProvider provider;

    private GameObject previousTargetObject;
    private GameObject currentTargetObject;
    private float startLooking;
    private float stopLooking;
    private bool isLooking = false;


    void Start()
    {
        if (PlayerInputManager.IsLeapEnabled == false)
        {
            foreach (GameObject laser in GameObject.FindGameObjectsWithTag("Laser"))
            {
                DestroyObject(laser);
            }
        }
        line = gameObject.GetComponent<LineRenderer>();
        line.enabled = false;
    }

    void Update()
    {

        if (PlayerInputManager.IsLeapPointing)
        {
            //Debug.Log("DetectLeapPoint");
            StopCoroutine("FireLaser");
            StartCoroutine("FireLaser");

        }
        else
        {

            line.enabled = false;
        }
    }

    private void resetSelectionVariables()
    {
        isLooking = false;
        startLooking = 0;
        stopLooking = 0;
        currentTargetObject = null;
        previousTargetObject = null;
        ObjectManager.instance.IsLookingAtObject = false;
        ObjectManager.instance.Go = null;
    }

    IEnumerator FireLaser()
    {
        Frame frame = provider.CurrentFrame;
        if (frame != null)
        {
            line.enabled = true;
            while (PlayerInputManager.IsLeapPointing)
            {

                Ray ray = new Ray(transform.position, transform.forward);

                RaycastHit hit;
                Hand hand;
                Finger finger;

                if (frame.Hands[0] != null)
                {

                    hand = frame.Hands[0];
                    finger = hand.Fingers[0];
                    Vector3 direction = new Vector3(finger.Direction.Pitch, finger.Direction.Roll, finger.Direction.Yaw);
                    ray.direction = (finger.Direction.ToVector3());
                    transform.SetPositionAndRotation(finger.StabilizedTipPosition.ToVector3(), Quaternion.Euler(finger.Direction.x, finger.Direction.y, finger.Direction.z));





                    line.SetPosition(0, ray.origin);
                    bool isHit;
                    isHit = Physics.Raycast(ray, out hit, 100f);
                    if (isHit)
                    {
                        line.SetPosition(1, hit.point);
                        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 0.25f, false);
                    }
                    else
                    {
                        line.SetPosition(1, ray.GetPoint(100));
                        Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue, 0.25f, true);
                    }
                    if (isHit && (hit.transform.gameObject.tag == "Observable" || hit.transform.gameObject.tag == "ObservableBottle" || hit.transform.gameObject.tag == "ObsBotParnet" || hit.transform.gameObject.tag == "Pickable" || hit.transform.gameObject.tag == "Readable"))
                    {

                        currentTargetObject = hit.transform.gameObject;

                        //Bottle Debug
                        if (currentTargetObject.tag == "ObservableBottle")
                        {
                            currentTargetObject.GetComponent<Rigidbody>().useGravity = false;
                            currentTargetObject.GetComponent<Rigidbody>().isKinematic = true;
                            currentTargetObject = currentTargetObject.transform.parent.gameObject;
                        }

                        Debug.Log("hit : " + currentTargetObject.name);
                        if (isLooking == false)
                        {
                            isLooking = true;
                            startLooking = Time.time;
                            previousTargetObject = currentTargetObject;
                        }
                        else
                        {
                            stopLooking = Time.time;
                            if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                            {
                                ObjectManager.instance.IsLookingAtObject = true;
                                ObjectManager.instance.Go = currentTargetObject;
                                //Debug.Log("did this " + PlayerInputManager.IsLookingAtObject +" and " + PlayerInputManager.ObjectName);
                                //Debug.Log("Has been looking forever now "+ (stopLooking-startLooking) + " with " + startLooking + " and stop at "+stopLooking);
                            }
                            else if (currentTargetObject != previousTargetObject)
                            {
                                resetSelectionVariables();
                            }
                        }
                    }
                    else if (isHit && (hit.transform.gameObject.tag == "Page" || hit.transform.gameObject.tag == "TextArea"))
                    {
                        currentTargetObject = hit.transform.gameObject;
                        if (hit.transform.gameObject.tag == "TextArea")
                        {
                            if (isLooking == false)
                            {
                                isLooking = true;
                                startLooking = Time.time;
                                previousTargetObject = currentTargetObject;
                            }
                            else
                            {
                                stopLooking = Time.time;
                                if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                                {
                                    Debug.Log("start audio clip");
                                    AudioSource audi = currentTargetObject.GetComponent<AudioSource>();
                                    audi.PlayOneShot(audi.clip, 1);
                                    startLooking = stopLooking;
                                }
                                else if (currentTargetObject != previousTargetObject)
                                {
                                    resetSelectionVariables();
                                }
                            }
                        }
                        Debug.Log("hit : " + currentTargetObject.name);
                    }
                    // No Collision with Ray.
                    else if (isHit && (hit.transform.gameObject.tag == "Page" || hit.transform.gameObject.tag == "TextArea"))
                    {
                        currentTargetObject = hit.transform.gameObject;
                        if (hit.transform.gameObject.tag == "TextArea")
                        {
                            if (isLooking == false)
                            {
                                isLooking = true;
                                startLooking = Time.time;
                                previousTargetObject = currentTargetObject;
                            }
                            else
                            {
                                stopLooking = Time.time;
                                if (stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name != "floor")
                                {
                                    Debug.Log("start audio clip");
                                    AudioSource audi = currentTargetObject.GetComponent<AudioSource>();
                                    audi.PlayOneShot(audi.clip, 1);
                                    startLooking = stopLooking;
                                }
                                else if (currentTargetObject != previousTargetObject)
                                {
                                    resetSelectionVariables();
                                }
                            }
                        }
                        Debug.Log("hit : " + currentTargetObject.name);
                    }
                    else
                    {
                        //Debug.Log("No Collision with Ray");
                        ObjectManager.instance.IsLookingAtObject = false;
                        if (isLooking == true)
                        {
                            resetSelectionVariables();
                        }
                    }
                    /*if (Physics.Raycast(ray, out hit, 100))
                    {
                        //Debug.Log("Collision with ray");
                        line.SetPosition(1, hit.point);
                        //Debug.Log(hit.transform.gameObject);
                        currentTargetObject = hit.transform.gameObject;
                        if(isLooking == false)
                        {
                            isLooking = true;
                            startLooking = Time.time;
                            previousTargetObject = currentTargetObject;
                        }
                        else
                        {
                            stopLooking = Time.time;
                            if(stopLooking - startLooking > 2 && currentTargetObject == previousTargetObject && currentTargetObject.name!="Floor")
                            {
                                ObjectManager.instance.IsLookingAtObject = true;
                                ObjectManager.instance.Go = currentTargetObject;
                                //Debug.Log("did this " + PlayerInputManager.IsLookingAtObject +" and " + PlayerInputManager.ObjectName);
                                //Debug.Log("Has been looking forever now "+ (stopLooking-startLooking) + " with " + startLooking + " and stop at "+stopLooking);
                            }else if(currentTargetObject != previousTargetObject)
                            {
                                isLooking = false;
                                startLooking = 0;
                                stopLooking = 0;
                                currentTargetObject = null;
                                previousTargetObject = null;
                                ObjectManager.instance.IsLookingAtObject = false;
                                ObjectManager.instance.Go = null;
                            }
                        }
                    }
                    else
                    {
                        //Debug.Log("No Collision with Ray");
                        ObjectManager.instance.IsLookingAtObject = false;
                        line.SetPosition(1, ray.GetPoint(100));
                        if(isLooking == true)
                        {
                            isLooking = false;
                            startLooking = 0;
                            stopLooking = 0;
                            currentTargetObject = null;
                            previousTargetObject = null;
                            ObjectManager.instance.IsLookingAtObject = false;
                            ObjectManager.instance.Go = null;
                        }
                    }*/

                }
                yield return null;
            }
        }
    }


    public GameObject CurrentTargetObject
    {
        get
        {
            return currentTargetObject;
        }

        set
        {
            currentTargetObject = value;
        }
    }
}